use std::cmp::Ordering;
use std::collections::{BinaryHeap, HashMap, HashSet};
use std::mem;
use std::time::{Duration, Instant};

use ggez::conf::{WindowMode, WindowSetup};
use ggez::glam::Vec2;
use ggez::graphics::{self, Canvas, Color, DrawMode, Mesh, Rect};
use ggez::{
    event::{self, EventHandler},
    Context, GameResult,
};

// Visualization for advent of code 2024 day 16 part 2

const DIRECTIONS: [(i32, i32); 4] = [(0, 1), (0, -1), (1, 0), (-1, 0)];

const TILE_SIZE: f32 = 8.0;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub struct Point<T> {
    pub x: T,
    pub y: T,
}

impl<T: std::ops::AddAssign + Clone + Copy> Point<T> {
    pub fn new(x: T, y: T) -> Point<T> {
        Point { x, y }
    }

    pub fn add(&mut self, other: &Point<T>) {
        self.x += other.x;
        self.y += other.y;
    }
}

impl<T: Ord> Ord for Point<T> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.x.cmp(&self.x).then_with(|| other.y.cmp(&self.y))
    }
}

impl<T: Ord> PartialOrd for Point<T> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Clone, Eq, PartialEq)]
struct State {
    cost: usize,
    position: Point<usize>,
    last_direction: usize,
    visited: HashSet<Point<usize>>,
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        other
            .cost
            .cmp(&self.cost)
            .then_with(|| self.position.cmp(&other.position))
            .then_with(|| self.last_direction.cmp(&other.last_direction))
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

struct MainState {
    map: Vec<Vec<char>>,
    best_paths_positions: HashSet<Point<usize>>,
    rows: i32,
    cols: i32,
    start: Point<usize>,
    end: Point<usize>,
    queue: BinaryHeap<State>,
    lowest_cost: usize,
    costs: HashMap<(Point<usize>, usize), usize>,
    steps: Vec<Point<usize>>,
    prev_steps: Vec<Point<usize>>,
    last_draw: Instant,
    draw_interval: Duration,
    is_first_draw: bool,
    canvas_image: graphics::ScreenImage,
}

impl MainState {
    pub fn new(map: Vec<Vec<char>>, ctx: &mut Context) -> GameResult<MainState> {
        let mut start = Point::new(0, 0);
        let mut end = start;
        for (i, line) in map.iter().enumerate() {
            for (j, &c) in line.iter().enumerate() {
                if c == 'S' {
                    start = Point::new(j, i);
                } else if c == 'E' {
                    end = Point::new(j, i);
                }
            }
        }

        let mut queue = BinaryHeap::new();
        queue.push(State {
            cost: 0,
            position: start,
            last_direction: 2,
            visited: HashSet::new(),
        });

        let mut best_paths_positions = HashSet::new();
        best_paths_positions.insert(start);

        let mut costs = HashMap::new();
        costs.insert((start, 2), 0);

        let steps = vec![start];

        Ok(MainState {
            rows: map.len() as i32,
            cols: map[0].len() as i32,
            map,
            best_paths_positions,
            start,
            end,
            queue,
            lowest_cost: usize::MAX,
            costs,
            steps,
            prev_steps: vec![start],
            last_draw: Instant::now(),
            draw_interval: Duration::from_millis(10),
            is_first_draw: true,
            canvas_image: graphics::ScreenImage::new(ctx, None, 1., 1., 1),
        })
    }

    fn draw_cell(
        &self,
        ctx: &mut Context,
        canvas: &mut Canvas,
        x: usize,
        y: usize,
        color: Color,
    ) -> GameResult<()> {
        let rect = Rect::new(
            (x as f32) * TILE_SIZE,
            (y as f32) * TILE_SIZE,
            TILE_SIZE,
            TILE_SIZE,
        );
        let mesh = Mesh::new_rectangle(ctx, DrawMode::fill(), rect, color)?;
        canvas.draw(&mesh, Vec2::new(0.0, 0.0));
        Ok(())
    }

    fn draw_spritebatch_maze(&mut self, ctx: &mut Context) -> GameResult {
        let mut canvas = graphics::Canvas::from_screen_image(ctx, &mut self.canvas_image, None);

        for (y, row) in self.map.iter().enumerate() {
            for (x, &cell) in row.iter().enumerate() {
                let color = match cell {
                    '#' => Color::from_rgb(0, 0, 0),
                    '.' => Color::from_rgb(255, 255, 255),
                    _ => continue,
                };
                self.draw_cell(ctx, &mut canvas, x, y, color)?;
            }
        }
        canvas.finish(ctx)?;

        Ok(())
    }

    fn draw_spritebatch_path(&mut self, ctx: &mut Context) -> GameResult {
        let mut canvas = graphics::Canvas::from_screen_image(ctx, &mut self.canvas_image, None);

        // Draw the current path
        for pos in &self.steps {
            self.draw_cell(
                ctx,
                &mut canvas,
                pos.x,
                pos.y,
                Color::from_rgb(155, 89, 182),
            )?;
        }
        for pos in &self.prev_steps {
            self.draw_cell(
                ctx,
                &mut canvas,
                pos.x,
                pos.y,
                Color::from_rgb(41, 128, 185),
            )?;
        }
        self.prev_steps = mem::replace(&mut self.steps, vec![]);
        // self.steps.clear();

        canvas.finish(ctx)?;
        Ok(())
    }
}

impl EventHandler for MainState {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        while self.last_draw.elapsed() < self.draw_interval && !self.queue.is_empty() {
            if let Some(State {
                cost,
                position,
                last_direction,
                mut visited,
            }) = self.queue.pop()
            {
                visited.insert(position);

                if position == self.end {
                    if cost < self.lowest_cost {
                        self.lowest_cost = cost;
                        self.best_paths_positions = visited.clone();
                    } else if cost == self.lowest_cost {
                        self.best_paths_positions.extend(&visited);
                    }
                    return Ok(());
                }

                for (i, (dx, dy)) in DIRECTIONS.iter().enumerate() {
                    let next = Point::new(position.x as i32 + dx, position.y as i32 + dy);
                    if next.x < 0
                        || next.x >= self.cols
                        || next.y < 0
                        || next.y >= self.rows
                        || self.map[next.y as usize][next.x as usize] == '#'
                    {
                        continue;
                    }

                    let next = Point::new(next.x as usize, next.y as usize);
                    let mut next_cost = cost + 1;
                    if last_direction != i {
                        next_cost += 1000;
                    }

                    if next_cost < *self.costs.get(&(next, i)).unwrap_or(&usize::MAX) {
                        self.costs.insert((next, i), next_cost);
                        self.queue.push(State {
                            cost: next_cost,
                            position: next,
                            last_direction: i,
                            visited: visited.clone(),
                        });
                        self.steps.push(next);
                    } else if next_cost == *self.costs.get(&(next, i)).unwrap_or(&usize::MAX) {
                        self.queue.push(State {
                            cost: next_cost,
                            position: next,
                            last_direction: i,
                            visited: visited.clone(),
                        });
                        self.steps.push(next);
                    }
                }
            }
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        if self.last_draw.elapsed() < self.draw_interval {
            return Ok(());
        }
        self.last_draw = Instant::now();

        let mut canvas = graphics::Canvas::from_frame(ctx, None);

        if self.is_first_draw {
            self.is_first_draw = false;
            self.draw_spritebatch_maze(ctx)?;
        }

        self.draw_spritebatch_path(ctx)?;
        canvas.draw(&self.canvas_image.image(ctx), Vec2::new(0.0, 0.0));

        // Draw best paths if algorithm finished
        if self.queue.is_empty() {
            for pos in &self.best_paths_positions {
                self.draw_cell(ctx, &mut canvas, pos.x, pos.y, Color::from_rgb(200, 150, 0))?;
            }

            canvas.draw(
                graphics::Text::new(&format!(
                    "Tiles of all best paths: {}",
                    self.best_paths_positions.len()
                ))
                .set_scale(30.0),
                graphics::DrawParam::new()
                    .dest(Vec2::new(0.0, self.cols as f32 * (TILE_SIZE + 0.4)))
                    .color(Color::from_rgb(200, 150, 0)),
            );
        }

        // Draw the start position
        self.draw_cell(
            ctx,
            &mut canvas,
            self.start.x,
            self.start.y,
            Color::from_rgb(0, 255, 0),
        )?;

        // Draw the end position
        self.draw_cell(
            ctx,
            &mut canvas,
            self.end.x,
            self.end.y,
            Color::from_rgb(255, 0, 0),
        )?;

        canvas.finish(ctx)?;
        Ok(())
    }
}

pub fn main() -> GameResult {
    let input: Vec<Vec<char>> = String::from_utf8_lossy(include_bytes!("16"))
        .lines()
        .map(|line| line.chars().collect())
        .collect();

    let size = TILE_SIZE + 1.0;

    let (mut ctx, event_loop) =
        ggez::ContextBuilder::new("advent of code 2024 day 16 part 2", "Konstantin Müller")
            .window_mode(
                WindowMode::default()
                    .dimensions(input[0].len() as f32 * size, input.len() as f32 * size),
            )
            .window_setup(WindowSetup::default().title("Advent of Code 2024 Day 16 Part 2"))
            .build()?;

    let state = MainState::new(input, &mut ctx)?;
    event::run(ctx, event_loop, state)
}
