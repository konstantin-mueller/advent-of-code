struct Monkey {
    items: Vec<i64>,
    operation: Box<dyn Fn(i64) -> i64>,
    test: (i64, usize, usize),
    inspected_items: usize,
}

pub fn monkey_in_the_middle(notes: &String, part_two: bool) -> usize {
    let mut modulo = 1;

    let mut monkeys: Vec<Monkey> = notes
        .split("\n\n")
        .map(|note| -> Monkey {
            let mut lines = note.split("\n");
            lines.next();

            Monkey {
                items: lines
                    .next()
                    .expect("not enough lines for monkey")
                    .replace("Starting items:", "")
                    .split(", ")
                    .filter_map(|item| item.trim().parse().ok())
                    .collect(),

                operation: {
                    let line = lines
                        .next()
                        .expect("not enough lines for monkey")
                        .replace("  Operation: new = old ", "");
                    let operand = line
                        .split_whitespace()
                        .last()
                        .expect("operation line is empty")
                        .parse::<i64>()
                        .ok();

                    if let Some(number) = operand {
                        match line
                            .chars()
                            .next()
                            .expect("expected operation line to contain an operation")
                        {
                            '+' => Box::new(move |old: i64| old + number),
                            '-' => Box::new(move |old: i64| old - number),
                            '*' => Box::new(move |old: i64| old * number),
                            '/' => Box::new(move |old: i64| old / number),
                            _ => panic!("unrecognized operator"),
                        }
                    } else {
                        match line
                            .chars()
                            .next()
                            .expect("expected operation line to contain an operation")
                        {
                            '+' => Box::new(move |old: i64| old + old),
                            '-' => Box::new(move |old: i64| old - old),
                            '*' => Box::new(move |old: i64| old * old),
                            '/' => Box::new(move |old: i64| old / old),
                            _ => panic!("unrecognized operator"),
                        }
                    }
                },

                test: (
                    {
                        let test = lines
                            .next()
                            .expect("not enough lines for monkey")
                            .split_whitespace()
                            .last()
                            .expect("test line is empty")
                            .parse()
                            .expect("expected last characters of test line to be a number");
                        modulo *= test;
                        test
                    },
                    lines
                        .next()
                        .expect("not enough lines for monkey")
                        .split_whitespace()
                        .last()
                        .expect("if true line is empty")
                        .parse()
                        .expect("expected last characters of if true line to be a number"),
                    lines
                        .next()
                        .expect("not enough lines for monkey")
                        .split_whitespace()
                        .last()
                        .expect("if false line is empty")
                        .parse()
                        .expect("expected last characters of if false line to be a number"),
                ),

                inspected_items: 0,
            }
        })
        .collect();

    for _ in 0..if part_two { 10000 } else { 20 } {
        for i in 0..monkeys.len() {
            for j in 0..monkeys[i].items.len() {
                let mut number = (monkeys[i].operation)(monkeys[i].items[j]);
                if part_two {
                    number %= modulo;
                } else {
                    number /= 3;
                }

                let k = if number % monkeys[i].test.0 == 0 {
                    monkeys[i].test.1
                } else {
                    monkeys[i].test.2
                };
                monkeys[k].items.push(number);
            }
            monkeys[i].inspected_items += monkeys[i].items.len();
            monkeys[i].items.clear();
        }
    }

    monkeys.sort_by_key(|monkey| monkey.inspected_items);
    monkeys
        .iter()
        .skip(monkeys.len() - 2)
        .map(|monkey| monkey.inspected_items)
        .product()
}
