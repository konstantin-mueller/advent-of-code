use std::collections::{HashMap, HashSet};

#[derive(Clone)]
struct Rock {
    left_most: i64,
    right_most: i64,
    down_most: i64,
    all_positions: Vec<(i64, i64)>,
}

pub fn pyroclastic_flow(jet_pattern: &String, max_rocks: i64) -> i64 {
    let rock_types = vec![
        Rock {
            left_most: 2,
            right_most: 3 + 2,
            down_most: 3,
            all_positions: vec![(0, 0), (1, 0), (2, 0), (3, 0)],
        },
        Rock {
            left_most: 2,
            right_most: 2 + 2,
            down_most: 3,
            all_positions: vec![(1, 0), (0, 1), (1, 1), (2, 1), (1, 2)],
        },
        Rock {
            left_most: 2,
            right_most: 2 + 2,
            down_most: 3,
            all_positions: vec![(0, 0), (1, 0), (2, 0), (2, 1), (2, 2)],
        },
        Rock {
            left_most: 2,
            right_most: 0 + 2,
            down_most: 3,
            all_positions: vec![(0, 0), (0, 1), (0, 2), (0, 3)],
        },
        Rock {
            left_most: 2,
            right_most: 1 + 2,
            down_most: 3,
            all_positions: vec![(0, 0), (1, 0), (0, 1), (1, 1)],
        },
    ];

    let mut rock_type_counter = 0;
    let mut rock = rock_types[rock_type_counter].clone();

    let mut fallen_rocks = HashSet::new();
    let mut pattern_counter = 0;
    let mut fall_down = false;
    let mut fallen_rocks_counter: i64 = 0;
    let mut cache: HashMap<(usize, usize), (i64, i64)> = HashMap::new();
    while fallen_rocks_counter < max_rocks {
        if fall_down {
            rock.down_most -= 1;
        } else {
            if jet_pattern.chars().nth(pattern_counter).unwrap() == '<' {
                if rock.left_most != 0
                    && !rock.all_positions.iter().any(|(x, y)| {
                        fallen_rocks.contains(&(x + rock.left_most - 1, y + rock.down_most))
                    })
                {
                    rock.left_most -= 1;
                    rock.right_most -= 1;
                }
            } else {
                if rock.right_most != 6
                    && !rock.all_positions.iter().any(|(x, y)| {
                        fallen_rocks.contains(&(x + rock.left_most + 1, y + rock.down_most))
                    })
                {
                    rock.left_most += 1;
                    rock.right_most += 1;
                }
            }

            pattern_counter += 1;
            if pattern_counter > jet_pattern.len() - 1 {
                pattern_counter = 0;
            }
        }
        fall_down = !fall_down;

        if rock.down_most < 0
            || rock
                .all_positions
                .iter()
                .any(|(x, y)| fallen_rocks.contains(&(x + rock.left_most, y + rock.down_most)))
        {
            fallen_rocks.extend(
                rock.all_positions
                    .iter()
                    .map(|(x, y)| (x + rock.left_most, y + rock.down_most + 1)),
            );

            rock_type_counter += 1;
            if rock_type_counter > rock_types.len() - 1 {
                rock_type_counter = 0;
            }
            rock = rock_types[rock_type_counter].clone();
            rock.down_most += *fallen_rocks.iter().map(|(_, y)| y).max().unwrap() + 1;

            fall_down = false;
            fallen_rocks_counter += 1;

            // try to detect cycles to be able to calculate end result
            if fallen_rocks_counter > 1000 {
                let counters = (rock_type_counter, pattern_counter);
                let height = fallen_rocks.iter().map(|(_, y)| y).max().unwrap() + 1;

                if cache.contains_key(&counters) {
                    let cached_rock = cache[&counters];
                    let period = fallen_rocks_counter - cached_rock.0;

                    if fallen_rocks_counter % period == max_rocks % period {
                        return cached_rock.1
                            + ((height - cached_rock.1)
                                * ((max_rocks - fallen_rocks_counter) / period + 1));
                    }
                } else {
                    cache.insert(counters, (fallen_rocks_counter, height));
                }
            }
        }
    }

    *fallen_rocks.iter().map(|(_, y)| y).max().unwrap() + 1
}
