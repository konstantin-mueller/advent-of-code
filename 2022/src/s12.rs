static RULES: [char; 27] = [
    'S', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
    's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
];

pub fn hill_climbing_algorithm(input: &Vec<String>, part_two: bool) -> i32 {
    let width = input[0].len();
    let heightmap: Vec<char> = input.iter().flat_map(|line| line.chars()).collect();

    let mut visited = vec![false; input.len() * width];

    let mut queue;
    if part_two {
        queue = heightmap
            .iter()
            .enumerate()
            .filter_map(
                |(i, height)| {
                    if height == &'a' {
                        Some((0, i))
                    } else {
                        None
                    }
                },
            )
            .collect();
    } else {
        let start = heightmap
            .iter()
            .position(|height| height == &'S')
            .expect("expected start 'S' to be in input");
        queue = vec![(0, start)];
    }

    let top_edge: Vec<usize> = (0..width).collect();
    let right_edge: Vec<usize> = top_edge.iter().map(|i| (i + 1) * width - 1).collect();
    let bottom_edge: Vec<usize> = ((heightmap.len() - width)..heightmap.len()).collect();
    let left_edge: Vec<usize> = top_edge.iter().map(|i| i * width).collect();

    let rule_index = |i: usize| {
        RULES
            .iter()
            .position(|rule| rule == &heightmap[i])
            .expect("expected every char to be present in rules")
    };

    while !queue.is_empty() {
        if let Some((steps, i)) = queue.pop() {
            if visited[i] {
                continue;
            }
            if heightmap[i] == 'E' {
                return steps;
            }

            visited[i] = true;

            let rule = RULES[rule_index(i) + 1] as u32;
            if !top_edge.contains(&i) && is_step_valid(heightmap[i], heightmap[i - width], rule) {
                queue.push((steps + 1, i - width));
            }
            if !right_edge.contains(&i) && is_step_valid(heightmap[i], heightmap[i + 1], rule) {
                queue.push((steps + 1, i + 1));
            }
            if !bottom_edge.contains(&i) && is_step_valid(heightmap[i], heightmap[i + width], rule)
            {
                queue.push((steps + 1, i + width));
            }
            if !left_edge.contains(&i) && is_step_valid(heightmap[i], heightmap[i - 1], rule) {
                queue.push((steps + 1, i - 1));
            }
        }
        queue.sort_by_key(|(steps, _)| *steps);
        queue.reverse();
    }

    panic!("No solution found!");
}

fn is_step_valid(current_height: char, next_height: char, current_rule: u32) -> bool {
    let next_height_u32 = next_height as u32;
    // 97 = a, 121 = y
    next_height_u32 <= current_rule
        && (next_height_u32 >= 97 || (current_height as u32 >= 121 && next_height == 'E'))
}
