use anyhow::Result;

pub fn cathode_ray_tube(instructions: &Vec<String>) -> Result<i32> {
    let mut result = 0;
    let mut result_counter = 20;
    let mut x = 1;
    let mut cycle = 1;
    for line in instructions {
        if line.starts_with("addx") {
            let number = line
                .split_whitespace()
                .last()
                .expect("expected addx instructions to contain a number")
                .parse::<i32>()?;

            cycle += 2;
            if cycle > result_counter {
                result += result_counter * x;
                result_counter += 40;
            }
            x += number;
        } else {
            cycle += 1;
        }

        if cycle == result_counter {
            result += result_counter * x;
            result_counter += 40;
        }
    }

    Ok(result)
}

pub fn part_two(instructions: &Vec<String>) -> Result<String> {
    let mut x = 1;
    let mut cycle = 1;
    let mut crt = vec![String::from(""); 6];
    crt[0] += "#";

    let mut i = 0;

    for line in instructions {
        if line.starts_with("addx") {
            let number = line
                .split_whitespace()
                .last()
                .expect("expected addx instructions to contain a number")
                .parse::<i32>()?;

            if x - 1 == cycle % 40 || x == cycle % 40 || x + 1 == cycle % 40 {
                crt[i] += "#";
            } else {
                crt[i] += ".";
            }

            if crt[i].len() == 40 {
                i += 1;
                if i == crt.len() {
                    break;
                }
            }

            cycle += 1;
            x += number;
        }

        if x - 1 == cycle % 40 || x == cycle % 40 || x + 1 == cycle % 40 {
            crt[i] += "#";
        } else {
            crt[i] += ".";
        }

        if crt[i].len() == 40 {
            i += 1;
            if i == crt.len() {
                break;
            }
        }

        cycle += 1;
    }

    Ok(crt.join("\n"))
}
