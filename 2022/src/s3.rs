pub fn rucksack_reorganization(rucksacks: &Vec<String>) -> u32 {
    let mut result = 0;

    for rucksack in rucksacks {
        let rest = &rucksack[rucksack.len() / 2..];

        for item in rucksack[0..rucksack.len() / 2].chars() {
            if rest.contains(item) {
                result += u32::from(item) - if item.is_uppercase() { 38 } else { 96 };
                break;
            }
        }
    }
    result
}

pub fn part_two(rucksacks: &Vec<String>) -> u32 {
    let mut result = 0;

    for i in (0..rucksacks.len()).step_by(3) {
        for item in rucksacks[i].chars() {
            if rucksacks[i + 1].contains(item) && rucksacks[i + 2].contains(item) {
                result += u32::from(item) - if item.is_uppercase() { 38 } else { 96 };
                break;
            }
        }
    }
    result
}
