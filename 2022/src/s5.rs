use anyhow::{Context, Result};

pub fn supply_stacks(drawing: &Vec<String>) -> Result<String> {
    let mut instructions = drawing
        .iter()
        .skip_while(|line| line.chars().nth(1) != Some('1'));

    let stack_numbers: Vec<&str> = instructions
        .next()
        .context("could not find stack numbers")?
        .split(" ")
        .collect();
    let stack_len: usize = stack_numbers[stack_numbers.len() - 2].parse()?;

    let mut stacks: Vec<Vec<char>> = vec![Vec::new(); stack_len];

    let all_crates = drawing
        .iter()
        .take_while(|line| line.chars().nth(1) != Some('1'))
        .collect::<Vec<&String>>();
    for crates in all_crates.iter().rev() {
        for i in 0..stack_len {
            let letter = crates
                .chars()
                .nth(i * 4 + 1)
                .context("less crates in string than expected")?;
            if letter != ' ' {
                stacks[i].push(letter);
            }
        }
    }

    instructions.next();
    for instruction in instructions {
        let mut inst = instruction.split(" ");

        let crate_count = inst
            .nth(1)
            .expect("expected instruction to contain numbers for crate count")
            .parse()?;

        let first_stack = inst
            .nth(1)
            .expect("expected instruction to contain numbers for first stack")
            .parse::<usize>()?
            - 1;

        let second_stack = inst
            .nth(1)
            .expect("expected instruction to contain numbers for second stack")
            .parse::<usize>()?
            - 1;

        for _ in 0..crate_count {
            let value = stacks[first_stack].pop();
            if let Some(letter) = value {
                stacks[second_stack].push(letter);
            }
        }
    }

    let mut result = String::new();
    for stack in stacks {
        result.push(*stack.last().expect("expected last element on stack"));
    }
    Ok(result)
}

pub fn part_two(drawing: &Vec<String>) -> Result<String> {
    let mut instructions = drawing
        .iter()
        .skip_while(|line| line.chars().nth(1) != Some('1'));

    let stack_numbers: Vec<&str> = instructions
        .next()
        .context("could not find stack numbers")?
        .split(" ")
        .collect();
    let stack_len: usize = stack_numbers[stack_numbers.len() - 2].parse()?;

    let mut stacks: Vec<Vec<char>> = vec![Vec::new(); stack_len];

    let all_crates = drawing
        .iter()
        .take_while(|line| line.chars().nth(1) != Some('1'))
        .collect::<Vec<&String>>();
    for crates in all_crates.iter().rev() {
        for i in 0..stack_len {
            let letter = crates
                .chars()
                .nth(i * 4 + 1)
                .context("less crates in string than expected")?;
            if letter != ' ' {
                stacks[i].push(letter);
            }
        }
    }

    instructions.next();
    for instruction in instructions {
        let mut inst = instruction.split(" ");

        let crate_count = inst
            .nth(1)
            .expect("expected instruction to contain numbers for crate count")
            .parse()?;

        let first_stack = inst
            .nth(1)
            .expect("expected instruction to contain numbers for first stack")
            .parse::<usize>()?
            - 1;

        let second_stack = inst
            .nth(1)
            .expect("expected instruction to contain numbers for second stack")
            .parse::<usize>()?
            - 1;

        let mut popped = (0..crate_count)
            .filter_map(|_| stacks[first_stack].pop())
            .collect::<Vec<char>>();
        popped.reverse();
        for letter in popped {
            stacks[second_stack].push(letter);
        }
    }

    let mut result = String::new();
    for stack in stacks {
        result.push(*stack.last().expect("expected last element on stack"));
    }
    Ok(result)
}
