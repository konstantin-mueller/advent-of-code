use std::collections::{HashMap, HashSet};

pub fn boiling_boulders(droplet: &Vec<String>) -> usize {
    let cubes: HashSet<(i32, i32, i32)> = droplet
        .iter()
        .map(|pos| {
            let mut position = pos.split(",");
            (
                position.next().unwrap().parse().unwrap(),
                position.next().unwrap().parse().unwrap(),
                position.next().unwrap().parse().unwrap(),
            )
        })
        .collect();

    cubes
        .iter()
        .map(|cube| 6 - (cubes.len() - (&cubes - &possible_neighbours(cube)).len()))
        .sum()
}

pub fn part_two(droplet: &Vec<String>) -> usize {
    let cubes: HashSet<(i32, i32, i32)> = droplet
        .iter()
        .map(|pos| {
            let mut position = pos.split(",");
            (
                position.next().unwrap().parse().unwrap(),
                position.next().unwrap().parse().unwrap(),
                position.next().unwrap().parse().unwrap(),
            )
        })
        .collect();

    let mut exterior: HashSet<(i32, i32, i32)> = HashSet::new();
    let max = (
        *cubes.iter().map(|(x, _, _)| x).max().unwrap(),
        *cubes.iter().map(|(_, y, _)| y).max().unwrap(),
        *cubes.iter().map(|(_, _, z)| z).max().unwrap(),
    );
    bfs((0, 0, 0), &cubes, &mut exterior, max);

    let mut air_pockets: HashMap<(i32, i32, i32), usize> = HashMap::new();
    for x in 0..max.0 {
        for y in 0..max.1 {
            for z in 0..max.2 {
                let cube = (x, y, z);
                if !exterior.contains(&cube) && !cubes.contains(&cube) {
                    air_pockets.insert(cube, 6);
                }
            }
        }
    }

    for i in 0..air_pockets.len() {
        for j in i + 1..air_pockets.len() {
            let first = air_pockets.keys().nth(i).unwrap();
            let second = air_pockets.keys().nth(j).unwrap().clone();
            if (first.0 == second.0 && first.1 == second.1 && (first.2 - second.2).abs() == 1)
                || (first.0 == second.0 && (first.1 - second.1).abs() == 1 && first.2 == second.2)
                || ((first.0 - second.0).abs() == 1 && first.1 == second.1 && first.2 == second.2)
            {
                air_pockets.insert(*first, 0.max(air_pockets[first] - 1));
                air_pockets.insert(second, 0.max(air_pockets[&second] - 1));
            }
        }
    }

    cubes
        .iter()
        .map(|cube| 6 - (cubes.len() - (&cubes - &possible_neighbours(cube)).len()))
        .sum::<usize>()
        - air_pockets.values().sum::<usize>()
}

fn possible_neighbours(cube: &(i32, i32, i32)) -> HashSet<(i32, i32, i32)> {
    [
        (cube.0 + 1, cube.1, cube.2),
        (cube.0 - 1, cube.1, cube.2),
        (cube.0, cube.1 + 1, cube.2),
        (cube.0, cube.1 - 1, cube.2),
        (cube.0, cube.1, cube.2 + 1),
        (cube.0, cube.1, cube.2 - 1),
    ]
    .into_iter()
    .collect()
}

fn bfs<'a>(
    cube: (i32, i32, i32),
    cubes: &HashSet<(i32, i32, i32)>,
    exterior: &'a mut HashSet<(i32, i32, i32)>,
    max: (i32, i32, i32),
) {
    let mut queue = vec![cube];
    let mut visited = HashSet::new();
    visited.insert(cube);

    while !queue.is_empty() {
        let current = queue.pop().unwrap();
        exterior.insert(current);

        for neighbour in possible_neighbours(&current)
            .into_iter()
            .filter(|neighbour| {
                neighbour.0 >= 0
                    && neighbour.0 <= max.0
                    && neighbour.1 >= 0
                    && neighbour.1 <= max.1
                    && neighbour.2 >= 0
                    && neighbour.2 <= max.2
                    && !cubes.contains(&neighbour)
                    && !exterior.contains(&neighbour)
            })
        {
            if visited.contains(&neighbour) {
                continue;
            }
            visited.insert(neighbour);
            queue.push(neighbour);
        }
    }
}
