use std::collections::HashMap;

pub fn rock_paper_scissors(guide: &Vec<String>) -> i32 {
    let score_map = HashMap::from([('A', 1), ('B', 2), ('C', 3)]);

    guide
        .iter()
        .map(|strategy| {
            let mut chars = strategy.chars();
            get_score(
                &score_map,
                chars.next().expect("expected one character"),
                chars
                    .nth(1)
                    .expect("expected second character at third position in string"),
            )
        })
        .sum()
}

fn get_score(score_map: &HashMap<char, i32>, first: char, sec: char) -> i32 {
    let second = match sec {
        'X' => 'A',
        'Y' => 'B',
        'Z' => 'C',
        _ => panic!(),
    };

    let score = score_map[&second];

    if first == second {
        return 3 + score;
    }

    score
        + match (first, second) {
            ('A', 'B') | ('B', 'C') | ('C', 'A') => 6,
            ('A', 'C') | ('B', 'A') | ('C', 'B') => 0,
            _ => panic!(),
        }
}

pub fn part_two(guide: &Vec<String>) -> i32 {
    let score_map = HashMap::from([('A', 1), ('B', 2), ('C', 3)]);

    guide
        .iter()
        .map(|strategy| {
            let mut chars = strategy.chars();
            get_score_two(
                &score_map,
                chars.next().expect("expected one character"),
                chars
                    .nth(1)
                    .expect("expected second character at third position in string"),
            )
        })
        .sum()
}

fn get_score_two(score_map: &HashMap<char, i32>, first: char, second: char) -> i32 {
    if second == 'X' {
        return match first {
            'A' => 3,
            'B' => 1,
            'C' => 2,
            _ => panic!(),
        };
    }

    if second == 'Y' {
        return 3 + score_map[&first];
    }

    6 + match first {
        'A' => 2,
        'B' => 3,
        'C' => 1,
        _ => panic!(),
    }
}
