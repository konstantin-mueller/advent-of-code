pub fn tuning_trouble(datastream: &String, char_count: usize) -> usize {
    let mut current_chars = String::new();
    let mut skip_until = char_count;
    for (i, data) in datastream.chars().enumerate() {
        if i >= char_count {
            current_chars.remove(0);
        }

        if let Some(duplicate) = current_chars.rfind(data) {
            let mut new_skip = i + duplicate + 1;
            if i < char_count {
                new_skip += char_count - i - 1;
            }
            if new_skip > skip_until {
                skip_until = new_skip;
            }
        }

        current_chars.push(data);

        if skip_until > 0 && i < skip_until {
            if skip_until == i + 1 {
                skip_until = 0;
            }
            continue;
        }
        return i + 1;
    }
    panic!("No solution found!")
}
