use std::time::Instant;

mod resources;
mod s1;
mod s10;
mod s11;
mod s12;
mod s13;
mod s14;
mod s15;
mod s16;
mod s17;
mod s18;
mod s19;
mod s2;
mod s20;
mod s21;
mod s22;
mod s23;
mod s24;
mod s25;
mod s3;
mod s4;
mod s5;
mod s6;
mod s7;
mod s8;
mod s9;

fn main() {
    println!("Day 1");
    one();
    println!();

    println!("Day 2");
    two();
    println!();

    println!("Day 3");
    three();
    println!();

    println!("Day 4");
    four();
    println!();

    println!("Day 5");
    five();
    println!();

    println!("Day 6");
    six();
    println!();

    println!("Day 7");
    seven();
    println!();

    println!("Day 8");
    eight();
    println!();

    println!("Day 9");
    nine();
    println!();

    println!("Day 10");
    ten();
    println!();

    println!("Day 11");
    eleven();
    println!();

    println!("Day 12");
    twelve();
    println!();

    println!("Day 13");
    thirteen();
    println!();

    println!("Day 14");
    fourteen();
    println!();

    println!("Day 15");
    fifteen();
    println!();

    println!("Day 16");
    sixteen();
    println!();

    println!("Day 17");
    seventeen();
    println!();

    println!("Day 18");
    eighteen();
    println!();

    println!("Day 19");
    nineteen();
    println!();

    println!("Day 20");
    twenty();
    println!();

    println!("Day 21");
    twentyone();
    println!();

    println!("Day 22");
    twentytwo();
    println!();

    println!("Day 23");
    twentythree();
    println!();

    println!("Day 24");
    twentyfour();
    println!();

    println!("Day 25");
    twentyfive();
    println!();
}

fn timer<T>(func: &dyn Fn() -> T)
where
    T: std::fmt::Display,
{
    let mut times = Vec::new();
    for _ in 0..100 {
        let start = Instant::now();
        func();
        times.push(start.elapsed().as_micros());
    }

    println!(
        "100 iterations\tBest: {:.5}\tAverage: {:.5}\t(milliseconds)",
        *(times.iter().min().unwrap()) as f32 / 1000.0,
        times.iter().sum::<u128>() as f32 / times.len() as f32 / 1000.0
    );
    println!("Function result: {}\n\n", func());
}

fn one() {
    let calories = resources::as_list(include_bytes!("../resources/1"));

    timer(&|| s1::calorie_counting(&calories));
    timer(&|| s1::part_two(&calories));
}

fn two() {
    let guide = resources::as_list(include_bytes!("../resources/2"));

    timer(&|| s2::rock_paper_scissors(&guide));
    timer(&|| s2::part_two(&guide));
}

fn three() {
    let rucksacks = resources::as_list(include_bytes!("../resources/3"));

    timer(&|| s3::rucksack_reorganization(&rucksacks));
    timer(&|| s3::part_two(&rucksacks));
}

fn four() {
    let assignments = resources::as_list(include_bytes!("../resources/4"));

    timer(&|| s4::camp_cleanup(&assignments));
    timer(&|| s4::part_two(&assignments));
}

fn five() {
    let drawing = resources::as_list(include_bytes!("../resources/5"));

    timer(&|| s5::supply_stacks(&drawing).unwrap());
    timer(&|| s5::part_two(&drawing).unwrap());
}

fn six() {
    let datastream = resources::as_string(include_bytes!("../resources/6"));

    timer(&|| s6::tuning_trouble(&datastream, 4));
    timer(&|| s6::tuning_trouble(&datastream, 14));
}

fn seven() {
    let terminal_output = resources::as_list(include_bytes!("../resources/7"));

    timer(&|| s7::no_space_left_on_device(&terminal_output).unwrap());
    timer(&|| s7::part_two(&terminal_output).unwrap());
}

fn eight() {
    let map = resources::as_list(include_bytes!("../resources/8"));

    println!("{}", s8::treetop_tree_house(&map));
    println!("{}", s8::part_two(&map));
}

fn nine() {
    let motions = resources::as_list(include_bytes!("../resources/9"));

    println!("{}", s9::rope_bridge(&motions, 2).unwrap());
    println!("{}", s9::rope_bridge(&motions, 10).unwrap());
}

fn ten() {
    let instructions = resources::as_list(include_bytes!("../resources/10"));

    timer(&|| s10::cathode_ray_tube(&instructions).unwrap());
    timer(&|| String::from("\n") + &s10::part_two(&instructions).unwrap());
}

fn eleven() {
    let notes = resources::as_string(include_bytes!("../resources/11"));

    println!("{}", s11::monkey_in_the_middle(&notes, false));
    println!("{}", s11::monkey_in_the_middle(&notes, true));
}

fn twelve() {
    let heightmap = resources::as_list(include_bytes!("../resources/12"));

    println!("{}", s12::hill_climbing_algorithm(&heightmap, false));
    println!("{}", s12::hill_climbing_algorithm(&heightmap, true));
}

fn thirteen() {
    let packets = resources::as_string(include_bytes!("../resources/13"));

    timer(&|| s13::distress_signal(&packets));
    println!("{}", s13::part_two(&packets));
}

fn fourteen() {
    let cave = resources::as_list(include_bytes!("../resources/14"));

    println!("{}", s14::regolith_reservoir(&cave, false));
    println!("{}", s14::regolith_reservoir(&cave, true));
}

fn fifteen() {
    let sensors_and_beacons = resources::as_list(include_bytes!("../resources/15"));

    println!("{}", s15::beacon_exclusion_zone(&sensors_and_beacons));
    println!("{}", s15::part_two(&sensors_and_beacons));
}

fn sixteen() {
    let report = resources::as_list(include_bytes!("../resources/16"));

    println!("{}", s16::proboscidea_volcanium(&report, 30, false));
    println!("{}", s16::proboscidea_volcanium(&report, 26, true));
}

fn seventeen() {
    let jet_pattern = resources::as_string(include_bytes!("../resources/17")).replace("\n", "");

    println!("{}", s17::pyroclastic_flow(&jet_pattern, 2022));
    println!("{}", s17::pyroclastic_flow(&jet_pattern, 1_000_000_000_000));
}

fn eighteen() {
    let droplet = resources::as_list(include_bytes!("../resources/18"));

    println!("{}", s18::boiling_boulders(&droplet));
    println!("{}", s18::part_two(&droplet));
}

fn nineteen() {
    let blueprints = resources::as_list(include_bytes!("../resources/19"));

    println!("{}", s19::not_enough_minerals(&blueprints, 24, false));
    println!(
        "{}",
        s19::not_enough_minerals(&blueprints.iter().cloned().take(3).collect(), 32, true)
    );
}

fn twenty() {
    let file = resources::as_list::<i64>(include_bytes!("../resources/20"));

    timer(&|| s20::grove_positioning_system(&file, 1, 1));
    println!("{}", s20::grove_positioning_system(&file, 811589153, 10));
}

fn twentyone() {
    let monkeys = resources::as_list(include_bytes!("../resources/21"));

    timer(&|| s21::monkey_math(&monkeys, false));
    timer(&|| s21::monkey_math(&monkeys, true));
}

fn twentytwo() {
    let notes = resources::as_list(include_bytes!("../resources/22"));

    timer(&|| s22::monkey_map(&notes));
}

fn twentythree() {
    let grove = resources::as_list(include_bytes!("../resources/23"));

    timer(&|| s23::unstable_diffusion(&grove, false));
    println!("{}", s23::unstable_diffusion(&grove, true));
}

fn twentyfour() {
    let valley = resources::as_list(include_bytes!("../resources/24"));

    println!("{}", s24::blizzard_basin(&valley, false));
    println!("{}", s24::blizzard_basin(&valley, true));
}

fn twentyfive() {
    let fuel_requirements = resources::as_list(include_bytes!("../resources/25"));

    timer(&|| s25::full_of_hot_air(&fuel_requirements));
}
