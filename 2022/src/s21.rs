use std::collections::{HashMap, HashSet};

pub fn monkey_math(monkey_input: &Vec<String>, part_two: bool) -> i64 {
    let mut monkeys: HashMap<String, i64> = monkey_input
        .iter()
        .filter(|monkey| monkey.chars().last().unwrap().is_digit(10))
        .map(|monkey| {
            let mut split_monkey = monkey.split(": ");
            (
                split_monkey.next().unwrap().to_string(),
                split_monkey.next().unwrap().parse().unwrap(),
            )
        })
        .collect();

    if !part_two {
        return monkey_number(
            monkey_input
                .iter()
                .find(|monkey| monkey.starts_with("root"))
                .unwrap(),
            &mut monkeys,
            monkey_input,
        );
    }

    // part two
    let mut humn_path: HashSet<String> = HashSet::new();
    let mut search_monkey = String::from("humn");
    humn_path.insert(search_monkey.clone());
    loop {
        let found = monkey_input
            .iter()
            .find(|monkey| !monkey.starts_with(&search_monkey) && monkey.contains(&search_monkey))
            .unwrap();
        if found.starts_with("root") {
            break;
        }

        search_monkey = found.split(": ").next().unwrap().to_string();
        humn_path.insert(search_monkey.clone());
    }

    let mut next_monkey = monkey_input
        .iter()
        .find(|monkey| monkey.starts_with("root"))
        .unwrap()
        .split(": ")
        .nth(1)
        .unwrap()
        .split_whitespace();
    let first = next_monkey.next().unwrap().to_string();
    let second = next_monkey.nth(1).unwrap().to_string();

    let first_contains_humn = humn_path.contains(&first);

    // get result of monkey that does not use humn
    let not_humn_result = monkey_number(
        monkey_input
            .iter()
            .find(|monkey| monkey.starts_with(if first_contains_humn { &second } else { &first }))
            .unwrap(),
        &mut monkeys,
        monkey_input,
    );

    humn_number(
        monkey_input
            .iter()
            .find(|monkey| monkey.starts_with(if first_contains_humn { &first } else { &second }))
            .unwrap(),
        &mut monkeys,
        monkey_input,
        not_humn_result,
        humn_path,
    )
}

fn monkey_number<'a>(
    monkey: &String,
    monkeys: &'a mut HashMap<String, i64>,
    monkey_input: &Vec<String>,
) -> i64 {
    let mut split = monkey.split(": ");
    let current_monkey = split.next().unwrap().to_string();
    if monkeys.contains_key(&current_monkey) {
        return monkeys[&current_monkey];
    }

    split = split.next().unwrap().split(" ");
    let first_monkey = split.next().unwrap().to_string();
    let first = monkey_number(
        &monkey_input
            .iter()
            .find(|m| m.starts_with(&first_monkey))
            .unwrap(),
        monkeys,
        monkey_input,
    );

    let operator = split.next().unwrap();

    let second_monkey = split.next().unwrap().to_string();
    let second = monkey_number(
        &monkey_input
            .iter()
            .find(|m| m.starts_with(&second_monkey))
            .unwrap(),
        monkeys,
        monkey_input,
    );

    let result = match operator {
        "+" => first + second,
        "-" => first - second,
        "*" => first * second,
        "/" => first / second,
        _ => panic!("unkown operator!"),
    };

    monkeys.insert(current_monkey, result);
    result
}

fn humn_number<'a>(
    monkey: &String,
    monkeys: &'a mut HashMap<String, i64>,
    monkey_input: &Vec<String>,
    expected_result: i64,
    humn_path: HashSet<String>,
) -> i64 {
    let mut split = monkey.split(": ");
    let current_monkey = split.next().unwrap().to_string();
    if current_monkey == "humn" {
        return expected_result;
    }

    split = split.next().unwrap().split(" ");

    let first_monkey = split.next().unwrap().to_string();
    let operator = split.next().unwrap();
    let second_monkey = split.next().unwrap().to_string();

    let first_contains_humn = humn_path.contains(&first_monkey);

    // get result of monkey that does not use humn
    let not_humn_result = monkey_number(
        monkey_input
            .iter()
            .find(|monkey| {
                monkey.starts_with(if first_contains_humn {
                    &second_monkey
                } else {
                    &first_monkey
                })
            })
            .unwrap(),
        monkeys,
        monkey_input,
    );

    let new_expected_result = match operator {
        "+" => expected_result - not_humn_result,
        "-" => {
            if first_contains_humn {
                expected_result + not_humn_result
            } else {
                not_humn_result - expected_result
            }
        }
        "*" => expected_result / not_humn_result,
        "/" => {
            if first_contains_humn {
                expected_result * not_humn_result
            } else {
                not_humn_result / expected_result
            }
        }
        _ => panic!("unkown operator!"),
    };

    humn_number(
        monkey_input
            .iter()
            .find(|monkey| {
                monkey.starts_with(if first_contains_humn {
                    &first_monkey
                } else {
                    &second_monkey
                })
            })
            .unwrap(),
        monkeys,
        monkey_input,
        new_expected_result,
        humn_path,
    )
}
