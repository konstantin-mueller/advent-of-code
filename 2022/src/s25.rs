pub fn full_of_hot_air(fuel_requirements: &Vec<String>) -> String {
    let sum: isize = fuel_requirements.iter().map(snafu_to_decimal).sum();
    decimal_to_snafu(sum)
}

fn snafu_to_decimal(snafu: &String) -> isize {
    snafu
        .chars()
        .rev()
        .enumerate()
        .map(|(i, digit)| {
            5_isize.pow(i as u32)
                * match digit {
                    '=' => -2,
                    '-' => -1,
                    _ => digit.to_digit(10).unwrap() as isize,
                }
        })
        .sum()
}

fn decimal_to_snafu(mut decimal: isize) -> String {
    let mut snafu = String::new();
    let mut remainder = 0;

    while decimal > 0 {
        snafu = match decimal % 5 + remainder {
            i if i <= 2 => {
                remainder = 0;
                i.to_string()
            }
            3 => {
                remainder = 1;
                "=".to_string()
            }
            4 => {
                remainder = 1;
                "-".to_string()
            }
            5 => {
                remainder = 1;
                "0".to_string()
            }
            _ => unreachable!(),
        } + &snafu;

        decimal /= 5;
    }

    snafu
}
