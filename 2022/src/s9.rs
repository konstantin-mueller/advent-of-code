use std::collections::HashSet;

use anyhow::Result;

pub fn rope_bridge(motions: &Vec<String>, number_of_knots: usize) -> Result<usize> {
    let mut knots = vec![(0, 0); number_of_knots];
    let mut visited: HashSet<(i32, i32)> = HashSet::new();
    visited.insert((0, 0));

    for motion in motions {
        let number = motion
            .split_whitespace()
            .last()
            .expect("expected motion to contain two values separated by a space")
            .parse()?;

        let direction = match motion
            .chars()
            .next()
            .expect("expected motion to contain two values separated by a space")
        {
            'U' => (0, 1),
            'R' => (1, 0),
            'D' => (0, -1),
            'L' => (-1, 0),
            _ => panic!("expected direction of motion to be one of U, R, D or L"),
        };

        for _ in 0..number {
            knots[0].0 += direction.0;
            knots[0].1 += direction.1;

            for i in 1..knots.len() {
                let difference_x = knots[i - 1].0 - knots[i].0;
                let difference_y = knots[i - 1].1 - knots[i].1;

                if i32::abs(difference_x) >= 2 || i32::abs(difference_y) >= 2 {
                    if difference_x != 0 {
                        knots[i].0 += difference_x / i32::abs(difference_x);
                    }

                    if difference_y != 0 {
                        knots[i].1 += difference_y / i32::abs(difference_y);
                    }

                    if i == knots.len() - 1 {
                        visited.insert(knots[i]);
                    }
                }
            }
        }
    }

    Ok(visited.len())
}
