use std::cmp::Ordering;

use serde_json::Value;

pub fn distress_signal(packets: &String) -> usize {
    let mut result = 0;

    for (i, packet) in packets.split("\n\n").enumerate() {
        let mut pair = packet.split("\n");

        let first: Value = serde_json::from_str(
            pair.next()
                .expect("expected two packets to be separated by \\n"),
        )
        .expect("expected packets to be valid JSON arrays");

        let second: Value = serde_json::from_str(
            pair.next()
                .expect("expected two packets to be separated by \\n"),
        )
        .expect("expected packets to be valid JSON arrays");

        if compare(&first, &second) == Ordering::Less {
            result += i + 1;
        }
    }

    result
}

pub fn part_two(packets: &String) -> usize {
    let mut all_packets = format!("{}[[2]]\n[[6]]", packets.replace("\n\n", "\n"))
        .split("\n")
        .map(|packet| {
            serde_json::from_str(packet).expect("expected packets to be valid JSON arrays")
        })
        .collect::<Vec<_>>();

    all_packets.sort_by(compare);

    all_packets
        .iter()
        .enumerate()
        .filter(|(_, packet)| packet.to_string() == "[[2]]" || packet.to_string() == "[[6]]")
        .map(|(i, _)| i + 1)
        .product()
}

fn compare(first: &Value, second: &Value) -> Ordering {
    match (first, second) {
        (Value::Number(first), Value::Number(second)) => {
            if first.as_u64() < second.as_u64() {
                Ordering::Less
            } else if first.as_u64() == second.as_u64() {
                Ordering::Equal
            } else {
                Ordering::Greater
            }
        }
        (Value::Number(_), Value::Array(_)) => compare(&Value::Array(vec![first.clone()]), second),
        (Value::Array(_), Value::Number(_)) => compare(first, &Value::Array(vec![second.clone()])),
        (Value::Array(first), Value::Array(second)) => {
            let mut first_array = first.iter();

            for second_item in second.iter() {
                let first_item = first_array.next();

                if first_item.is_none() {
                    return Ordering::Less;
                } else {
                    let result = compare(first_item.unwrap(), second_item);
                    if result == Ordering::Less || result == Ordering::Greater {
                        return result;
                    }
                }
            }

            if first_array.next().is_none() {
                return Ordering::Equal;
            }
            Ordering::Greater
        }
        _ => panic!("Could not compare values!"),
    }
}
