use std::collections::HashSet;

pub fn regolith_reservoir(cave: &Vec<String>, part_two: bool) -> usize {
    let mut rocks: HashSet<(usize, usize)> = HashSet::new();

    for line in cave {
        let mut path = line.split(" -> ").map(|position| {
            let mut numbers = position.split(",");
            (
                numbers.next().unwrap().parse().unwrap(),
                numbers.next().unwrap().parse::<usize>().unwrap(),
            )
        });

        let mut previous = path.next().unwrap();
        for current in path {
            if previous.0 == current.0 {
                let min = previous.1.min(current.1);
                let max = previous.1.max(current.1);
                rocks.extend((min..max + 1).map(|y| (previous.0, y)));
            } else {
                let min = previous.0.min(current.0);
                let max = previous.0.max(current.0);
                rocks.extend((min..max + 1).map(|x| (x, previous.1)));
            }
            previous = current;
        }
    }

    let max_y;
    if part_two {
        max_y = rocks.iter().map(|(_, y)| y).max().unwrap().to_owned() + 2;
        rocks.extend((0..1000).map(|x| (x, max_y)));
    } else {
        max_y = rocks.iter().map(|(_, y)| y).max().unwrap().to_owned();
    }

    let mut sand_count = 0;
    'outer: loop {
        let mut sand = (500, 0);
        loop {
            sand.1 += 1;

            if rocks.contains(&sand) {
                sand.0 -= 1;

                if rocks.contains(&sand) {
                    sand.0 += 2;

                    if rocks.contains(&sand) {
                        sand.0 -= 1;
                        sand.1 -= 1;

                        if part_two && sand == (500, 0) {
                            sand_count += 1;
                            break 'outer;
                        }

                        rocks.insert(sand);
                        sand_count += 1;
                        break;
                    }
                }
            }

            if !part_two && sand.1 > max_y {
                break 'outer;
            }
        }
    }

    sand_count
}
