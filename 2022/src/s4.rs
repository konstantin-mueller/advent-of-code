pub fn camp_cleanup(assignments: &Vec<String>) -> usize {
    assignments
        .iter()
        .filter(|line| {
            let (first, second) = line
                .split_once(",")
                .expect("expected assignment to contain one comma");

            let one: i32 = first
                .split("-")
                .next()
                .expect("expected assignment to contain numbers separated by minus")
                .parse()
                .expect("expected assignment sections to be numbers");
            let two: i32 = first
                .split("-")
                .nth(1)
                .expect("expected assignment to contain numbers separated by minus")
                .parse()
                .expect("expected assignment sections to be numbers");
            let three: i32 = second
                .split("-")
                .next()
                .expect("expected assignment to contain numbers separated by minus")
                .parse()
                .expect("expected assignment sections to be numbers");
            let four: i32 = second
                .split("-")
                .nth(1)
                .expect("expected assignment to contain numbers separated by minus")
                .parse()
                .expect("expected assignment sections to be numbers");

            (one <= three && two >= four) || (three <= one && four >= two)
        })
        .count()
}

pub fn part_two(assignments: &Vec<String>) -> usize {
    assignments
        .iter()
        .filter(|line| {
            let (first, second) = line
                .split_once(",")
                .expect("expected assignment to contain one comma");

            let one: i32 = first
                .split("-")
                .next()
                .expect("expected assignment to contain numbers separated by minus")
                .parse()
                .expect("expected assignment sections to be numbers");
            let two: i32 = first
                .split("-")
                .nth(1)
                .expect("expected assignment to contain numbers separated by minus")
                .parse()
                .expect("expected assignment sections to be numbers");
            let three: i32 = second
                .split("-")
                .next()
                .expect("expected assignment to contain numbers separated by minus")
                .parse()
                .expect("expected assignment sections to be numbers");
            let four: i32 = second
                .split("-")
                .nth(1)
                .expect("expected assignment to contain numbers separated by minus")
                .parse()
                .expect("expected assignment sections to be numbers");

            (one >= three && one <= four)
                || (two >= three && two <= four)
                || (three >= one && three <= two)
                || (four >= one && four <= two)
        })
        .count()
}
