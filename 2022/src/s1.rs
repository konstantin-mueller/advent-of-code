use std::collections::HashSet;

pub fn calorie_counting(calories: &Vec<String>) -> i32 {
    let mut current_calories = 0;
    let mut max_calories = 0;

    for calory in calories {
        if calory.is_empty() {
            if current_calories > max_calories {
                max_calories = current_calories;
            }
            current_calories = 0;
            continue;
        }

        current_calories += calory
            .parse::<i32>()
            .expect("expected number but could not parse");
    }

    max_calories
}

pub fn part_two(calories: &Vec<String>) -> i32 {
    let mut current_calories = 0;
    let mut max_calories = HashSet::new();
    max_calories.insert(0);
    max_calories.insert(1);
    max_calories.insert(2);

    let mut current_min = 0;

    for calory in calories {
        if calory.is_empty() {
            if current_calories > current_min {
                max_calories.remove(&current_min);
                max_calories.insert(current_calories);
                current_min = *max_calories
                    .iter()
                    .min()
                    .expect("expected value in max_calories");
            }

            current_calories = 0;
            continue;
        }

        current_calories += calory
            .parse::<i32>()
            .expect("expected number but could not parse");
    }

    max_calories.iter().sum()
}
