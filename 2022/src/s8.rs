pub fn treetop_tree_house(map: &Vec<String>) -> usize {
    let mut visible_trees = 0;

    let columns: Vec<Vec<u32>> = (0..map[0].len())
        .map(|i| {
            (0..map.len())
                .map(|j| {
                    map[j]
                        .chars()
                        .nth(i)
                        .expect("tree index out of range")
                        .to_digit(10)
                        .expect("expected trees to be numbers")
                })
                .collect::<Vec<u32>>()
        })
        .collect();

    for i in 1..map.len() - 1 {
        let row: Vec<u32> = map[i]
            .chars()
            .map(|tree| tree.to_digit(10).expect("expected trees to be numbers"))
            .collect();

        for j in 1..row.len() - 1 {
            // top
            if columns[j][0..i].iter().all(|tree| tree < &row[j]) {
                visible_trees += 1;
                continue;
            }

            // right
            if row[j + 1..row.len()].iter().all(|tree| tree < &row[j]) {
                visible_trees += 1;
                continue;
            }

            // bottom
            if columns[j][i + 1..map.len()]
                .iter()
                .all(|tree| tree < &row[j])
            {
                visible_trees += 1;
                continue;
            }

            // left
            if row[0..j].iter().all(|tree| tree < &row[j]) {
                visible_trees += 1;
            }
        }
    }

    map.len() * 2 + map[0].len() * 2 - 4 + visible_trees
}

pub fn part_two(map: &Vec<String>) -> usize {
    let mut score = 0;

    let columns: Vec<Vec<u32>> = (0..map[0].len())
        .map(|i| {
            (0..map.len())
                .map(|j| {
                    map[j]
                        .chars()
                        .nth(i)
                        .expect("tree index out of range")
                        .to_digit(10)
                        .expect("expected trees to be numbers")
                })
                .collect::<Vec<u32>>()
        })
        .collect();

    for i in 1..map.len() - 1 {
        let row: Vec<u32> = map[i]
            .chars()
            .map(|tree| tree.to_digit(10).expect("expected trees to be numbers"))
            .collect();

        for j in 1..row.len() - 1 {
            let mut top = columns[j][0..i]
                .iter()
                .rev()
                .take_while(|tree| tree < &&row[j])
                .count();
            if top < i {
                top += 1;
            }

            let mut right = row[j + 1..row.len()]
                .iter()
                .take_while(|tree| tree < &&row[j])
                .count();
            if right < row.len() - (j + 1) {
                right += 1;
            }

            let mut bottom = columns[j][i + 1..map.len()]
                .iter()
                .take_while(|tree| tree < &&row[j])
                .count();
            if bottom < map.len() - (i + 1) {
                bottom += 1;
            }

            let mut left = row[0..j]
                .iter()
                .rev()
                .take_while(|tree| tree < &&row[j])
                .count();
            if left < j {
                left += 1;
            }

            let result = top * right * bottom * left;
            if result > score {
                score = result;
            }
        }
    }

    score
}
