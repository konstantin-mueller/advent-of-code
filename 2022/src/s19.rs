use std::collections::{HashMap, HashSet};

#[derive(Clone, Copy)]
enum Resource {
    ORE,
    CLAY,
    OBSIDIAN,
    GEODE,
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
struct Ore {
    ore: usize,
    clay: usize,
    obsidian: usize,
    geode: usize,
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
struct Robots {
    ore: usize,
    clay: usize,
    obsidian: usize,
    geode: usize,
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
struct State {
    ore: [usize; 4],
    robots: [usize; 4],
}

struct Blueprint {
    ore_robot_ore_costs: usize,
    clay_robot_ore_costs: usize,
    obsidian_robot_ore_and_clay_costs: (usize, usize),
    geode_robot_ore_and_obsidian_costs: (usize, usize),
}

pub fn not_enough_minerals(blueprints: &Vec<String>, max_minutes: usize, part_two: bool) -> usize {
    let mut result = if part_two { 1 } else { 0 };
    for blueprint_str in blueprints {
        let mut split_blueprint = blueprint_str.split("costs ");
        let id_number: usize = split_blueprint
            .next()
            .unwrap()
            .split_whitespace()
            .nth(1)
            .unwrap()
            .replace(":", "")
            .parse()
            .unwrap();

        let blueprint = Blueprint {
            ore_robot_ore_costs: split_blueprint
                .next()
                .unwrap()
                .split_whitespace()
                .next()
                .unwrap()
                .parse()
                .unwrap(),
            clay_robot_ore_costs: split_blueprint
                .next()
                .unwrap()
                .split_whitespace()
                .next()
                .unwrap()
                .parse()
                .unwrap(),
            obsidian_robot_ore_and_clay_costs: {
                let mut obsidian_costs = split_blueprint.next().unwrap().split_whitespace();
                (
                    obsidian_costs.next().unwrap().parse().unwrap(),
                    obsidian_costs.nth(2).unwrap().parse().unwrap(),
                )
            },
            geode_robot_ore_and_obsidian_costs: {
                let mut geode_costs = split_blueprint.next().unwrap().split_whitespace();
                (
                    geode_costs.next().unwrap().parse().unwrap(),
                    geode_costs.nth(2).unwrap().parse().unwrap(),
                )
            },
        };

        let max_ore_build_consumption = [
            blueprint.ore_robot_ore_costs,
            blueprint.clay_robot_ore_costs,
            blueprint.obsidian_robot_ore_and_clay_costs.0,
            blueprint.geode_robot_ore_and_obsidian_costs.0,
        ]
        .into_iter()
        .max()
        .unwrap();

        let mut states: HashMap<usize, Vec<State>> = HashMap::new();
        states.insert(
            1,
            vec![State {
                ore: [0; 4],
                robots: [1, 0, 0, 0],
            }],
        );

        for minute in 1..max_minutes {
            if !states.contains_key(&minute) {
                continue;
            }

            while !states[&minute].is_empty() {
                let mut state = states.get_mut(&minute).unwrap().pop().unwrap();

                // ore
                build_robot(
                    &mut states,
                    state,
                    minute,
                    max_minutes,
                    Some(max_ore_build_consumption),
                    blueprint.ore_robot_ore_costs,
                    Resource::ORE as usize,
                    Resource::ORE as usize,
                    None,
                    None,
                );

                // clay
                build_robot(
                    &mut states,
                    state,
                    minute,
                    max_minutes,
                    Some(blueprint.obsidian_robot_ore_and_clay_costs.1),
                    blueprint.clay_robot_ore_costs,
                    Resource::CLAY as usize,
                    Resource::ORE as usize,
                    None,
                    None,
                );

                // obsidian
                build_robot(
                    &mut states,
                    state,
                    minute,
                    max_minutes,
                    Some(blueprint.geode_robot_ore_and_obsidian_costs.1),
                    blueprint.obsidian_robot_ore_and_clay_costs.0,
                    Resource::OBSIDIAN as usize,
                    Resource::ORE as usize,
                    Some(Resource::CLAY as usize),
                    Some(blueprint.obsidian_robot_ore_and_clay_costs.1),
                );

                // geode
                build_robot(
                    &mut states,
                    state,
                    minute,
                    max_minutes,
                    None,
                    blueprint.geode_robot_ore_and_obsidian_costs.0,
                    Resource::GEODE as usize,
                    Resource::ORE as usize,
                    Some(Resource::OBSIDIAN as usize),
                    Some(blueprint.geode_robot_ore_and_obsidian_costs.1),
                );

                collect_ore(&mut state, 1);
                states
                    .entry(minute + 1)
                    .and_modify(|states_vec| states_vec.push(state))
                    .or_insert(vec![state]);
            }

            // filter duplicates and solutions with low geodes
            states.entry(minute + 1).and_modify(|states_vec| {
                let tmp = states_vec.iter().cloned().collect::<HashSet<State>>();
                let max_geodes = tmp
                    .iter()
                    .max_by_key(|state| state.ore[Resource::GEODE as usize])
                    .unwrap()
                    .ore[Resource::GEODE as usize];
                states_vec.clear();
                states_vec.extend(tmp.into_iter().filter(|state| {
                    state.ore[Resource::GEODE as usize] == max_geodes
                        || state.ore[Resource::GEODE as usize] == max_geodes - 1
                }))
            });
        }

        // last minute
        let mut max_geodes = 0;
        for i in 0..states[&max_minutes].len() {
            let mut state = states[&max_minutes][i];
            collect_ore(&mut state, 1);
            if state.ore[Resource::GEODE as usize] > max_geodes {
                max_geodes = state.ore[Resource::GEODE as usize];
            }
        }

        if part_two {
            result *= max_geodes;
        } else {
            result += id_number * max_geodes;
        }
    }

    result
}

fn collect_ore(state: &mut State, minutes: usize) {
    state.ore[0] += state.robots[0] * minutes;
    state.ore[1] += state.robots[1] * minutes;
    state.ore[2] += state.robots[2] * minutes;
    state.ore[3] += state.robots[3] * minutes;
}

fn build_robot<'a>(
    states: &'a mut HashMap<usize, Vec<State>>,
    state: State,
    minute: usize,
    max_minutes: usize,
    max_ore_build_consumption: Option<usize>,
    robot_costs: usize,
    robot_to_build: usize,
    ore: usize,
    second_ore: Option<usize>,
    second_robot_costs: Option<usize>,
) {
    // only build more ore robots if it is possible to consume all of the produced ore by building robots with the highest ore consumption
    if max_ore_build_consumption.is_none()
        || state.robots[robot_to_build] < max_ore_build_consumption.unwrap()
    {
        // is it possible to build a robot now?
        if state.ore[ore] >= robot_costs
            && ((second_ore.is_none() && second_robot_costs.is_none())
                || state.ore[second_ore.unwrap()] >= second_robot_costs.unwrap())
        {
            let mut new_state = state.clone();
            collect_ore(&mut new_state, 1);
            new_state.ore[ore] -= robot_costs;
            if second_ore.is_some() && second_robot_costs.is_some() {
                new_state.ore[second_ore.unwrap()] -= second_robot_costs.unwrap();
            }
            new_state.robots[robot_to_build] += 1;

            states
                .entry(minute + 1)
                .and_modify(|states_vec| states_vec.push(new_state))
                .or_insert(vec![new_state]);
        } else if (second_ore.is_none() && second_robot_costs.is_none())
            || state.robots[second_ore.unwrap()] > 0
        {
            // build robot later as soon as possible
            let mut new_minute = (robot_costs - state.ore[ore]) * state.robots[ore];
            if second_ore.is_some() && second_robot_costs.is_some() {
                new_minute = new_minute.max(
                    (second_robot_costs.unwrap() - state.ore[second_ore.unwrap()])
                        * state.robots[second_ore.unwrap()],
                );
            }

            if minute + new_minute < max_minutes {
                let mut new_state = state.clone();
                collect_ore(&mut new_state, new_minute);

                states
                    .entry(minute + new_minute)
                    .and_modify(|states_vec| states_vec.push(new_state))
                    .or_insert(vec![new_state]);
            }
        }
    }
}
