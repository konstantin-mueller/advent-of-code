use std::collections::BTreeSet;
use std::str::FromStr;

pub fn as_list<T: FromStr>(contents: &[u8]) -> Vec<T> {
    String::from_utf8_lossy(contents)
        .lines()
        .filter_map(|line| line.parse::<T>().ok())
        .collect()
}

pub fn as_int_set(contents: &[u8]) -> BTreeSet<i32> {
    String::from_utf8_lossy(contents)
        .lines()
        .map(|line| line.parse::<i32>().unwrap())
        .collect()
}

pub fn as_list_of_lists(contents: &[u8]) -> Vec<Vec<char>> {
    String::from_utf8_lossy(contents)
        .lines()
        .map(|line| line.chars().collect())
        .collect()
}

pub fn as_string(contents: &[u8]) -> String {
    String::from_utf8_lossy(contents).to_string()
}
