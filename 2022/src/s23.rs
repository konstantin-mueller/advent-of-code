use std::collections::{HashMap, HashSet, VecDeque};

#[derive(Debug)]
enum Direction {
    NORTH,
    SOUTH,
    WEST,
    EAST,
}

pub fn unstable_diffusion(grove: &Vec<String>, part_two: bool) -> i32 {
    let mut elves: HashSet<(i32, i32)> = grove
        .iter()
        .enumerate()
        .flat_map(|(y, line)| {
            line.chars()
                .enumerate()
                .filter(|(_, c)| c == &'#')
                .map(move |(x, _)| (x as i32, y as i32))
        })
        .collect();

    let mut directions = VecDeque::from([
        Direction::NORTH,
        Direction::SOUTH,
        Direction::WEST,
        Direction::EAST,
    ]);

    for i in 0..if part_two { 10000 } else { 10 } {
        let mut proposed_positions = HashMap::new();
        let mut elves_moved = false;

        for elf in elves.iter() {
            let (x, y) = *elf;
            let north_neighbours = elves.contains(&(x - 1, y - 1))
                || elves.contains(&(x, y - 1))
                || elves.contains(&(x + 1, y - 1));
            let south_neighbours = elves.contains(&(x - 1, y + 1))
                || elves.contains(&(x, y + 1))
                || elves.contains(&(x + 1, y + 1));
            let west_neighbours = elves.contains(&(x - 1, y - 1))
                || elves.contains(&(x - 1, y))
                || elves.contains(&(x - 1, y + 1));
            let east_neighbours = elves.contains(&(x + 1, y - 1))
                || elves.contains(&(x + 1, y))
                || elves.contains(&(x + 1, y + 1));

            if (!north_neighbours && !south_neighbours && !west_neighbours && !east_neighbours)
                || (north_neighbours && south_neighbours && west_neighbours && east_neighbours)
            {
                proposed_positions.insert(*elf, *elf);
            } else {
                for direction in directions.iter() {
                    match direction {
                        Direction::NORTH if !north_neighbours => {
                            elves_moved = set_proposed_position(
                                &mut proposed_positions,
                                elf,
                                (x, y - 1),
                                elves_moved,
                            );
                            break;
                        }
                        Direction::SOUTH if !south_neighbours => {
                            elves_moved = set_proposed_position(
                                &mut proposed_positions,
                                elf,
                                (x, y + 1),
                                elves_moved,
                            );
                            break;
                        }
                        Direction::WEST if !west_neighbours => {
                            elves_moved = set_proposed_position(
                                &mut proposed_positions,
                                elf,
                                (x - 1, y),
                                elves_moved,
                            );
                            break;
                        }
                        Direction::EAST if !east_neighbours => {
                            elves_moved = set_proposed_position(
                                &mut proposed_positions,
                                elf,
                                (x + 1, y),
                                elves_moved,
                            );
                            break;
                        }
                        _ => continue,
                    }
                }
            }
        }

        if part_two && !elves_moved {
            return i + 1;
        }

        let first_direction = directions.pop_front().unwrap();
        directions.push_back(first_direction);

        elves = proposed_positions
            .into_iter()
            .map(|(_, proposed)| proposed)
            .collect();
    }

    if part_two {
        panic!("No solution found!");
    }

    let min_x = elves.iter().min_by_key(|(x, _)| x).unwrap().0;
    let max_x = elves.iter().max_by_key(|(x, _)| x).unwrap().0;
    let min_y = elves.iter().min_by_key(|(_, y)| y).unwrap().1;
    let max_y = elves.iter().max_by_key(|(_, y)| y).unwrap().1;
    (max_x - min_x + 1) * (max_y - min_y + 1) - elves.len() as i32
}

fn set_proposed_position<'a>(
    proposed_positions: &'a mut HashMap<(i32, i32), (i32, i32)>,
    elf: &(i32, i32),
    proposed_position: (i32, i32),
    elves_moved: bool,
) -> bool {
    let existing = proposed_positions
        .iter()
        .find(|(_, prop)| **prop == proposed_position);

    if existing.is_none() {
        proposed_positions.insert(*elf, proposed_position);
        return true;
    } else {
        proposed_positions.insert(*existing.unwrap().0, *existing.unwrap().0);
        proposed_positions.insert(*elf, *elf);
    }
    elves_moved
}
