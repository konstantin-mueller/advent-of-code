struct Sensor {
    pos: (i32, i32),
    beacon: (i32, i32),
    distance: i32,
}

pub fn beacon_exclusion_zone(sensors_and_beacons: &Vec<String>) -> usize {
    let mut x_min = i32::MAX;
    let mut x_max = i32::MIN;

    let sensors: Vec<Sensor> = sensors_and_beacons
        .iter()
        .map(|line| {
            let replaced = line
                .replace("Sensor at x=", "")
                .replace(": closest beacon is at x=", ", y=");
            let mut numbers = replaced
                .split(", y=")
                .map(|number| number.parse::<i32>().unwrap());
            let pos = (numbers.next().unwrap(), numbers.next().unwrap());

            let beacon = (numbers.next().unwrap(), numbers.next().unwrap());
            if beacon.0 < x_min {
                x_min = beacon.0;
            }
            if beacon.0 > x_max {
                x_max = beacon.0;
            }

            Sensor {
                pos,
                beacon,
                distance: (pos.0 - beacon.0).abs() + (pos.1 - beacon.1).abs(),
            }
        })
        .collect();

    (x_min * 2..=x_max * 2)
        .filter(|x| {
            sensors.iter().any(|sensor| {
                sensor.beacon != (*x, 2000000)
                    && (sensor.pos.0 - x).abs() + (sensor.pos.1 - 2000000).abs() <= sensor.distance
            })
        })
        .count()
}

pub fn part_two(sensors_and_beacons: &Vec<String>) -> usize {
    let sensors: Vec<Sensor> = sensors_and_beacons
        .iter()
        .map(|line| {
            let replaced = line
                .replace("Sensor at x=", "")
                .replace(": closest beacon is at x=", ", y=");
            let mut numbers = replaced
                .split(", y=")
                .map(|number| number.parse::<i32>().unwrap());
            let pos = (numbers.next().unwrap(), numbers.next().unwrap());

            let beacon = (numbers.next().unwrap(), numbers.next().unwrap());

            Sensor {
                pos,
                beacon,
                distance: ((pos.0 - beacon.0).abs() + (pos.1 - beacon.1).abs()) + 1,
            }
        })
        .collect();

    (0..sensors.len())
        .find_map(|i| search_along_edges(i, &sensors, 4000000))
        .unwrap()
}

fn search_along_edges(sensor_index: usize, sensors: &Vec<Sensor>, max: i32) -> Option<usize> {
    let sensor = &sensors[sensor_index];
    (-sensor.distance..=sensor.distance).find_map(|coord| {
        let t = if coord < 0 {
            sensor.distance + coord
        } else {
            sensor.distance - coord
        };

        let x = (sensor.pos.0 + coord, sensor.pos.1 + t);
        let is_x_valid = x.0 >= 0
            && x.1 >= 0
            && x.0 <= max
            && x.1 <= max
            && !sensors.iter().any(|sensor| {
                ((sensor.pos.0 - x.0).abs() + (sensor.pos.1 - x.1).abs()) < sensor.distance
            });

        let y = (sensor.pos.0 + t, sensor.pos.1 + coord);
        let is_y_valid = y.0 >= 0
            && y.1 >= 0
            && y.0 <= max
            && y.1 <= max
            && !sensors.iter().any(|sensor| {
                ((sensor.pos.0 - y.0).abs() + (sensor.pos.1 - y.1).abs()) < sensor.distance
            });

        if !is_x_valid && !is_y_valid {
            None
        } else if is_y_valid {
            Some(y.0 as usize * max as usize + y.1 as usize)
        } else {
            Some(x.0 as usize * max as usize + x.1 as usize)
        }
    })
}
