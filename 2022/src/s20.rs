pub fn grove_positioning_system(
    file: &Vec<i64>,
    multiply_numbers_with: i64,
    number_of_mixes: usize,
) -> i64 {
    let mut numbers: Vec<(usize, i64)> = file
        .iter()
        .map(|number| *number * multiply_numbers_with)
        .enumerate()
        .collect();

    let numbers_copy: Vec<(usize, i64)> = numbers.iter().cloned().collect();

    let numbers_len = numbers.len() as i64 - 1;
    for _ in 0..number_of_mixes {
        for num in numbers_copy.iter() {
            let index = numbers.iter().position(|n| n == num).unwrap();

            let number = numbers.remove(index);

            let mut new_index = index as i64 + number.1;
            // Python's modulo calculation which works for negative numbers as well
            new_index -= (new_index as f64 / numbers_len as f64).floor() as i64 * numbers_len;

            numbers.insert(new_index as usize, number);
        }
    }

    let mut cycle = numbers
        .iter()
        .skip(numbers.iter().position(|(_, number)| *number == 0).unwrap() + 1)
        .chain(numbers.iter().cycle());

    cycle.nth(999).unwrap().1 + cycle.nth(999).unwrap().1 + cycle.nth(999).unwrap().1
}
