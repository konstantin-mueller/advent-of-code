use std::collections::HashSet;

pub fn blizzard_basin(valley: &Vec<String>, part_two: bool) -> isize {
    let height = valley.len() as isize - 2;
    let width = valley[0].len() as isize - 2;

    let directions: Vec<(isize, isize)> = valley
        .iter()
        .flat_map(|line| {
            line.chars()
                .filter(|c| *c != '#' && *c != '.')
                .map(move |c| match c {
                    '^' => (0, -1),
                    '>' => (1, 0),
                    '<' => (-1, 0),
                    'v' => (0, 1),
                    _ => unreachable!(),
                })
        })
        .collect();

    let blizzards: Vec<(isize, isize)> = valley
        .iter()
        .enumerate()
        .flat_map(|(y, line)| {
            line.chars()
                .enumerate()
                .filter(|(_, c)| *c != '#' && *c != '.')
                .map(move |(x, _)| (x as isize, y as isize))
        })
        .collect();

    let start = (
        valley
            .iter()
            .next()
            .unwrap()
            .split(".")
            .next()
            .unwrap()
            .len() as isize,
        0,
    );
    let end = (
        valley
            .iter()
            .last()
            .unwrap()
            .split(".")
            .next()
            .unwrap()
            .len() as isize,
        valley.iter().count() as isize - 1,
    );

    let (first_result, blizzards) = find_path(&blizzards, &directions, start, end, width, height);
    if !part_two {
        return first_result;
    }

    let (second_result, blizzards) = find_path(&blizzards, &directions, end, start, width, height);
    first_result + second_result + find_path(&blizzards, &directions, start, end, width, height).0
}

fn find_path(
    blizzard_map: &Vec<(isize, isize)>,
    directions: &Vec<(isize, isize)>,
    start: (isize, isize),
    end: (isize, isize),
    width: isize,
    height: isize,
) -> (isize, Vec<(isize, isize)>) {
    let mut blizzards = blizzard_map.clone();

    let mut queue: HashSet<(isize, isize)> = HashSet::new();
    queue.insert(start);

    let mut minutes = 0;
    loop {
        if queue.contains(&end) {
            break;
        }

        blizzards = blizzards
            .iter()
            .enumerate()
            .map(|(i, (x, y))| {
                let new_x = x + directions[i].0;
                let new_y = y + directions[i].1;
                if new_x <= 0 {
                    (width, new_y)
                } else if new_x > width {
                    (1, new_y)
                } else if new_y <= 0 {
                    (new_x, height)
                } else if new_y > height {
                    (new_x, 1)
                } else {
                    (new_x, new_y)
                }
            })
            .collect();

        let mut new_queue = HashSet::new();

        for pos in queue {
            if !blizzards.contains(&pos) {
                new_queue.insert(pos);
            }
            if (pos.1 - 1 > 0 || (pos.1 - 1 == 0 && pos.0 == 1))
                && !blizzards.contains(&(pos.0, pos.1 - 1))
            {
                new_queue.insert((pos.0, pos.1 - 1));
            }
            if (pos.1 + 1 < height + 1 || (pos.1 + 1 == height + 1 && pos.0 == width))
                && !blizzards.contains(&(pos.0, pos.1 + 1))
            {
                new_queue.insert((pos.0, pos.1 + 1));
            }
            if pos.0 - 1 > 0 && pos.1 < height + 1 && !blizzards.contains(&(pos.0 - 1, pos.1)) {
                new_queue.insert((pos.0 - 1, pos.1));
            }
            if pos.0 + 1 <= width && pos.1 > 0 && !blizzards.contains(&(pos.0 + 1, pos.1)) {
                new_queue.insert((pos.0 + 1, pos.1));
            }
        }

        queue = new_queue;
        minutes += 1;
    }

    (minutes, blizzards)
}
