use std::collections::HashMap;

#[derive(Debug, PartialEq)]
enum TileType {
    EMPTY,
    WALL,
}

#[derive(Debug)]
enum Direction {
    RIGHT,
    DOWN,
    LEFT,
    UP,
}

pub fn monkey_map(notes: &Vec<String>) -> usize {
    let bottom_edge = notes.len() - 3;

    let map: HashMap<(usize, usize), TileType> = notes
        .iter()
        .enumerate()
        .flat_map(|(y, row)| {
            row.chars().enumerate().filter_map(move |(x, tile)| {
                if tile == ' ' || y > bottom_edge {
                    None
                } else {
                    Some((
                        (x + 1, y + 1),
                        match tile {
                            '.' => TileType::EMPTY,
                            '#' => TileType::WALL,
                            t => panic!("unknown tile: {t}"),
                        },
                    ))
                }
            })
        })
        .collect();

    let mut position = (
        map.iter()
            .filter(|(_, tile)| **tile == TileType::EMPTY)
            .map(|(coords, _)| coords.0)
            .min()
            .unwrap(),
        1_usize,
    );
    let mut direction = Direction::RIGHT;

    for instruction in notes
        .last()
        .unwrap()
        .replace("R", " R ")
        .replace("L", " L ")
        .split_whitespace()
    {
        if instruction == "L" {
            direction = match direction {
                Direction::RIGHT => Direction::UP,
                Direction::UP => Direction::LEFT,
                Direction::LEFT => Direction::DOWN,
                Direction::DOWN => Direction::RIGHT,
            };
        } else if instruction == "R" {
            direction = match direction {
                Direction::RIGHT => Direction::DOWN,
                Direction::DOWN => Direction::LEFT,
                Direction::LEFT => Direction::UP,
                Direction::UP => Direction::RIGHT,
            };
        } else {
            for _ in 1..=instruction.parse().unwrap() {
                let mut new_position = match direction {
                    Direction::RIGHT => (position.0 + 1, position.1),
                    Direction::DOWN => (position.0, position.1 + 1),
                    Direction::LEFT => (position.0 - 1, position.1),
                    Direction::UP => (position.0, position.1 - 1),
                };
                if !map.contains_key(&new_position) {
                    new_position = match direction {
                        Direction::RIGHT => (
                            *map.keys()
                                .filter(|(_, y)| y == &position.1)
                                .map(|(x, _)| x)
                                .min()
                                .unwrap(),
                            position.1,
                        ),
                        Direction::DOWN => (
                            position.0,
                            *map.keys()
                                .filter(|(x, _)| x == &position.0)
                                .map(|(_, y)| y)
                                .min()
                                .unwrap(),
                        ),
                        Direction::LEFT => (
                            *map.keys()
                                .filter(|(_, y)| y == &position.1)
                                .map(|(x, _)| x)
                                .max()
                                .unwrap(),
                            position.1,
                        ),
                        Direction::UP => (
                            position.0,
                            *map.keys()
                                .filter(|(x, _)| x == &position.0)
                                .map(|(_, y)| y)
                                .max()
                                .unwrap(),
                        ),
                    };
                }

                if map[&new_position] == TileType::WALL {
                    break;
                }

                position = new_position;
            }
        }
    }

    4 * position.0
        + 1000 * position.1
        + match direction {
            Direction::RIGHT => 0,
            Direction::DOWN => 1,
            Direction::LEFT => 2,
            Direction::UP => 3,
        }
}
