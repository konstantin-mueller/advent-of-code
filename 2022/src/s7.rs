use std::collections::HashMap;

use anyhow::Result;

struct Node {
    size: i32,
    parent: String,
}

pub fn no_space_left_on_device(terminal_output: &Vec<String>) -> Result<i32> {
    let tree = file_system_tree(terminal_output)?;

    Ok(tree
        .values()
        .filter(|node| node.size <= 100000)
        .map(|node| node.size)
        .sum())
}

pub fn part_two(terminal_output: &Vec<String>) -> Result<i32> {
    let tree = file_system_tree(terminal_output)?;

    let total_space = 70000000;
    let update_size = 30000000;
    let free_space = total_space - tree["/"].size;
    let space_necessary = update_size - free_space;

    let mut dir_size_to_remove = 0;
    for node in tree.values() {
        if dir_size_to_remove < space_necessary
            || (dir_size_to_remove > node.size && node.size >= space_necessary)
        {
            dir_size_to_remove = node.size;
        }
    }
    Ok(dir_size_to_remove)
}

fn file_system_tree(terminal_output: &Vec<String>) -> Result<HashMap<String, Node>> {
    let mut tree: HashMap<String, Node> = HashMap::new();
    let mut current_dir = terminal_output[0].split(" ").last().unwrap().to_string();
    tree.insert(
        current_dir.clone(),
        Node {
            size: 0,
            parent: String::from(""),
        },
    );

    for line in terminal_output.iter().skip(2) {
        if line.starts_with("dir") {
            tree.insert(
                format!(
                    "{}/{}",
                    if current_dir == "/" {
                        String::from("")
                    } else {
                        current_dir.clone()
                    },
                    line.split(" ").last().unwrap()
                ),
                Node {
                    size: 0,
                    parent: current_dir.clone(),
                },
            );
        } else if line.starts_with("$ cd") {
            if line.ends_with("..") {
                current_dir = tree[current_dir.as_str()].parent.clone();
            } else {
                current_dir = format!(
                    "{}/{}",
                    if current_dir == "/" {
                        String::from("")
                    } else {
                        current_dir
                    },
                    line.split(" ").last().unwrap()
                );
            }
        } else if line == "$ ls" {
            continue;
        } else {
            let size = line.split(" ").next().unwrap().parse::<i32>()?;
            tree.get_mut(current_dir.as_str())
                .expect("expected current dir to be present in tree")
                .size += size;

            let mut parent = tree[current_dir.as_str()].parent.clone();
            while parent != "" {
                let node = tree
                    .get_mut(parent.as_str())
                    .expect("expected parent to be present in tree");
                node.size += size;
                parent = node.parent.clone();
            }
        }
    }
    Ok(tree)
}
