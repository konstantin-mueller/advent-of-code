use regex::Regex;
use std::collections::HashMap;

pub struct Cave {
    flow_rate: i64,
    mask: i64,
    neighbours: Vec<String>,
}

pub fn proboscidea_volcanium(report: &Vec<String>, max_minutes: i64, part_two: bool) -> i64 {
    let valve_regex =
        Regex::new(r"Valve ([A-Z][A-Z]) has flow rate=(\d+); tunnels? leads? to valves? (.+)")
            .unwrap();

    let caves: HashMap<String, Cave> = report
        .iter()
        .enumerate()
        .map(|(i, line)| {
            let matches = valve_regex.captures(&line).unwrap();
            let neighbours = matches[3]
                .split(", ")
                .map(|s| s.to_string())
                .collect::<Vec<String>>();

            (
                matches[1].to_string(),
                Cave {
                    flow_rate: matches[2].parse().unwrap(),
                    mask: 1 << i,
                    neighbours,
                },
            )
        })
        .collect();

    let mut distances: HashMap<(String, String), i64> = HashMap::new();
    for name in caves.keys() {
        for other_name in caves.keys() {
            distances
                .entry((name.clone(), other_name.clone()))
                .or_insert(if caves[name].neighbours.contains(other_name) {
                    1
                } else {
                    i64::MAX
                });
        }
    }

    for name in caves.keys() {
        for other_name in caves.keys() {
            for third_name in caves.keys() {
                let distance_one = distances[&(other_name.clone(), third_name.clone())];
                let distance_two = distances[&(other_name.clone(), name.clone())];
                let distance_three = distances[&(name.clone(), third_name.clone())];

                let distance = if distance_two == i64::MAX || distance_three == i64::MAX {
                    distance_one.min(i64::MAX)
                } else {
                    distance_one.min(distance_two + distance_three)
                };
                distances.insert((other_name.clone(), third_name.clone()), distance);
            }
        }
    }

    let mut result: HashMap<i64, i64> = HashMap::new();
    visit(
        String::from("AA"),
        max_minutes,
        0,
        &caves,
        &distances,
        0,
        &mut result,
    );

    if part_two {
        result
            .iter()
            .map(|(state, pressure)| {
                result
                    .iter()
                    .filter(|(other_state, _)| state & *other_state == 0)
                    .map(|(_, other_pressure)| pressure + other_pressure)
                    .max()
                    .unwrap()
            })
            .max()
            .unwrap()
    } else {
        *result.values().max().unwrap()
    }
}

fn visit<'a>(
    cave: String,
    max_minutes: i64,
    state: i64,
    caves: &HashMap<String, Cave>,
    distances: &HashMap<(String, String), i64>,
    pressure: i64,
    result: &'a mut HashMap<i64, i64>,
) {
    let current_pressure = if !result.contains_key(&state) {
        0
    } else {
        result[&state]
    };
    result.insert(state, pressure.max(current_pressure));

    for name in caves
        .iter()
        .filter(|(_, cave)| cave.flow_rate > 0)
        .map(|(name, _)| name)
    {
        let distance = distances[&(cave.clone(), name.clone())];
        let minutes = max_minutes - distance - 1;
        if (state & caves[name].mask) != 0 || minutes < 0 {
            continue;
        } else {
            visit(
                name.clone(),
                minutes,
                state | caves[name].mask,
                &caves,
                &distances,
                pressure + (minutes * caves[name].flow_rate),
                result,
            );
        }
    }
}
