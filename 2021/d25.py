from typing import List, Set, Tuple

import resources
from timer import timer


def sea_cucumber(data: List[str]) -> int:
    """
    There are two herds of sea cucumbers sharing the same region; one always moves east
    (>), while the other always moves south (v). Each location can contain at most one
    sea cucumber; the remaining locations are empty (.).
    Every step, the sea cucumbers in the east-facing herd attempt to move forward one
    location, then the sea cucumbers in the south-facing herd attempt to move forward
    one location. When a herd moves forward, every sea cucumber in the herd first
    simultaneously considers whether there is a sea cucumber in the adjacent location
    it's facing (even another sea cucumber facing the same direction), and then every
    sea cucumber facing an empty location simultaneously moves into that location.
    Find somewhere safe to land your submarine. What is the first step on which no sea
    cucumbers move?
    """
    east: Set[Tuple[int, int]] = set()
    south: Set[Tuple[int, int]] = set()
    for i, line in enumerate(data):
        for j, char in enumerate(line):
            if char == ">":
                east.add((i, j))
            elif char == "v":
                south.add((i, j))

    row_len = len(data[0])
    col_len = len(data)

    unchanged = False
    count = 1
    while True:
        temp = {
            (x, y + 1 if y + 1 < row_len else 0)
            if (y + 1 < row_len and (x, y + 1) not in east and (x, y + 1) not in south)
            or (y + 1 == row_len and (x, 0) not in east and (x, 0) not in south)
            else (x, y)
            for x, y in east
        }
        if temp == east:
            unchanged = True
        else:
            east = temp

        temp = {
            (x + 1 if x + 1 < col_len else 0, y)
            if (x + 1 < col_len and (x + 1, y) not in east and (x + 1, y) not in south)
            or (x + 1 == col_len and (0, y) not in east and (0, y) not in south)
            else (x, y)
            for x, y in south
        }
        if temp == south and unchanged:
            break
        south = temp
        unchanged = False
        count += 1

    return count


if __name__ == "__main__":
    input_data = resources.as_list("25")
    print(sea_cucumber(input_data))
    # timer(lambda: sea_cucumber(input_data))
