import resources
from timer import timer


def transparent_origami(data: str) -> int:
    """
    The first section is a list of dots on the transparent paper. 0,0 represents the
    top-left coordinate. The first value, x, increases to the right. The second value,
    y, increases downward.
    Then, there is a list of fold instructions. Each instruction indicates a line on the
    transparent paper and wants you to fold the paper up (for horizontal y=... lines) or
    left (for vertical x=... lines).
    How many dots are visible after completing just the first fold instruction on your
    transparent paper?
    """
    dots, instr = data.split("\n\n")
    instructions = instr.splitlines()[0].split("=")
    fold = int(instructions[-1])
    fold_x = instructions[0].split()[-1] == "x"

    dot_coords = set()
    for coord in dots.splitlines():
        xy = coord.split(",")
        number = int(xy[0 if fold_x else 1])

        if number < fold:
            dot_coords.add(coord)
            continue

        dot_coords.add(coord.replace(str(number), str((number - fold * 2) * -1)))

    return len(dot_coords)


def part_two(data: str) -> str:
    """
    Finish folding the transparent paper according to the instructions. The manual says
    the code is always eight capital letters.
    """
    dots, instr = data.split("\n\n")
    instructions = instr.splitlines()

    points = [
        (int(x), int(y)) for x, y in (coord.split(",") for coord in dots.splitlines())
    ]

    for instruction in instructions:
        fold = int(instruction.split("=")[-1])
        fold_x = instruction.split("=")[0].split()[-1] == "x"

        for i in range(len(points)):
            number = points[i][0 if fold_x else 1]

            if number < fold:
                continue

            if fold_x:
                point = ((points[i][0] - fold * 2) * -1, points[i][1])
            else:
                point = (points[i][0], (points[i][1] - fold * 2) * -1)
            points[i] = point

    return "\n" + "\n".join(
        "".join(
            "#" if (j, i) in points else " "
            for j in range(max(point[0] for point in points) + 1)
        )
        for i in range(max(point[1] for point in points) + 1)
    )


if __name__ == "__main__":
    input_data = resources.as_str("13")
    timer(lambda: transparent_origami(input_data))
    timer(lambda: part_two(input_data))
