from dataclasses import dataclass
from math import prod

import resources
from timer import timer


@dataclass
class Result:
    number: int
    len_bits: int
    version: int = 0


def _decode(paket: str) -> Result:
    version = int(paket[:3], 2)
    type_id = int(paket[3:6], 2)
    paket = paket[6:]

    if type_id == 4:
        return Result(
            0,
            next((j for j in range(0, len(paket), 5) if paket[j] == "0"), 0) + 5 + 6,
            version,
        )

    version_sum = 0
    total_len = 0

    if paket[0] == "0":
        target_len = int(paket[1:16], 2)
        paket = paket[16:]
        prefix_len = 16

        while total_len < target_len:
            sub_result = _decode(paket[total_len:])
            total_len += sub_result.len_bits
            version_sum += sub_result.version

    else:
        count = int(paket[1:12], 2)
        paket = paket[12:]
        prefix_len = 12

        for _ in range(count):
            sub_result = _decode(paket[total_len:])
            total_len += sub_result.len_bits
            version_sum += sub_result.version

    return Result(0, total_len + prefix_len + 6, version + version_sum)


def paket_decoder(data: str) -> int:
    """
    Every packet begins with a standard header: the first three bits encode the packet
    version, and the next three bits encode the packet type ID. These two values are
    numbers; all numbers encoded in any packet are represented as binary with the most
    significant bit first.
    Packets with type ID 4 represent a literal value. Literal value packets encode a
    single binary number. To do this, the binary number is padded with leading zeroes
    until its length is a multiple of four bits, and then it is broken into groups of
    four bits. Each group is prefixed by a 1 bit except the last group, which is
    prefixed by a 0 bit. These groups of five bits immediately follow the packet header.
    Every other type of packet is an operator paket that contains one or more packets.
    To indicate which subsequent binary data represents its sub-packets, an operator
    packet can use one of two modes indicated by the bit immediately after the packet
    header; this is called the length type ID:
        - If the length type ID is 0, then the next 15 bits are a number that represents
            the total length in bits of the sub-packets contained by this packet.
        - If the length type ID is 1, then the next 11 bits are a number that represents
            the number of sub-packets immediately contained by this packet.
    Finally, after the length type ID bit and the 15-bit or 11-bit field, the
    sub-packets appear.
    Decode the structure of your hexadecimal-encoded BITS transmission; what do you get
    if you add up the version numbers in all packets?
    """
    paket = "".join(f"{int('0x' + hexa, 16):0>4b}" for hexa in data)
    return _decode(paket).version


def _part_two_decode(paket: str) -> Result:
    type_id = int(paket[3:6], 2)
    paket = paket[6:]

    if type_id == 4:
        result = ""
        i = 0
        for i in range(0, len(paket), 5):
            result += paket[i + 1 : i + 5]

            if paket[i] == "0":
                break

        return Result(int(result, 2), i + 5 + 6)

    results = []
    total_len = 0

    if paket[0] == "0":
        target_len = int(paket[1:16], 2)
        paket = paket[16:]
        prefix_len = 16

        while total_len < target_len:
            sub_result = _part_two_decode(paket[total_len:])
            total_len += sub_result.len_bits
            results.append(sub_result.number)

    else:
        count = int(paket[1:12], 2)
        paket = paket[12:]
        prefix_len = 12

        for _ in range(count):
            sub_result = _part_two_decode(paket[total_len:])
            total_len += sub_result.len_bits
            results.append(sub_result.number)

    if type_id == 0:
        res = sum(results)
    elif type_id == 1:
        res = prod(results)
    elif type_id == 2:
        res = min(results)
    elif type_id == 3:
        res = max(results)
    elif type_id == 5:
        res = 1 if results[0] > results[1] else 0
    elif type_id == 6:
        res = 1 if results[0] < results[1] else 0
    elif type_id == 7:
        res = 1 if results[0] == results[1] else 0
    else:
        raise ValueError(f"Invalid type id: {type_id}")

    return Result(res, total_len + prefix_len + 6)


def part_two(data: str) -> int:
    """
    New type ID rules:
        - Packets with type ID 0 are sum packets - their value is the sum of the values
            of their sub-packets. If they only have a single sub-packet, their value is
            the value of the sub-packet.
        - Packets with type ID 1 are product packets - their value is the result of
            multiplying together the values of their sub-packets. If they only have a
            single sub-packet, their value is the value of the sub-packet.
        - Packets with type ID 2 are minimum packets - their value is the minimum of the
            values of their sub-packets.
        - Packets with type ID 3 are maximum packets - their value is the maximum of the
            values of their sub-packets.
        - Packets with type ID 5 are greater than packets - their value is 1 if the
            value of the first sub-packet is greater than the value of the second
            sub-packet; otherwise, their value is 0. These packets always have exactly
            two sub-packets.
        - Packets with type ID 6 are less than packets - their value is 1 if the value
            of the first sub-packet is less than the value of the second sub-packet;
            otherwise, their value is 0. These packets always have exactly two
            sub-packets.
        - Packets with type ID 7 are equal to packets - their value is 1 if the value of
            the first sub-packet is equal to the value of the second sub-packet;
            otherwise, their value is 0. These packets always have exactly two
            sub-packets.
    What do you get if you evaluate the expression represented by your
    hexadecimal-encoded BITS transmission?
    """
    paket = "".join(f"{int('0x' + hexa, 16):0>4b}" for hexa in data)
    return _part_two_decode(paket).number


if __name__ == "__main__":
    input_data = resources.as_list("16")
    timer(lambda: paket_decoder(input_data[0]))
    timer(lambda: part_two(input_data[0]))
