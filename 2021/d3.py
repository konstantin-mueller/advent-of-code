from typing import List

import resources
from timer import timer


def _sum_of_nth_bit(data: List[str], n: int) -> int:
    return sum(int(number[n]) for number in data)


def binary_diagnostic(report: List[str]) -> int:
    """
    Each bit in the gamma rate can be determined by finding the most common bit in the
    corresponding position of all numbers in the diagnostic report.
    The epsilon rate uses the least common bit from each position.
    Use the binary numbers in your diagnostic report to calculate the gamma rate and
    epsilon rate, then multiply them together.
    """
    gamma_rate = ""
    epsilon_rate = ""

    for i in range(len(report[0])):
        if _sum_of_nth_bit(report, i) > len(report) / 2:
            gamma_rate += "1"
            epsilon_rate += "0"
        else:
            gamma_rate += "0"
            epsilon_rate += "1"

    return int(gamma_rate, 2) * int(epsilon_rate, 2)


def _most_common_nth_bit(data: List[str], n: int) -> str:
    return "1" if _sum_of_nth_bit(data, n) >= len(data) / 2 else "0"


def _least_common_nth_bit(data: List[str], n: int) -> str:
    return "1" if _sum_of_nth_bit(data, n) < len(data) / 2 else "0"


def part_two(report: List[str]) -> int:
    """
    Start with the full list of binary numbers from your diagnostic report and consider
    just the first bit of those numbers. Then:
        - Keep only numbers selected by the bit criteria for the type of rating value
            for which you are searching. Discard numbers which do not match the bit
            criteria.
        - If you only have one number left, stop; this is the rating value for which you
        are searching.
        - Otherwise, repeat the process, considering the next bit to the right.

    The bit criteria depends on which type of rating value you want to find:
        - To find oxygen generator rating, determine the most common value (0 or 1) in
            the current bit position, and keep only numbers with that bit in that
            position. If 0 and 1 are equally common, keep values with a 1 in the
            position being considered.
        - To find CO2 scrubber rating, determine the least common value (0 or 1) in the
            current bit position, and keep only numbers with that bit in that position.
            If 0 and 1 are equally common, keep values with a 0 in the position being
            considered.

    Use the binary numbers in your diagnostic report to calculate the oxygen generator
    rating and CO2 scrubber rating, then multiply them together.
    """
    oxygen_generator_rating = list(report)
    co2_scrubber_rating = list(report)

    for i in range(len(report[0])):
        if len(oxygen_generator_rating) > 1:
            most_common = _most_common_nth_bit(oxygen_generator_rating, i)
            oxygen_generator_rating = [
                number for number in oxygen_generator_rating if number[i] == most_common
            ]

        if len(co2_scrubber_rating) > 1:
            least_common = _least_common_nth_bit(co2_scrubber_rating, i)
            co2_scrubber_rating = [
                number for number in co2_scrubber_rating if number[i] == least_common
            ]

        if len(oxygen_generator_rating) == 1 and len(co2_scrubber_rating) == 1:
            break

    return int(oxygen_generator_rating[0], 2) * int(co2_scrubber_rating[0], 2)


if __name__ == "__main__":
    input_data = resources.as_list("3")
    timer(lambda: binary_diagnostic(input_data))
    timer(lambda: part_two(input_data))
