from collections import Counter
from typing import Dict, Generator, List

import resources
from timer import timer

LETTERS = ("a", "b", "c", "d", "e", "f", "g")


def seven_segment_search(data: List[str]) -> int:
    """
    In the output values (values after |), how many times do digits 1, 4, 7, or 8
    appear?
    """
    lengths = {2, 4, 3, 7}  # lengths correspond to digits 1, 4, 7, 8
    return sum(
        1
        for line in data
        for signals in line.split("|")[1].split()
        if len(signals) in lengths
    )


def _least_common(data: str) -> Generator[str, None, None]:
    counter = Counter(data)
    minimum = min(counter.values())
    return (char for char, count in counter.items() if count == minimum)


def _generate_letter_map(signals: List[str]) -> Dict[str, str]:
    signals.sort(key=len)
    letter_map: Dict[str, str] = {}

    # top with 1 & 7
    top = next(_least_common(signals[0] + signals[1]))

    # top left and center with 1 & 4 and 2, 3 & 5
    top_left_center = tuple(_least_common(signals[0] + signals[2]))
    if all(
        top_left_center[0] in signal
        # 2, 3 & 5
        for signal in signals
        if len(signal) == 5
    ):
        letter_map[top_left_center[0]] = LETTERS[3]
        letter_map[top_left_center[1]] = LETTERS[1]
    else:
        letter_map[top_left_center[0]] = LETTERS[1]
        letter_map[top_left_center[1]] = LETTERS[3]

    # top right and bottom right with 1 & 5
    five = next(
        signal
        for signal in signals
        if len(signal) == 5 and all(letter in signal for letter in top_left_center)
    )
    if signals[0][0] in five:
        letter_map[signals[0][0]] = LETTERS[5]
        top_right = signals[0][1]
    else:
        top_right = signals[0][0]
        letter_map[signals[0][1]] = LETTERS[5]

    # bottom with 4 & 5 and top and top right
    letter_map[next(_least_common(signals[2] + five + top + top_right))] = LETTERS[6]

    letter_map[top] = LETTERS[0]
    letter_map[top_right] = LETTERS[2]

    # bottom left with 8 and all others
    letter_map[next(_least_common(signals[-1] + "".join(letter_map)))] = LETTERS[4]

    return letter_map


def part_two(data: List[str]) -> int:
    """
    For each entry, determine all of the wire/segment connections and decode the
    four-digit output values. What do you get if you add up all of the output values?
    """

    result_map = {
        "abcefg": "0",
        "cf": "1",
        "acdeg": "2",
        "acdfg": "3",
        "bcdf": "4",
        "abdfg": "5",
        "abdefg": "6",
        "acf": "7",
        "abcdefg": "8",
        "abcdfg": "9",
    }

    result = 0

    for line in data:
        line_data = line.split("|")
        digits = line_data[1].split()

        letter_map = _generate_letter_map(line_data[0].split())

        result += int(
            "".join(
                result_map["".join(sorted(letter_map[letter] for letter in digit))]
                for digit in digits
            )
        )
    return result


if __name__ == "__main__":
    input_data = resources.as_list("8")
    timer(lambda: seven_segment_search(input_data))
    timer(lambda: part_two(input_data))
