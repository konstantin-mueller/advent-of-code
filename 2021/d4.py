from dataclasses import dataclass
from typing import Generator, List, Set

import resources
from timer import timer

WIN_CONDITION = "-1 -1 -1 -1 -1"


@dataclass
class Board:
    rows: str
    columns: str
    id: int = 0


def _get_columns(board: str) -> Generator[str, None, None]:
    return (
        " ".join(numbers)
        for numbers in zip(*(numbers.split() for numbers in board.split("\n")))
    )


def giant_squid(data: List[str]) -> int:
    """
    Find the bingo board that wins. A new number is drawn every turn (first line of
    input). A board wins as soon as it has at least one complete row or column of marked
    numbers. Calculate and return the sum of all unmarked numbers on the winning board
    and multiply that sum by the number that was just called when the board won.
    """
    numbers = data[0].split(",")
    boards = [
        Board(
            " " + board.replace("\n", " \n ") + " ",
            " " + " \n ".join(_get_columns(board)) + " ",
        )
        for board in "\n".join(data[2:]).split("\n\n")
    ]

    for number in numbers:
        for board in boards:
            board.rows = board.rows.replace(f" {number} ", " -1 ")
            board.columns = board.columns.replace(f" {number} ", " -1 ")

            if WIN_CONDITION in board.rows or WIN_CONDITION in board.columns:
                return sum(
                    int(number) for number in board.rows.split() if number != "-1"
                ) * int(number)

    raise ValueError("No solution found!")


def part_two(data: List[str]) -> int:
    """
    Figure out which board will win last. Once it wins, what would its final score be?
    """
    numbers = data[0].split(",")
    boards = [
        Board(
            " " + board.replace("\n", " \n ") + " ",
            " " + " \n ".join(_get_columns(board)) + " ",
            i,
        )
        for i, board in enumerate("\n".join(data[2:]).split("\n\n"))
    ]

    board_len = len(boards) - 1
    won_board_ids: Set[int] = set()

    for number in numbers:
        for board in boards:
            if board.id in won_board_ids:
                continue

            board.rows = board.rows.replace(f" {number} ", " -1 ")
            board.columns = board.columns.replace(f" {number} ", " -1 ")

            if WIN_CONDITION in board.rows or WIN_CONDITION in board.columns:
                if len(won_board_ids) == board_len:
                    return sum(
                        int(number) for number in board.rows.split() if number != "-1"
                    ) * int(number)

                won_board_ids.add(board.id)

    raise ValueError("No solution found!")


if __name__ == "__main__":
    input_data = resources.as_list("4")
    timer(lambda: giant_squid(input_data))
    timer(lambda: part_two(input_data))
