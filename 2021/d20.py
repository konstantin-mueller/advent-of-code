import resources
from timer import timer


def trench_map(data: str, executions: int) -> int:
    """
    The first section of the input is the image enhancement algorithm.
    The second section is the input image, a two-dimensional grid of light pixels (#)
    and dark pixels (.).
    The image enhancement algorithm describes how to enhance an image by simultaneously
    converting all pixels in the input image into an output image. Each pixel of the
    output image is determined by looking at a 3x3 square of pixels centered on the
    corresponding input image pixel. These nine input pixels are combined into a single
    binary number that is used as an index in the image enhancement algorithm string.
    Turn dark pixels (.) into 0 and light pixels (#) into 1.
    Through advances in imaging technology, the images being operated on here are
    infinite in size. Every pixel of the infinite output image needs to be calculated
    exactly based on the relevant pixels of the input image. The small input image you
    have is only a small region of the actual infinite input image; the rest of the
    input image consists of dark pixels (.).
    Start with the original input image and apply the image enhancement algorithm twice,
    being careful to account for the infinite size of the images. How many pixels are
    lit in the resulting image?

    Part two:
    Start again with the original input image and apply the image enhancement algorithm
    50 times. How many pixels are lit in the resulting image?
    """
    image = data.replace("#", "1").replace(".", "0").splitlines()
    algorithm = image[0]
    del image[:2]

    for execution in range(executions):
        add = "1" if execution % 2 > 0 and algorithm[0] == "1" else "0"
        lines = [add * (len(image[0]) + 4)] * 2
        image = lines + [f"{add*2}{line}{add*2}" for line in image] + lines

        result = [""] * len(image)

        for i in range(len(image)):
            for j in range(len(image[0])):
                binary = "".join(
                    image[i + k][j + l]
                    if 0 <= i + k < len(image) and 0 <= j + l < len(image[0])
                    else add
                    for k in range(-1, 2)
                    for l in range(-1, 2)
                )
                result[i] += algorithm[int(binary, 2)]

        image = result
    return sum(int(num) for line in image for num in line)


if __name__ == "__main__":
    input_data = resources.as_str("20")
    print(trench_map(input_data, 2))
    print(trench_map(input_data, 50))
