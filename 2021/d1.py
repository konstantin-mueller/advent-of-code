from functools import reduce
from typing import List

import resources
from timer import timer


def sonar_sweep(report: List[int]) -> int:
    """
    Count the number of times a depth measurement increases from the previous
    measurement
    """
    result = 0

    def depth_increased(a: int, b: int) -> int:
        if a - b < 0:
            nonlocal result
            result += 1
        return b

    reduce(depth_increased, report)
    return result


def part_two(report: List[int]) -> int:
    """
    Consider sums of a three-measurement sliding window. Count the number of times the
    sum of measurements in this sliding window increases.
    """
    windows = [
        sum(report[i : i + 3]) for i in range(len(report)) if i + 3 <= len(report)
    ]
    return sonar_sweep(windows)


if __name__ == "__main__":
    input_data = resources.as_int_list("1")
    timer(lambda: sonar_sweep(input_data))
    timer(lambda: part_two(input_data))
