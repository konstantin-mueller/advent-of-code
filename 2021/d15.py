import heapq
from typing import List

import resources
from timer import timer


def chiton(grid: List[List[int]]) -> int:
    """
    You start in the top left position, your destination is the bottom right position,
    and you cannot move diagonally. The number at each position is its risk level; to
    determine the total risk of an entire path, add up the risk levels of each position
    you enter.
    What is the lowest total risk of any path from the top left to the bottom right?
    """
    queue = [(grid[0][0], 0, 0)]
    visited = [[False] * len(line) for line in grid]
    grid_size = len(grid)

    drow = [-1, 1, 0, 0]
    dcol = [0, 0, -1, 1]

    while queue:
        distance, i, j = heapq.heappop(queue)
        if visited[i][j]:
            continue

        if i == j == grid_size - 1:
            return distance - 1

        visited[i][j] = True

        for k in range(len(drow)):
            row = i + drow[k]
            col = j + dcol[k]

            if 0 <= row < grid_size and 0 <= col < grid_size:
                heapq.heappush(queue, (distance + grid[row][col], row, col))

    raise ValueError()


if __name__ == "__main__":
    data = [[int(number) for number in line] for line in resources.as_list("15")]

    """
    Grid for part two:
    The area you originally scanned is just one tile in a 5x5 tile area that forms the
    full map. Your original map tile repeats to the right and downward; each time the
    tile repeats to the right or downward, all of its risk levels are 1 higher than the
    tile immediately up or left of it. However, risk levels above 9 wrap back around to
    1.
    """
    part_two = []
    for column in range(5):
        for line in data:
            current = []

            for cur_row in range(5):
                for number in line:
                    num = number + cur_row + column

                    while num > 9:
                        num -= 9

                    current.append(num)

            part_two.append(current)

    print(chiton(data))
    print(chiton(part_two))
    # timer(lambda: chiton(data))
    # timer(lambda: chiton(part_two))
