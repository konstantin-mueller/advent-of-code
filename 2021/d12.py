from collections import defaultdict
from typing import Dict, List

import resources
from timer import timer


def _get_nodes(data: List[str]) -> Dict[str, List[str]]:
    nodes: Dict[str, List[str]] = defaultdict(list)

    for line in data:
        start, end = line.split("-")
        if end != "start" and start != "end":
            nodes[start].append(end)

        if start != "start" and end != "end":
            nodes[end].append(start)

    return nodes


def passage_pathing(data: List[str]) -> int:
    """
    Your goal is to find the number of distinct paths that start at start, end at end,
    and don't visit small caves more than once. There are two types of caves: big caves
    (written in uppercase, like A) and small caves (written in lowercase, like b).
    How many paths through this cave system are there that visit small caves at most
    once?
    """
    count = 0
    nodes = _get_nodes(data)

    def _find_paths(node: str, visited: List[str]) -> None:
        """Count paths with a backtracking algorithm"""
        visited.append(node)

        if node == "end":
            nonlocal count
            count += 1
        else:
            for adjacent in nodes[node]:
                if adjacent.islower() and adjacent in visited:
                    continue
                _find_paths(adjacent, visited)

        visited.remove(node)

    _find_paths("start", [])
    return count


def part_two(data: List[str]) -> int:
    """
    Big caves can be visited any number of times, a single small cave can be visited at
    most twice, and the remaining small caves can be visited at most once. However, the
    caves named start and end can only be visited exactly once each.
    Given these new rules, how many paths through this cave system are there?
    """
    count = 0
    nodes = _get_nodes(data)

    def _find_paths(
        node: str, visited: List[str], two_small_caves_visited: bool = False
    ) -> None:
        """Count paths with a backtracking algorithm"""
        visited.append(node)

        if node == "end":
            nonlocal count
            count += 1
        else:
            orig_small_caves_visited = two_small_caves_visited

            for adjacent in nodes[node]:
                if adjacent.islower() and adjacent in visited:
                    if two_small_caves_visited:
                        continue
                    two_small_caves_visited = True

                _find_paths(adjacent, visited, two_small_caves_visited)
                two_small_caves_visited = orig_small_caves_visited

        visited.remove(node)

    _find_paths("start", [])
    return count


if __name__ == "__main__":
    input_data = resources.as_list("12")
    timer(lambda: passage_pathing(input_data))
    print(part_two(input_data))
    # timer(lambda: part_two(input_data))
