from typing import List

import resources
from timer import timer


def dive(course: List[str]) -> int:
    """
    Input is series of commands like:
    - forward X increases the horizontal position by X units.
    - down X increases the depth by X units.
    - up X decreases the depth by X units.

    Calculate the horizontal position and depth you would have after following the
    planned course. What do you get if you multiply your final horizontal position by
    your final depth?
    """
    position = 0
    depth = 0

    for step in course:
        instruction, amount_str = step.split()
        amount = int(amount_str)

        if instruction == "forward":
            position += amount
        elif instruction == "down":
            depth += amount
        elif instruction == "up":
            depth -= amount

    return position * depth


def part_two(course: List[str]) -> int:
    """
    New commands meaning:
    - down X increases your aim by X units.
    - up X decreases your aim by X units.
    - forward X does two things:
        - It increases your horizontal position by X units.
        - It increases your depth by your aim multiplied by X.

    Using this new interpretation of the commands, calculate the horizontal position and
    depth you would have after following the planned course. What do you get if you
    multiply your final horizontal position by your final depth?
    """
    position = 0
    depth = 0
    aim = 0

    for step in course:
        instruction, amount_str = step.split()
        amount = int(amount_str)

        if instruction == "forward":
            position += amount
            depth += aim * amount
        elif instruction == "down":
            aim += amount
        elif instruction == "up":
            aim -= amount

    return position * depth


if __name__ == "__main__":
    input_data = resources.as_list("2")
    timer(lambda: dive(input_data))
    timer(lambda: part_two(input_data))
