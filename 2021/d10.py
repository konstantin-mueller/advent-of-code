import re
from typing import List

import resources
from timer import timer


def syntax_scoring(data: List[str]) -> int:
    """
    A corrupted line is one where a chunk closes with the wrong character - that is,
    where the characters it opens and closes with do not form one of the four legal
    pairs ((), [], {}, <>).
    Find the first illegal character in each corrupted line of the navigation subsystem.
    What is the total syntax error score for those errors?
    """
    score_map = {")": 3, "]": 57, "}": 1197, ">": 25137}
    score = 0

    regex = re.compile(r"(\<\>)|(\(\))|(\{\})|(\[\])")

    for line in data:
        temp = ""
        while len(line) != len(temp):
            temp = line
            line = regex.sub("", line)

        if len(chars := [ch for ch in score_map if ch in line]) > 0:
            char = line[min(line.index(char) for char in chars)]
            score += score_map[char]

    return score


def part_two(data: List[str]) -> int:
    """
    Incomplete lines don't have any incorrect characters - instead, they're missing some
    closing characters at the end of the line. To repair the navigation subsystem, you
    just need to figure out the sequence of closing characters that complete all open
    chunks in the line.
    Find the completion string for each incomplete line, score the completion strings,
    and sort the scores. What is the middle score?
    """
    closing_chars = (")", "]", "}", ">")
    score_map = {"(": 1, "[": 2, "{": 3, "<": 4}
    scores: List[int] = []

    regex = re.compile(r"(\<\>)|(\(\))|(\{\})|(\[\])")

    for line in data:
        temp = ""
        while len(line) != len(temp):
            temp = line
            line = regex.sub("", line)

        if any(char in line for char in closing_chars):
            continue

        score = 0
        for char in line[::-1]:
            score = score * 5 + score_map[char]
        scores.append(score)

    return sorted(scores)[len(scores) // 2]


if __name__ == "__main__":
    input_data = resources.as_list("10")
    timer(lambda: syntax_scoring(input_data))
    timer(lambda: part_two(input_data))
