from collections import Counter, defaultdict
from itertools import tee
from typing import Dict

import resources
from timer import timer


def extended_polymerization(data: str, steps: int) -> int:
    """
    The first line is the polymer template - this is the starting point of the process.
    The following section defines the pair insertion rules. A rule like AB -> C means
    that when elements A and B are immediately adjacent, element C should be inserted
    between them. These insertions all happen simultaneously.
    What do you get if you take the quantity of the most common element and subtract the
    quantity of the least common element?
    """
    template, insertion_rules = data.split("\n\n")
    rules = {rule.split()[0]: rule.split()[-1] for rule in insertion_rules.splitlines()}

    a, b = tee(template)
    next(b, None)
    pairs: Dict[str, int] = Counter(x + y for x, y in zip(a, b))

    counter = defaultdict(lambda: 0, Counter(template))

    for _ in range(steps):
        temp: Dict[str, int] = defaultdict(lambda: 0)

        for pair, add in rules.items():
            if pair not in pairs:
                continue

            counter[add] += pairs[pair]

            temp[pair[0] + add] += pairs[pair]
            temp[add + pair[1]] += pairs[pair]

        pairs = temp

    return max(counter.values()) - min(counter.values())


if __name__ == "__main__":
    input_data = resources.as_str("14")
    timer(lambda: extended_polymerization(input_data, 10))
    timer(lambda: extended_polymerization(input_data, 40))
