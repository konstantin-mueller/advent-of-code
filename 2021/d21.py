from functools import cache
from itertools import cycle, islice, product
from typing import Iterable, List, Tuple

import resources
from timer import timer


def _get_pos(pos: int, rolls: Iterable[int]) -> int:
    pos = (pos + sum(rolls)) % 10
    return 10 if pos == 0 else pos


def dirac_dice(data: List[str]) -> int:
    """
    On each player's turn, the player rolls the die three times and adds up the results.
    Then, the player moves their pawn that many times forward around the track (that is,
    moving clockwise on spaces in order of increasing value, wrapping back around to 1
    after 10).
    After each player moves, they increase their score by the value of the space their
    pawn stopped on.
    The game immediately ends as a win for any player whose score reaches at least 1000.
    Play a practice game using the deterministic 100-sided die. The moment either player
    wins, what do you get if you multiply the score of the losing player by the number
    of times the die was rolled during the game?
    """
    score1 = 0
    score2 = 0
    pos1 = int(data[0].split()[-1])
    pos2 = int(data[1].split()[-1])
    die = cycle(range(1, 101))
    roll_count = 0

    while True:
        roll_count += 3
        pos1 = _get_pos(pos1, islice(die, 3))
        score1 += pos1
        if score1 >= 1000:
            break

        roll_count += 3
        pos2 = _get_pos(pos2, islice(die, 3))
        score2 += pos2
        if score2 >= 1000:
            break

    return roll_count * (score1 if score1 < 1000 else score2)


def part_two(data: List[str]) -> int:
    """
    When you roll the die, the universe splits into multiple copies, one copy for each
    possible outcome of the die. In this case, rolling the die always splits the
    universe into three copies: one where the outcome of the roll was 1, one where it
    was 2, and one where it was 3.
    The game now ends when either player's score reaches at least 21.
    Find the player that wins in more universes; in how many universes does that player
    win?
    """
    rolls = list(product((1, 2, 3), repeat=3))

    @cache
    def count_winners(
        pos1: int, pos2: int, score1: int = 0, score2: int = 0
    ) -> Tuple[int, int]:
        if score1 >= 21:
            return 1, 0

        if score2 >= 21:
            return 0, 1

        nonlocal rolls
        result = zip(
            *(
                count_winners(
                    pos2,
                    _get_pos(pos1, roll),
                    score2,
                    score1 + _get_pos(pos1, roll),
                )
                for roll in rolls
            )
        )
        first_result = sum(next(result))
        return sum(next(result)), first_result

    return max(count_winners(int(data[0].split()[-1]), int(data[1].split()[-1])))


if __name__ == "__main__":
    input_data = resources.as_list("21")
    timer(lambda: dirac_dice(input_data))
    print(part_two(input_data))
    # timer(lambda: part_two(input_data))
