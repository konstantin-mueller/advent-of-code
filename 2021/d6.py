from collections import Counter

import resources
from timer import timer


def lanternfish(data: str, days: int) -> int:
    """
    Each lanternfish creates a new lanternfish once every 7 days. You can model each
    fish as a single number that represents the number of days until it creates a new
    lanternfish.
    """
    fish = Counter(int(i) for i in data.split(","))

    for _ in range(days):
        tmp = [fish[i] for i in range(9)]
        # Reset fish with a timer of 0 (timer 7 because it gets decreased to 6 below)
        tmp[7] += fish[0]
        # Add spawned fish: Every fish with a timer of 0 spawns a new fish with timer 8
        fish[8] = fish[0]

        # Update fish by decreasing timers
        for i, fish_count in enumerate(tmp[1:], 1):
            fish[i - 1] = fish_count

    return sum(fish.values())


if __name__ == "__main__":
    input_data = resources.as_str("6")
    timer(lambda: lanternfish(input_data, 80))
    timer(lambda: lanternfish(input_data, 256))
