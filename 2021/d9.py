from math import prod
from typing import List, Optional, Set, Tuple

import resources
from timer import timer


def smoke_basin(data: List[str]) -> int:
    """
    Your first goal is to find the low points - the locations that are lower than any of
    its adjacent locations.
    The risk level of a low point is 1 plus its height.
    Find all of the low points on your heightmap. What is the sum of the risk levels of
    all low points on your heightmap?
    """
    result = 0
    for i, line in enumerate(data):
        for j, num in enumerate(line):
            number = int(num)
            if (
                (j == 0 or int(line[j - 1]) > number)
                and (j == len(line) - 1 or int(line[j + 1]) > number)
                and (i == 0 or int(data[i - 1][j]) > number)
                and (i == len(data) - 1 or int(data[i + 1][j]) > number)
            ):
                result += number + 1
    return result


def _get_neighbors(data: List[str], i: int, j: int) -> Set[Tuple[int, int]]:
    neighbors = set()

    if i > 0 and data[i - 1][j] != "9":
        neighbors.add((i - 1, j))

    if i < len(data) - 1 and data[i + 1][j] != "9":
        neighbors.add((i + 1, j))

    if j > 0 and data[i][j - 1] != "9":
        neighbors.add((i, j - 1))

    if j < len(data[i]) - 1 and data[i][j + 1] != "9":
        neighbors.add((i, j + 1))

    return neighbors


def _breadth_first_search(
    data: List[str], i: int, j: int, visited: Optional[Set[Tuple[int, int]]] = None
) -> Set[Tuple[int, int]]:
    if visited:
        visited.add((i, j))
    else:
        visited = {(i, j)}

    for k, l in _get_neighbors(data, i, j) - visited:
        visited |= _breadth_first_search(data, k, l, visited)

    return visited


def part_two(data: List[str]) -> int:
    """
    Find the three largest basins and multiply their sizes together
    """
    visited: Set[Tuple[int, int]] = set()
    basins: Set[Tuple[Tuple[int, int], ...]] = set()

    for i, line in enumerate(data):
        for j, num in enumerate(line):
            number = int(num)
            if (
                (j == 0 or int(line[j - 1]) > number)
                and (j == len(line) - 1 or int(line[j + 1]) > number)
                and (i == 0 or int(data[i - 1][j]) > number)
                and (i == len(data) - 1 or int(data[i + 1][j]) > number)
            ):
                if (i, j) in visited:
                    continue

                basin = _breadth_first_search(data, i, j)
                visited |= basin
                basins.add(tuple(basin))

    return prod(sorted(len(basin) for basin in basins)[-3:])


if __name__ == "__main__":
    input_data = resources.as_list("9")
    timer(lambda: smoke_basin(input_data))
    timer(lambda: part_two(input_data))
