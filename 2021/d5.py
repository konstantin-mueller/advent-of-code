from typing import Dict, List

import resources
from timer import timer


def hydrothermal_venture(data: List[str]) -> int:
    """
    Input contains points which can be connected by lines.
    Consider only horizontal and vertical lines. At how many points do at least two
    lines overlap?
    """
    lines: Dict[str, int] = {}

    for data_line in data:
        line = data_line.split()
        start = tuple(int(number) for number in line[0].split(","))
        end = tuple(int(number) for number in line[-1].split(","))

        if start[0] != end[0] and start[1] != end[1]:
            continue

        i = 1 if start[0] == end[0] else 0
        step = 1 if start[i] < end[i] else -1

        for j in range(start[i], end[i] + step, step):
            point = f"{j if i == 0 else start[0]},{j if i == 1 else start[1]}"
            if point not in lines:
                lines[point] = 1
            elif lines[point] <= 1:
                lines[point] = 2

    return sum(1 for i in lines.values() if i > 1)


def part_two(data: List[str]) -> int:
    """
    Consider all of the lines (also diagonal lines). At how many points do at least two
    lines overlap?
    """
    lines: Dict[str, int] = {}

    for data_line in data:
        line = data_line.split()
        start = tuple(int(number) for number in line[0].split(","))
        end = tuple(int(number) for number in line[-1].split(","))

        if start[0] != end[0] and start[1] != end[1]:
            # diagonal
            x_step = 1 if start[0] < end[0] else -1
            y_step = 1 if start[1] < end[1] else -1

            x_values = iter(range(start[0], end[0] + x_step, x_step))
            for y in range(start[1], end[1] + y_step, y_step):
                point = f"{next(x_values)},{y}"
                if point not in lines:
                    lines[point] = 1
                elif lines[point] <= 1:
                    lines[point] = 2
        else:
            i = 1 if start[0] == end[0] else 0
            step = 1 if start[i] < end[i] else -1

            for j in range(start[i], end[i] + step, step):
                point = f"{j if i == 0 else start[0]},{j if i == 1 else start[1]}"
                if point not in lines:
                    lines[point] = 1
                elif lines[point] <= 1:
                    lines[point] = 2

    return sum(1 for i in lines.values() if i > 1)


if __name__ == "__main__":
    input_data = resources.as_list("5")
    print(hydrothermal_venture(input_data))
    print(part_two(input_data))
    # timer(lambda: hydrothermal_venture(input_data))
    # timer(lambda: part_two(input_data))
