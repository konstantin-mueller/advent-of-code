from typing import List, Set, Tuple

import resources
from timer import timer


def _increase_energy_levels(
    energy_levels: List[List[int]], len_energy_levels: int, len_line: int
) -> Set[Tuple[int, int]]:
    flashed: Set[Tuple[int, int]] = set()

    for i in range(len_energy_levels):
        for j in range(len_line):
            energy_levels[i][j] += 1

            if energy_levels[i][j] == 10:
                flashed.add((i, j))

    return flashed


def _increase_neighbors(
    energy_levels: List[List[int]],
    flashed: Set[Tuple[int, int]],
    len_energy_levels: int,
    len_line: int,
) -> None:
    for i, j in flashed:
        if i < len_energy_levels - 1:
            energy_levels[i + 1][j] += 1
            if j != 0:
                energy_levels[i + 1][j - 1] += 1
            if j < len_line - 1:
                energy_levels[i + 1][j + 1] += 1

        if i != 0:
            energy_levels[i - 1][j] += 1

        if j != 0:
            energy_levels[i][j - 1] += 1
            if i != 0:
                energy_levels[i - 1][j - 1] += 1

        if j < len_line - 1:
            energy_levels[i][j + 1] += 1
            if i != 0:
                energy_levels[i - 1][j + 1] += 1


def dumbo_octopus(data: List[str]) -> int:
    """
    You can model the energy levels and flashes of light in steps. During a single step,
    the following occurs:
        - First, the energy level of each octopus increases by 1.
        - Then, any octopus with an energy level greater than 9 flashes. This increases
            the energy level of all adjacent octopuses by 1, including octopuses that
            are diagonally adjacent. If this causes an octopus to have an energy level
            greater than 9, it also flashes. This process continues as long as new
            octopuses keep having their energy level increased beyond 9. (An octopus can
            only flash at most once per step.)
        - Finally, any octopus that flashed during this step has its energy level set to
            0, as it used all of its energy to flash.
    How many total flashes are there after 100 steps?
    """
    energy_levels = [[int(number) for number in line] for line in data]
    len_energy_levels = len(energy_levels)
    len_line = len(energy_levels[0])
    flashed_count = 0

    for _ in range(100):
        flashed = _increase_energy_levels(energy_levels, len_energy_levels, len_line)
        already_flashed: Set[Tuple[int, int]] = set()

        while len(flashed) > 0:
            already_flashed |= flashed
            flashed_count += len(flashed)

            _increase_neighbors(energy_levels, flashed, len_energy_levels, len_line)

            flashed = {
                (i, j)
                for i, line in enumerate(energy_levels)
                for j, energy_level in enumerate(line)
                if energy_level >= 10 and (i, j) not in already_flashed
            }

        for i, j in already_flashed:
            energy_levels[i][j] = 0

    return flashed_count


def part_two(data: List[str]) -> int:
    """
    What is the first step during which all octopuses flash simultaneously?
    """
    energy_levels = [[int(number) for number in line] for line in data]
    len_energy_levels = len(energy_levels)
    len_line = len(energy_levels[0])
    counter = 0
    already_flashed: Set[Tuple[int, int]] = set()

    while len(already_flashed) != len_line * len_energy_levels:
        counter += 1
        already_flashed.clear()

        flashed = _increase_energy_levels(energy_levels, len_energy_levels, len_line)

        while len(flashed) > 0:
            already_flashed |= flashed

            _increase_neighbors(energy_levels, flashed, len_energy_levels, len_line)

            flashed = {
                (i, j)
                for i, line in enumerate(energy_levels)
                for j, energy_level in enumerate(line)
                if energy_level >= 10 and (i, j) not in already_flashed
            }

        for i, j in already_flashed:
            energy_levels[i][j] = 0

    return counter


if __name__ == "__main__":
    input_data = resources.as_list("11")
    timer(lambda: dumbo_octopus(input_data))
    print(part_two(input_data))
    # timer(lambda: part_two(input_data))
