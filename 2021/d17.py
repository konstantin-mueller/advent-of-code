import resources
from timer import timer


def trick_shot(data: str, part_two: bool) -> int:
    """
    The probe's x,y position starts at 0,0. Then, it will follow some trajectory by
    moving in steps. On each step, these changes occur in the following order:
        - The probe's x position increases by its x velocity.
        - The probe's y position increases by its y velocity.
        - Due to drag, the probe's x velocity changes by 1 toward the value 0; that is,
            it decreases by 1 if it is greater than 0, increases by 1 if it is less than
            0, or does not change if it is already 0.
        - Due to gravity, the probe's y velocity decreases by 1.
        - For the probe to successfully make it into the trench, the probe must be on
            some trajectory that causes it to be within a target area after any step.
            The submarine computer has already calculated this target area (your puzzle
            input).
    What is the highest y position it reaches on this trajectory?

    Part two:
    How many distinct initial velocity values cause the probe to be within the target
    area after any step?
    """
    areas = data.split("=")
    x = tuple(int(number) for number in areas[1].split(",")[0].split(".."))
    y = tuple(int(number) for number in areas[-1].split(".."))

    max_height = 0
    result = 0
    for i in range(1, x[1] + 1):
        for j in range(y[0], x[1]):
            cur_x = 0
            cur_y = 0
            drag = i
            temp_y = j
            temp_height = max_height

            while True:
                cur_x += drag
                cur_y += temp_y
                temp_y -= 1

                if drag > 0:
                    drag -= 1
                elif drag < 0:
                    drag += 1

                if x[0] <= cur_x <= x[1] and y[0] <= cur_y <= y[1]:
                    result += 1
                    break

                if cur_x > x[1] or cur_y < y[0]:
                    max_height = temp_height
                    break

                if cur_y > max_height:
                    max_height = cur_y

    return result if part_two else max_height


if __name__ == "__main__":
    input_data = resources.as_list("17")
    print(trick_shot(input_data[0], False))
    print(trick_shot(input_data[0], True))
    # timer(lambda: trick_shot(input_data[0], False))
    # timer(lambda: trick_shot(input_data[0], True))
