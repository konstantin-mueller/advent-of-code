from collections import deque
from dataclasses import dataclass
from enum import Enum
from typing import Deque, List, Optional, Tuple

import resources
from timer import timer


class Rotation(Enum):
    ZERO = 0
    QUARTER = 1
    HALF = 2
    THREE_QUARTERS = 3


class UpOrientation(Enum):
    ZERO = 0
    ONE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5


@dataclass
class Vec:
    x: int
    y: int
    z: int

    @staticmethod
    def from_vecs(first: "Vec", second: "Vec") -> "Vec":
        return Vec(first.x + second.x, first.y + second.y, first.z + second.z)

    @staticmethod
    def from_list(vec: List[int]) -> "Vec":
        return Vec(vec[0], vec[1], vec[2])

    def rotate(self, rotation: Rotation, up: UpOrientation) -> "Vec":
        if up is UpOrientation.ONE:
            vec = Vec(self.x, -self.y, -self.z)
        elif up is UpOrientation.TWO:
            vec = Vec(self.x, -self.z, self.y)
        elif up is UpOrientation.THREE:
            vec = Vec(-self.y, -self.z, self.x)
        elif up is UpOrientation.FOUR:
            vec = Vec(-self.x, -self.z, -self.y)
        elif up is UpOrientation.FIVE:
            vec = Vec(self.y, -self.z, -self.x)
        else:
            vec = self

        if rotation is Rotation.QUARTER:
            return Vec(-vec.y, vec.x, vec.z)
        if rotation is Rotation.HALF:
            return Vec(-vec.x, -vec.y, vec.z)
        if rotation is Rotation.THREE_QUARTERS:
            return Vec(vec.y, -vec.x, vec.z)
        return vec

    def distance_to(self, vec: "Vec") -> int:
        return abs(vec.x - self.x) + abs(vec.y - self.y) + abs(vec.z - self.z)


class Scanner:
    def __init__(self, beacons: Optional[List[Vec]] = None) -> None:
        self.beacons = beacons if beacons is not None else []
        self._distances: Optional[List[List[int]]] = None

    @property
    def distances(self) -> List[List[int]]:
        if self._distances is None:
            self._distances = [
                sorted(
                    self.beacons[i].distance_to(self.beacons[j])
                    for j in range(len(self.beacons))
                )
                for i in range(len(self.beacons))
            ]

        return self._distances

    def is_match(self, scanner: "Scanner") -> bool:
        for i in range(len(self.beacons)):
            first = self.distances[i]

            for j in range(len(scanner.beacons)):
                second = scanner.distances[j]
                k = 0
                l = 0
                match_count = 0

                while k < len(first) and l < len(second):
                    if first[k] == second[l]:
                        k += 1
                        l += 1
                        match_count += 1

                        if match_count >= 12:
                            return True
                    elif first[k] < second[l]:
                        k += 1
                    elif first[k] > second[l]:
                        l += 1

        return False

    def _find_match(self, scanner: "Scanner") -> Optional[Vec]:
        for abeacon in self.beacons:
            for bbeacon in scanner.beacons:
                x = bbeacon.x - abeacon.x
                y = bbeacon.y - abeacon.y
                z = bbeacon.z - abeacon.z
                match_count = 0

                for i, cbeacon in enumerate(self.beacons):
                    if match_count + len(self.beacons) - i < 12:
                        break

                    for dbeacon in scanner.beacons:
                        if (
                            x + cbeacon.x == dbeacon.x
                            and y + cbeacon.y == dbeacon.y
                            and z + cbeacon.z == dbeacon.z
                        ):
                            match_count += 1
                            if match_count >= 12:
                                return Vec(x, y, z)
                            break
        return None

    def try_match(self, scanners: List["Scanner"]) -> Optional[Tuple["Scanner", Vec]]:
        for scanner in scanners:
            if match := self._find_match(scanner):
                return scanner, match
        return None

    def add_beacons(self, scanner: "Scanner", vec: Vec) -> None:
        for beacon in scanner.beacons:
            if (
                new_beacon := Vec(beacon.x - vec.x, beacon.y - vec.y, beacon.z - vec.z)
            ) not in self.beacons:
                self.beacons.append(new_beacon)


def beacon_sanner(data: str, part_two: bool) -> int:
    """
    Each scanner is capable of detecting all beacons in a large cube centered on the
    scanner; beacons that are at most 1000 units away from the scanner in each of the
    three axes (x, y, and z) have their precise position determined relative to the
    scanner.
    Unfortunately, while each scanner can report the positions of all detected beacons
    relative to itself, the scanners do not know their own position. You'll need to
    determine the positions of the beacons and scanners yourself.
    The scanners and beacons map a single contiguous 3d region. This region can be
    reconstructed by finding pairs of scanners that have overlapping detection regions
    such that there are at least 12 beacons that both scanners detect within the
    overlap. By establishing 12 common beacons, you can precisely determine where the
    scanners are relative to each other, allowing you to reconstruct the beacon map one
    scanner at a time.
    The scanners also don't know their rotation or facing direction. Due to magnetic
    alignment, each scanner is rotated some integer number of 90-degree turns around all
    of the x, y, and z axes. In total, each scanner could be in any of 24 different
    orientations: facing positive or negative x, y, or z, and considering any of four
    directions "up" from that facing.
    Assemble the full map of beacons. How many beacons are there?

    Part two:
    What is the largest Manhattan distance between any two scanners?
    """
    scanners: List[List[Scanner]] = []
    for str_scanner in data.split("\n\n"):
        scanners.append([None] * 24)

        for i in range(len(UpOrientation)):
            for j in range(len(Rotation)):
                scanners[-1][j + i * 4] = Scanner(
                    [
                        Vec.from_list([int(num) for num in beacon.split(",")]).rotate(
                            Rotation(j), UpOrientation(i)
                        )
                        for beacon in str_scanner.splitlines()
                        if not beacon.startswith("---")
                    ]
                )

    positions: List[Optional[Vec]] = [None] * len(scanners)
    positions[0] = Vec(0, 0, 0)
    rotations = [scanners[0][0]] * len(scanners)

    queue: Deque[int] = deque([0])
    while len(queue) > 0:
        index = queue.popleft()

        for i in range(len(scanners)):
            if (
                positions[i]
                or not scanners[index][0].is_match(scanners[i][0])
                or not (match := rotations[index].try_match(scanners[i]))
            ):
                continue

            scanner, vec = match
            rotations[i] = scanner
            positions[i] = Vec.from_vecs(positions[index], vec)
            queue.append(i)

    if not part_two:
        result = Scanner(list(rotations[0].beacons))
        for i in range(1, len(scanners)):
            result.add_beacons(rotations[i], positions[i])
        return len(result.beacons)

    return max(
        first.distance_to(second) for first in positions for second in positions[1:]
    )


if __name__ == "__main__":
    input_data = resources.as_str("19")
    print(beacon_sanner(input_data, False))
    print(beacon_sanner(input_data, True))
    # timer(lambda: snailfish(input_data))
    # timer(lambda: trick_shot(input_data[0], True))
