from statistics import median

import resources
from timer import timer


def treachery_of_whales(data: str) -> int:
    """
    Input: Horizontal position of each crab
    Crab submarines have limited fuel, so you need to find a way to make all of their
    horizontal positions match while requiring them to spend as little fuel as possible.
    Determine the horizontal position that the crabs can align to using the least fuel
    possible. How much fuel must they spend to align to that position?
    """
    numbers = [int(number) for number in data.split(",")]
    median_result = round(median(numbers))
    return sum(abs(number - median_result) for number in numbers)


def part_two(data: str) -> int:
    """
    As it turns out, crab submarine engines don't burn fuel at a constant rate. Instead,
    each change of 1 step in horizontal position costs 1 more unit of fuel than the
    last: the first step costs 1, the second step costs 2, the third step costs 3, and
    so on.
    """
    numbers = [int(number) for number in data.split(",")]

    return round(
        min(
            sum(abs(number - i) * (abs(number - i) + 1) / 2 for number in numbers)
            for i in range(min(numbers), max(numbers) + 1)
        )
    )


if __name__ == "__main__":
    input_data = resources.as_str("7")
    timer(lambda: treachery_of_whales(input_data))
    # timer(lambda: part_two(input_data))
    print(part_two(input_data))
