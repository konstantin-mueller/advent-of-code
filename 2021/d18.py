import json
import math
from typing import Any, List

import resources
from timer import timer


def snailfish(data: List[List[str]]) -> int:
    """
    To add two snailfish numbers, form a pair from the left and right parameters of the
    addition operator.
    To reduce a snailfish number, you must repeatedly do the first action in this list
    that applies to the snailfish number:
        - If any pair is nested inside four pairs, the leftmost such pair explodes.
        - If any regular number is 10 or greater, the leftmost such regular number
            splits.
    To explode a pair, the pair's left value is added to the first regular number to the
    left of the exploding pair (if any), and the pair's right value is added to the
    first regular number to the right of the exploding pair (if any). Exploding pairs
    will always consist of two regular numbers. Then, the entire exploding pair is
    replaced with the regular number 0.
    To split a regular number, replace it with a pair; the left element of the pair
    should be the regular number divided by two and rounded down, while the right
    element of the pair should be the regular number divided by two and rounded up.
    The magnitude of a pair is 3 times the magnitude of its left element plus 2 times
    the magnitude of its right element. The magnitude of a regular number is just that
    number.
    What is the magnitude of the final sum?
    """
    current = data[0]
    for next_number in data[1:]:
        current = ["[", *current, ",", *next_number, "]"]

        while True:
            while _explode(current):
                pass

            if not _split(current):
                break

    return _calc_magnitude(json.loads("".join(current)))


def _explode(number: List[str]) -> bool:
    depth = 0
    for i in range(len(number)):

        if depth >= 5:
            for x in range(i - 2, -1, -1):
                if number[x] not in ",[]":
                    number[x] = str(int(number[x]) + int(number[i]))
                    break

            for y in range(i + 3, len(number)):
                if number[y] not in ",[]":
                    number[y] = str(int(number[y]) + int(number[i + 2]))
                    break

            number.insert(i - 1, "0")
            for _ in range(5):
                del number[i]

            return True

        if number[i] == "[":
            depth += 1
        elif number[i] == "]":
            depth -= 1

    return False


def _split(number: List[str]) -> bool:
    for i in range(len(number)):
        if number[i] in ",[]" or (current := int(number[i])) < 10:
            continue

        del number[i]
        for char in ("]", str(math.ceil(current / 2)), ",", str(current // 2), "["):
            number.insert(i, char)

        return True
    return False


def _calc_magnitude(result: Any) -> int:
    return (
        result
        if isinstance(result, int)
        else 3 * _calc_magnitude(result[0]) + 2 * _calc_magnitude(result[1])
    )


def part_two(data: List[List[str]]) -> int:
    """
    What is the largest magnitude of any sum of two different snailfish numbers?
    """
    return max(snailfish([first, second]) for first in data[:-1] for second in data[1:])


if __name__ == "__main__":
    input_data = resources.as_list_of_lists("18")
    print(snailfish(input_data))
    print(part_two(input_data))
    # timer(lambda: snailfish(input_data))
    # timer(lambda: trick_shot(input_data[0], True))
