from functools import cache
from typing import List, Optional, Tuple

import resources
from timer import timer


@cache
def _find_ranges(
    first: Tuple[bool, int, int, int, int, int, int],
    second: Tuple[bool, int, int, int, int, int, int],
) -> Optional[List[Tuple[bool, int, int, int, int, int, int]]]:
    _, first_x1, first_x2, first_y1, first_y2, first_z1, first_z2 = first
    _, second_x1, second_x2, second_y1, second_y2, second_z1, second_z2 = second

    if (
        second_x1 > first_x2
        or first_x1 > second_x2
        or second_y1 > first_y2
        or first_y1 > second_y2
        or second_z1 > first_z2
        or first_z1 > second_z2
    ):
        return None

    result = []

    x = (max(first_x1, second_x1), min(first_x2, second_x2))
    y = (max(first_y1, second_y1), min(first_y2, second_y2))

    if second_x1 > first_x1:
        result.append((True, first_x1, second_x1 - 1, *first[3:]))
    if second_x2 < first_x2:
        result.append((True, second_x2 + 1, *first[2:]))

    if second_y1 > first_y1:
        result.append((True, *x, first_y1, second_y1 - 1, *first[5:]))
    if second_y2 < first_y2:
        result.append((True, *x, second_y2 + 1, *first[4:]))

    if second_z1 > first_z1:
        result.append((True, *x, *y, first_z1, second_z1 - 1))
    if second_z2 < first_z2:
        result.append((True, *x, *y, second_z2 + 1, first_z2))

    return result


def reactor_reboot(data: List[str], part_two: bool) -> int:
    """
    The reactor core is made up of a large 3-dimensional grid made up entirely of cubes,
    one cube per integer 3-dimensional coordinate (x,y,z). Each cube can be either on or
    off; at the start of the reboot process, they are all off.
    To reboot the reactor, you just need to set all of the cubes to either on or off by
    following a list of reboot steps (your puzzle input).
    Execute the reboot steps. Afterward, considering only cubes in the region
    x=-50..50,y=-50..50,z=-50..50, how many cubes are on?

    Part two:
    Starting again with all cubes off, execute all reboot steps. Afterward, considering
    all cubes, how many cubes are on?
    """
    steps = [
        (
            line.startswith("on"),
            int(line.split("=")[1].split("..")[0]),
            int(line.split("=")[1].split("..")[1].split(",")[0]),
            int(line.split("=")[2].split("..")[0]),
            int(line.split("=")[2].split("..")[1].split(",")[0]),
            int(line.split("=")[3].split("..")[0]),
            int(line.split("=")[3].split("..")[1]),
        )
        for line in data
    ]
    if not part_two:
        steps = [
            step
            for step in steps
            if all(-50 <= number <= 50 for number in step if isinstance(number, int))
        ]

    result: List[Tuple[bool, int, int, int, int, int, int]] = []
    for step in steps:
        temp = []

        for cube in result:
            if (ranges := _find_ranges(cube, step)) is not None:
                temp.extend(ranges)
            else:
                temp.append(cube)

        if step[0]:
            temp.append(step)

        result = temp

    return sum(
        (x2 + 1 - x1) * (y2 + 1 - y1) * (z2 + 1 - z1)
        for _, x1, x2, y1, y2, z1, z2 in result
    )


if __name__ == "__main__":
    input_data = resources.as_list("22")
    timer(lambda: reactor_reboot(input_data, False))
    print(reactor_reboot(input_data, True))
    # timer(lambda: reactor_reboot(input_data, True))
