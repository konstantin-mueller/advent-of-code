import itertools
from copy import deepcopy
from typing import Callable, Iterator, List, Optional, Tuple

import resources


def seating_system(seats: List[List[str]]) -> int:
    """
    How many seats end up occupied?
    [75]
    """
    return _get_occupied_seats(seats, _part_one_adjacent_func)


def _get_occupied_seats(
    seats: List[List[str]],
    adjacent_func: Callable[[int, int, List[List[str]]], Callable[[], Iterator[str]]],
    num_occupied_seats_for_empty: int = 4,
) -> int:
    previous = deepcopy(seats)
    result = deepcopy(seats)
    stabilized = True

    while True:
        for i in range(len(seats)):
            for j in range(len(seats[0])):
                result[i][j], changed = _apply_rules(
                    i,
                    j,
                    previous,
                    adjacent_func(i, j, previous),
                    num_occupied_seats_for_empty,
                )
                if stabilized and changed:
                    stabilized = False

        if stabilized:
            return sum(sum(1 for seat in row if seat == "#") for row in result)
        stabilized = True
        previous = deepcopy(result)


def _part_one_adjacent_func(
    i: int, j: int, seats: List[List[str]]
) -> Callable[[], Iterator[str]]:
    return lambda: (
        seats[row_i][col_j]
        for x, y in itertools.product([-1, 0, 1], [-1, 0, 1])
        if not (x == 0 and y == 0)
        and 0 <= (row_i := x + i) < len(seats)
        and 0 <= (col_j := y + j) < len(seats[0])
    )


def _apply_rules(
    i: int,
    j: int,
    seats: List[List[str]],
    get_adjacent_func: Callable[[], Iterator[str]],
    num_occupied_seats_for_empty: int = 4,
) -> Tuple[str, bool]:
    seat = seats[i][j]
    if seat == ".":
        return ".", False

    adjacent = get_adjacent_func()

    if seat == "L" and all(adj != "#" for adj in adjacent):
        return "#", True
    if (
        seat == "#"
        and sum(1 for adj in adjacent if adj == "#") >= num_occupied_seats_for_empty
    ):
        return "L", True
    return seat, False


def part_two(seats: List[List[str]]) -> int:
    """
    People don't just care about adjacent seats - they care about the first seat they
    can see in each of those eight directions!
    it now takes five or more visible occupied seats for an occupied seat to become
    empty (rather than four or more from the previous rules).
    How many seats end up occupied?
    [24]
    """
    return _get_occupied_seats(
        seats, _part_two_adjacent_fun, num_occupied_seats_for_empty=5
    )


UP = lambda i, j: (i - 1, j)
UP_RIGHT = lambda i, j: (i - 1, j + 1)
RIGHT = lambda i, j: (i, j + 1)
DOWN_RIGHT = lambda i, j: (i + 1, j + 1)
DOWN = lambda i, j: (i + 1, j)
DOWN_LEFT = lambda i, j: (i + 1, j - 1)
LEFT = lambda i, j: (i, j - 1)
UP_LEFT = lambda i, j: (i - 1, j - 1)
MOVEMENT_FUNCS = [UP, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT, UP_LEFT]


def _part_two_adjacent_fun(
    i: int, j: int, seats: List[List[str]]
) -> Callable[[], Iterator[str]]:
    return lambda: itertools.chain.from_iterable(
        filter(None, (_find_first_seat(i, j, seats, func) for func in MOVEMENT_FUNCS))
    )


def _find_first_seat(
    i: int,
    j: int,
    seats: List[List[str]],
    get_next_index: Callable[[int, int], Tuple[int, int]],
) -> Optional[str]:
    i, j = get_next_index(i, j)
    while 0 <= i < len(seats) and 0 <= j < len(seats[0]):
        if seats[i][j] in "#L":
            return seats[i][j]
        i, j = get_next_index(i, j)


def main():
    seats = resources.as_list_of_lists("11")

    print(seating_system(seats))
    print(part_two(seats))


if __name__ == "__main__":
    main()
