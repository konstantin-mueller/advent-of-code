import copy
import re
from typing import List

import resources
from timer import timer


def docking_data(program: List[str]) -> int:
    """
    Execute the initialization program. What is the sum of all values left in memory
    after it completes?
    """
    result = {}
    regex = re.compile(r"mem\[(\d+)\] = (\d+)")

    i = 0
    while i < len(program):
        mask = program[i].replace("mask = ", "")[::-1]
        mem_counter = 1
        for mem_counter, mem in enumerate(program[i + 1 :]):
            if mem.startswith("mask"):
                break

            mem = regex.search(mem)
            index, value = int(mem.group(1)), f"{int(mem.group(2)):b}"[::-1]

            bin_result = ""
            for j, bit in enumerate(value):
                if mask[j] == "X" or mask[j] == bit:
                    bin_result += bit
                else:
                    bin_result += mask[j]
            bin_result += mask[len(value) :].replace("X", "0")

            result[index] = int(bin_result[::-1], base=2)

        i += mem_counter + 1
    return sum(result.values())


def part_two(program: List[str]) -> int:
    """
    Execute the initialization program using an emulator for a version 2 decoder chip.
    What is the sum of all values left in memory after it completes?
    """
    result = {}
    regex = re.compile(r"mem\[(\d+)\] = (\d+)")

    i = 0
    while i < len(program):
        mask = program[i].replace("mask = ", "")[::-1]
        mem_counter = 1
        for mem_counter, mem in enumerate(program[i + 1 :]):
            if mem.startswith("mask"):
                break

            mem = regex.search(mem)
            address, value = f"{int(mem.group(1)):b}"[::-1], int(mem.group(2))

            bin_results = [""]
            for j, bit in enumerate(mask):
                x = False
                for k in range(len(bin_results)):
                    if bit == "0":
                        bin_results[k] += address[j] if j < len(address) else "0"
                    elif bit == "1":
                        bin_results[k] += "1"
                    else:
                        x = True
                        bin_results[k] += "0"
                if x:
                    addresses = copy.deepcopy(bin_results)
                    bin_results.extend(address[:-1] + "1" for address in addresses)

            for j in [int(bin_result[::-1], base=2) for bin_result in bin_results]:
                result[j] = value

        i += mem_counter + 1
    return sum(result.values())


def main() -> None:
    program = resources.as_list("14")

    timer(lambda: docking_data(program))
    print(part_two(program))


if __name__ == "__main__":
    main()
