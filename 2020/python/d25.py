from typing import Optional


def combo_breaker(card: int, door: int) -> int:
    card_loop = _run(end=card)
    return _run(subject=door, loop=card_loop)


def _run(
    subject: int = 7, loop: Optional[int] = None, end: Optional[int] = None
) -> int:
    value = 1
    i = 0
    while (loop is not None and i < loop) or (end is not None and value != end):
        value *= subject
        value %= 20201227
        i += 1
    return i if loop is None else value


def main() -> None:
    print(combo_breaker(14205034, 18047856))


if __name__ == "__main__":
    main()
