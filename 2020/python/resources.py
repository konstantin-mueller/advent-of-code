import os
from typing import List, Set


def _get_resource_path(filename: str) -> str:
    return os.path.join("..", "resources", filename)


def as_int_set(filename: str) -> Set[int]:
    with open(_get_resource_path(filename), "r") as _file:
        return {int(value) for value in _file.read().splitlines()}


def as_list(filename: str) -> List[str]:
    with open(_get_resource_path(filename), "r") as _file:
        return _file.read().splitlines()


def as_int_list(filename: str) -> List[int]:
    with open(_get_resource_path(filename), "r") as _file:
        return [int(value) for value in _file.read().splitlines()]


def as_list_of_lists(filename: str) -> List[List[str]]:
    return [list(s) for s in as_list(filename)]


def as_str(filename: str) -> str:
    with open(_get_resource_path(filename) ,"r") as _file:
        return _file.read()
