import math
from typing import Any, List, Tuple

import resources
from timer import timer


def ticket_translation(data: str):
    """
    Consider the validity of the nearby tickets you scanned. What is your ticket
    scanning error rate?
    """
    parts = data.split("\n\n")
    rules, _ = _get_rules(parts[0].split("\n"))

    result = 0
    for line in parts[2].split("\n")[1:-1]:
        for num in line.split(","):
            num = int(num)
            if not any(num in rule[0] or num in rule[1] for rule in rules):
                result += num

    return result


def part_two(data: str):
    """
    Once you work out which field is which, look for the six fields on your ticket that
    start with the word departure. What do you get if you multiply those six values
    together?
    """
    parts = data.split("\n\n")
    rules, departure_rules = _get_rules(parts[0].split("\n"))

    results = {}
    for line in parts[2].split("\n")[1:-1]:
        for i, num in enumerate(line.split(",")):
            num = int(num)

            valid = {
                i for i, rule in enumerate(rules) if num in rule[0] or num in rule[1]
            }
            if len(valid) > 0:
                results[i] = results[i] & valid if i in results else valid

    result = [0] * len(results)
    while len(results) > 0:
        i, r = next((i, res) for i, res in results.items() if len(res) == 1)
        r = r.pop()
        del results[i]
        result[i] = r

        for j, res in results.items():
            res.remove(r)
            results[j] = res

    ticket = [int(number) for number in parts[1].split("\n")[1].split(",")]
    return math.prod(ticket[i] for i, r in enumerate(result) if r in departure_rules)


def _get_rules(inp_rules: List[str]) -> Tuple[List[Tuple[Any, Any]], List[int]]:
    rules = []
    departure_rules = []
    for i, line in enumerate(inp_rules):
        rule = line.split(":")
        if rule[0].startswith("departure"):
            departure_rules.append(i)

        line = rule[1].split()
        a = line[0].split("-")
        b = line[2].split("-")

        a = range(int(a[0]), int(a[1]) + 1)
        b = range(int(b[0]), int(b[1]) + 1)
        rules.append((a, b))
    return rules, departure_rules


def main() -> None:
    data = resources.as_str("16")

    timer(lambda: ticket_translation(data))
    print(part_two(data))


if __name__ == "__main__":
    main()
