import re
from typing import Callable, Dict, List

import resources
from timer import timer


REQUIRED_FIELDS = ["byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:"]


def passport_processing(passports: List[str]) -> int:
    """
    Count the number of valid passports - those that have all required fields.
    Treat cid as optional. In your batch file, how many passports are valid?
    """
    if passports[-1] != "":
        passports.append("")

    valid_passports = 0

    field_counter = 0
    for i in range(len(passports)):
        if passports[i] == "":
            if field_counter == len(REQUIRED_FIELDS):
                valid_passports += 1

            field_counter = 0
            continue

        field_counter += len(
            [field for field in REQUIRED_FIELDS if field in passports[i]]
        )

    return valid_passports


def part_two(passports: List[str]) -> int:
    """
    Count the number of valid passports - those that have all required fields and valid
    values. Continue to treat cid as optional. In your batch file, how many passports
    are valid?
    """
    if passports[-1] != "":
        passports.append("")

    rules: Dict[str, Callable[[str], bool]] = {
        "byr:": lambda value: 1920 <= int(value) <= 2002,
        "iyr:": lambda value: 2010 <= int(value) <= 2020,
        "eyr:": lambda value: 2020 <= int(value) <= 2030,
        "hgt:": lambda value: (
            value.endswith("cm") and 150 <= int(value.replace("cm", "")) <= 193
        )
        or (value.endswith("in") and 59 <= int(value.replace("in", "")) <= 76),
        "hcl:": lambda value: re.fullmatch(r"#[0-9a-f]{6}", value) is not None,
        "ecl:": lambda value: value
        in {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"},
        "pid:": lambda value: re.fullmatch(r"\d{9}", value) is not None,
    }

    regex_get_value = re.compile(fr"({'|'.join(REQUIRED_FIELDS)})(\S+)")
    valid_passports = 0

    field_counter = 0
    for i in range(len(passports)):
        if passports[i] == "":
            if field_counter == len(REQUIRED_FIELDS):
                valid_passports += 1

            field_counter = 0
            continue

        values = regex_get_value.findall(passports[i])
        field_counter += len([field for field, value in values if rules[field](value)])

    return valid_passports


if __name__ == "__main__":
    passports = resources.as_list("4")
    timer(lambda: passport_processing(passports))
    timer(lambda: part_two(passports))
