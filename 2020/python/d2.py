import re
from typing import List

import resources
from timer import timer


def password_philosophy(passwords: List[str]) -> int:
    """How many passwords are valid according to their policies?"""
    valid_counter = 0
    for pw in passwords:
        policy, password = tuple(pw.split(": "))
        at_least, at_most, letter = tuple(re.split(r"-| ", policy))

        if int(at_least) <= password.count(letter) <= int(at_most):
            valid_counter += 1

    return valid_counter


def part_two(passwords: List[str]) -> int:
    """
    New Logic: The numbers in policies are positions in passwords. One of the two
    positions has to contain the lette, but not both.
    How many passwords are valid?
    """
    valid_counter = 0
    for pw in passwords:
        policy, password = tuple(pw.split(": "))
        at_least, at_most, letter = tuple(re.split(r"-| ", policy))
        first = password[int(at_least) - 1]
        second = password[int(at_most) - 1]

        if (first == letter and second != letter) or (
            second == letter and first != letter
        ):
            valid_counter += 1

    return valid_counter


if __name__ == "__main__":
    passwords = resources.as_list("2")
    timer(lambda: password_philosophy(passwords))
    timer(lambda: part_two(passwords))
