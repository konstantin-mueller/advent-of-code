import math
from typing import List

import resources
from timer import timer


def binary_boarding(passes: List[str]) -> int:
    """
    What is the highest seat ID on a boarding pass?
    """
    return max(_get_seat_id(boarding_pass) for boarding_pass in passes)


def part_two(passes: List[str]) -> int:
    """
    The seats with IDs +1 and -1 from yours will be in your list.
    What is the ID of your seat?
    """
    all_ids = [_get_seat_id(boarding_pass) for boarding_pass in passes]
    return 1 + min(
        filter(
            lambda seat_id: (
                (seat_id - 2 in all_ids and not seat_id - 1 in all_ids)
                or (seat_id + 2 in all_ids and not seat_id + 1 in all_ids)
            ),
            all_ids,
        )
    )


def _get_seat_id(boarding_pass: str) -> int:
    row = _find(boarding_pass[:7])
    column = _find(boarding_pass[-3:], max_num=7)
    return row * 8 + column


def _find(boarding_pass: str, min_num: int = 0, max_num: int = 127) -> int:
    if boarding_pass == "":
        return max_num

    next_num = (max_num - min_num) / 2 + min_num

    if boarding_pass[0] == "F" or boarding_pass[0] == "L":
        return _find(boarding_pass[1:], min_num, math.floor(next_num))

    if boarding_pass[0] == "B" or boarding_pass[0] == "R":
        return _find(boarding_pass[1:], math.ceil(next_num), max_num)

    raise Exception(f"Character '{boarding_pass[0]}' not supported")


if __name__ == "__main__":
    passes = resources.as_list("5")

    timer(lambda: binary_boarding(passes))
    timer(lambda: part_two(passes))
