from typing import List, Tuple

import resources
from timer import timer


def handheld_halting(instructions: List[str]) -> int:
    """
    Immediately before any instruction is executed a second time, what value is in the
    accumulator?
    """
    return _run_code(instructions, -1)[1]


def part_two(instructions: List[str]) -> int:
    """
    Fix the program so that it terminates normally by changing exactly one jmp (to nop)
    or nop (to jmp). What is the value of the accumulator after the program terminates?
    """
    results = (
        _run_code(instructions, i)
        for i, instruction in enumerate(instructions)
        if instruction.startswith("nop") or instruction.startswith("jmp")
    )
    return next(result[1] for result in results if result[0])


def _run_code(
    instructions: List[str], switch_nop_jmp_at_index: int
) -> Tuple[bool, int]:
    counters = [0] * len(instructions)
    accumulator = 0
    i = 0
    while i < len(instructions):
        if counters[i] > 0:
            return False, accumulator
        counters[i] += 1

        current = instructions[i].split()
        if i == switch_nop_jmp_at_index:
            if current[0] == "nop":
                current[0] = "jmp"
            elif current[0] == "jmp":
                current[0] = "nop"

        if current[0] == "acc":
            accumulator += int(current[1])
        elif current[0] == "jmp":
            i += int(current[1])
            continue
        i += 1

    return True, accumulator


if __name__ == "__main__":
    instructions = resources.as_list("8")

    timer(lambda: handheld_halting(instructions))
    timer(lambda: part_two(instructions))
