import sys
from typing import List, Tuple

import resources
from timer import timer


def shuttle_search(data: List[str]) -> int:
    """
    What is the ID of the earliest bus you can take to the airport multiplied by the
    number of minutes you'll need to wait for that bus?
    [11]
    """
    estimate = int(data[0])
    minimum = sys.maxsize, sys.maxsize

    for bus_id in data[1].split(","):
        if bus_id == "x":
            continue

        bus = int(bus_id)
        result = bus - (estimate % bus)

        if result < minimum[0]:
            minimum = result, bus

    return minimum[0] * minimum[1]


def part_two(data: List[str]) -> int:
    """
    What is the earliest timestamp such that all of the listed bus IDs depart at offsets
    matching their positions in the list?
    """
    busses = []
    bus_product = 1
    for i, bus in enumerate(data[1].split(",")):
        if bus == "x":
            continue

        bus = int(bus)
        busses.append((bus - i, bus))
        bus_product *= bus

    return _chinese_remainder_theorem(busses, bus_product) % bus_product


def _chinese_remainder_theorem(busses: List[Tuple[int, int]], bus_product: int) -> int:
    return sum(
        i * pow(bus_product // bus, bus - 2, bus) * bus_product // bus
        for i, bus in busses
    )


def main() -> None:
    data = resources.as_list("13")

    timer(lambda: shuttle_search(data))
    timer(lambda: part_two(data))


if __name__ == "__main__":
    main()
