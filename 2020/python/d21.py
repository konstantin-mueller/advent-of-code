from copy import deepcopy
from typing import List, Tuple

import resources
from timer import timer


def allergen_assessment(foods: List[str]) -> Tuple[int, str]:
    all_ingredients = set()
    allergens = {}
    for food in foods:
        food, cur_allergens = food.split(" (contains ")
        food = set(food.split())
        cur_allergens = cur_allergens.split(", ")
        cur_allergens[-1] = cur_allergens[-1][:-1]

        for allergen in cur_allergens:
            if allergen in allergens:
                allergens[allergen] &= food
            else:
                allergens[allergen] = deepcopy(food)

        all_ingredients |= food

    # Remove foods that are unique for an allergen from other allergens
    for _ in range(5):
        for allergen, food in filter(lambda a_f: len(a_f[1]) == 1, allergens.items()):
            for _foods in filter(
                lambda f: len(f) > 1 and next(iter(food)) in f, allergens.values()
            ):
                _foods.remove(next(iter(food)))

    inp = " " + " ".join(foods)
    part_one = sum(
        inp.count(f" {ingredient} ")
        for ingredient in all_ingredients
        if not any(ingredient in f for f in allergens.values())
    )

    part_two = ",".join(
        next(iter(food[1])) for food in sorted(allergens.items(), key=lambda a: a[0])
    )

    return part_one, part_two


def main() -> None:
    foods = resources.as_list("21")

    timer(lambda: allergen_assessment(foods))


if __name__ == "__main__":
    main()
