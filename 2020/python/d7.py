import itertools
import re
from typing import List, Pattern, Set

import resources
from timer import timer


def handy_haversacks(rules: List[str], bag: str) -> int:
    """
    How many bag colors can eventually contain at least one shiny gold bag?
    """
    regex = re.compile(r"(.+) bags contain")
    return len(_find_bag(rules, bag, regex))


def _find_bag(rules: List[str], bag: str, regex: Pattern[str]) -> Set[str]:
    result = {
        regex.match(rule).group(1)
        for rule in rules
        if bag in rule and not rule.startswith(bag)
    }

    return set(
        itertools.chain(
            result, *[_find_bag(rules, current_bag, regex) for current_bag in result]
        )
    )


def part_two(rules: List[str], bag: str) -> int:
    """
    How many individual bags are required inside your single shiny gold bag?
    """
    return _find_containing_bags(
        rules, bag, re.compile(r"(?:(\d+) (.+?) bag[^\d\.]*?)+")
    )


def _find_containing_bags(rules: List[str], bag: str, regex: Pattern[str]) -> int:
    rule = next(filter(lambda r: r.startswith(f"{bag} bags contain"), rules))
    return sum(
        int(current_bag[0])
        + int(current_bag[0]) * _find_containing_bags(rules, current_bag[1], regex)
        for current_bag in regex.findall(rule)
    )


if __name__ == "__main__":
    rules = resources.as_list("7")

    timer(lambda: handy_haversacks(rules, "shiny gold"))
    timer(lambda: part_two(rules, "shiny gold"))
