from typing import List, Tuple

import resources
from timer import timer


def rain_risk(instructions: List[str]) -> int:
    """
    Figure out where the navigation instructions lead. What is the Manhattan distance
    between that location and the ship's starting position?
    [44]
    """
    direction = "E"
    north, east, south, west = 0, 0, 0, 0
    for instruction in instructions:
        inst_value = int(instruction[1:])

        if instruction[0] == "F":
            instruction = instruction.replace("F", direction)

        if instruction[0] == "N":
            north, south = _move(north, south, inst_value)
        elif instruction[0] == "S":
            south, north = _move(south, north, inst_value)
        elif instruction[0] == "E":
            east, west = _move(east, west, inst_value)
        elif instruction[0] == "W":
            west, east = _move(west, east, inst_value)
        elif instruction[0] in "LR":
            direction = _turn(direction, instruction)

    return sum((north, east, south, west))


def _move(dir_to_change: int, opposite: int, new_value: int) -> Tuple[int, int]:
    """
    Returns new values for (dir_to_change, opposite)
    """
    if opposite > 0:
        if opposite >= new_value:
            return 0, opposite - new_value
        return abs(opposite - new_value), 0

    return dir_to_change + new_value, 0


R_TURNS = {"E": 0, "S": 1, "W": 2, "N": 3}
L_TURNS = {"E": 0, "N": 1, "W": 2, "S": 3}


def _turn(current_dir: str, instruction: str) -> str:
    num_turns = int(instruction[1:]) / 90
    turns = R_TURNS if instruction[0] == "R" else L_TURNS
    turn = turns[current_dir] + num_turns
    return [*turns][int(turn % 4)]


def part_two(instructions: List[str]) -> int:
    """
    What is the Manhattan distance between the waypoint location and the ship's
    starting position?
    [64]
    """
    north, east, south, west = 0, 0, 0, 0
    # waypoint
    w_north, w_east, w_south, w_west = 1, 10, 0, 0

    for instruction in instructions:
        inst_value = int(instruction[1:])

        if instruction[0] == "N":
            w_north, w_south = _move(w_north, w_south, inst_value)
        elif instruction[0] == "S":
            w_south, w_north = _move(w_south, w_north, inst_value)
        elif instruction[0] == "E":
            w_east, w_west = _move(w_east, w_west, inst_value)
        elif instruction[0] == "W":
            w_west, w_east = _move(w_west, w_east, inst_value)
        elif instruction[0] in "LR":
            w_east, w_south, w_west, w_north = _part_two_turn(
                instruction, w_east, w_south, w_west, w_north
            )
        elif instruction[0] == "F":
            if w_north > 0:
                north, south = _move(north, south, inst_value * w_north)
            if w_south > 0:
                south, north = _move(south, north, inst_value * w_south)
            if w_east > 0:
                east, west = _move(east, west, inst_value * w_east)
            if w_west > 0:
                west, east = _move(west, east, inst_value * w_west)
    return sum((north, east, south, west))


PART_TWO_L_TURNS = {"E": -4, "S": -3, "W": -2, "N": -1}


def _part_two_turn(
    instruction: str, w_east: int, w_south: int, w_west: int, w_north: int
) -> Tuple[int, int, int, int]:
    """
    Returns new values for waypoints (w_east, w_south, w_west, w_north)
    """
    result = [0, 0, 0, 0]
    num_turns = int(instruction[1:]) / 90
    turns = R_TURNS if instruction[0] == "R" else PART_TWO_L_TURNS

    get_turn = (
        (lambda direction: int((turns[direction] + num_turns) % 4))
        if instruction[0] == "R"
        else (lambda direction: -(int(abs(turns[direction] - num_turns)) % 4))
    )

    result[get_turn("E")] = w_east
    result[get_turn("S")] = w_south
    result[get_turn("W")] = w_west
    result[get_turn("N")] = w_north

    return tuple(result)


def main():
    instructions = resources.as_list("12")

    timer(lambda: rain_risk(instructions))
    timer(lambda: part_two(instructions))


if __name__ == "__main__":
    main()
