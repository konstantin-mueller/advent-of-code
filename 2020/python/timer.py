from timeit import default_timer
from typing import Any, Callable


def timer(func: Callable[[], Any]) -> None:
    times = []
    for _ in range(1000):
        start = default_timer()
        func()
        times.append((default_timer() - start) * 1000)

    print(
        f"1000 iterations\tBest: {min(times):.5f}"
        f"\tAverage: {(sum(times) / len(times)):.5f}\t(milliseconds)"
    )
    print(f"Function result: {func()}\n\n")
