from typing import Set

import resources
from timer import timer


def report_repair(report: Set[int]) -> int:
    """
    Find the two entries that sum to 2020; what do you get if you multiply them together?
    """
    return next(value * (2020 - value) for value in report if 2020 - value in report)


def part_two(report: Set[int]) -> int:
    """
    what is the product of the three entries that sum to 2020?
    """
    for first in report:
        for second in report:
            if 2020 - (first + second) in report:
                return first * second * (2020 - (first + second))

    raise Exception("No solution found")


if __name__ == "__main__":
    report = resources.as_int_set("1")
    timer(lambda: report_repair(report))
    timer(lambda: part_two(report))
