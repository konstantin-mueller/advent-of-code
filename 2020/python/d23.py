from typing import Generator, List

from timer import timer


class Node:
    def __init__(self, value: int) -> None:
        self.value = value
        self.next_node = None

    def print(self) -> None:
        print([c.value for c in self])

    def __iter__(self) -> Generator["Node", None, None]:
        current = None
        while current != self:
            if current is None:
                current = self

            current = current.next_node
            yield current


def crab_cups(
    inp: List[int],
    cups_size: int = 1000000,
    loops: int = 10000000,
    part_one: bool = False,
) -> str:
    cups = {}
    prev = -1
    for i in inp:
        cups[i] = Node(i)
        if prev != -1:
            cups[prev].next_node = cups[i]
        prev = i

    for i in range(len(inp) + 1, cups_size + 1):
        cups[i] = Node(i)
        cups[prev].next_node = cups[i]
        prev = i
    cups[prev].next_node = cups[inp[0]]

    current = cups[inp[0]]
    for _ in range(loops):
        selected = (
            current.next_node.value,
            current.next_node.next_node.value,
            current.next_node.next_node.next_node.value,
        )
        current.next_node = current.next_node.next_node.next_node.next_node

        destination = None
        search_index = current.value - 1
        while destination is None:
            destination = next(
                (cups[j] for j in range(search_index, 0, -1) if j not in selected),
                None,
            )
            search_index = cups_size

        cups[selected[2]].next_node = destination.next_node
        destination.next_node = cups[selected[0]]

        current = current.next_node

    return (
        "".join(map(str, (c.value for c in cups[1] if c != cups[1])))
        if part_one
        else str(cups[1].next_node.value * cups[1].next_node.next_node.value)
    )


def main() -> None:
    inp = [int(c) for c in "952316487"]

    print(crab_cups(inp, cups_size=9, loops=100, part_one=True))
    print(crab_cups(inp))


if __name__ == "__main__":
    main()
