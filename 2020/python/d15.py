from typing import List

import resources
from timer import timer


def rambunctious_recitation(numbers: List[str], n: int) -> int:
    """
    Given your starting numbers, what will be the nth number spoken?
    """
    split_turns = numbers[0].split(",")
    # dict turns contains number: index
    turns = {int(num): i for i, num in enumerate(split_turns[:-1])}

    number = int(split_turns[-1])
    # Substract one because of zero based index
    for turn in range(len(split_turns) - 1, n - 1):
        last_turn = turns.get(number)
        turns[number] = turn
        number = 0 if last_turn is None else turn - last_turn
    return number


def main() -> None:
    numbers = resources.as_list("15")

    timer(lambda: rambunctious_recitation(numbers, 2020))
    print(rambunctious_recitation(numbers, 30000000))


if __name__ == "__main__":
    main()
