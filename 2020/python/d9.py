import itertools
from collections import deque
from typing import List

import resources
from timer import timer


def encoding_error(data: List[int], preamble: int = 25) -> int:
    """
    The first step of attacking the weakness in the XMAS data is to find the first
    number in the list (after the preamble) which is not the sum of two of the 25
    numbers before it. What is the first number that does not have this property?
    """
    return next(
        value
        for i, value in enumerate(data[preamble:])
        if value not in map(sum, itertools.combinations(data[i : i + preamble], 2))
    )


def part_two(data: List[int], find_set_with_sum_of: int) -> int:
    """
    find a contiguous set of at least two numbers in your list which sum to the invalid
    number from step 1. add together the smallest and largest number in this contiguous
    range.
    """
    for i in range(len(data) - 2):
        for j in range(i + 2, len(data)):
            combined = data[i:j]
            combined_sum = sum(combined)
            if find_set_with_sum_of == combined_sum:
                return min(combined) + max(combined)
            elif combined_sum > find_set_with_sum_of:
                break

    raise Exception("No solution found")


def main():
    data = resources.as_int_list("9")

    timer(lambda: encoding_error(data))

    result = encoding_error(data)
    print(part_two(data, result))
    # TODO: too slow
    # timer(lambda: part_two(data, result))


if __name__ == "__main__":
    main()
