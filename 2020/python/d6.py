from itertools import groupby
from typing import Generator, List, Tuple

import resources
from timer import timer


def custom_customs(answers: List[str]) -> int:
    """
    For each group, count the number of questions to which anyone answered "yes".
    What is the sum of those counts?
    """
    return sum(len(set(group)) for group, _ in _answers_per_group(answers))


def part_two(answers: List[str]) -> int:
    """
    For each group, count the number of questions to which everyone answered "yes".
    What is the sum of those counts?
    """
    return sum(
        len(
            [
                key
                for key, group in groupby(sorted(group_answers))
                if len(list(group)) == count
            ]
        )
        for group_answers, count in _answers_per_group(answers)
    )


def _answers_per_group(answers: List[str]) -> Generator[Tuple[str, int], None, None]:
    counter = 0
    group = ""

    for answer in answers:
        if answer == "":
            yield group, counter
            counter = 0
            group = ""
            continue

        group += answer
        counter += 1


if __name__ == "__main__":
    answers = resources.as_list("6")
    answers.append("")

    timer(lambda: custom_customs(answers))
    timer(lambda: part_two(answers))
