import itertools
from typing import Dict, List, Set

import resources
from timer import timer


def monster_messages(messages: List[str], inp_rules: List[str]) -> int:
    """How many messages completely match rule 0?"""
    rules = {}
    _parse_rule(inp_rules, 0, rules)
    return sum(1 for message in messages if message in rules[0])


def part_two(messages: List[str], inp_rules: List[str]) -> int:
    """
    Completely replace rules 8: 42 and 11: 42 31 with the following:
    8: 42 | 42 8
    11: 42 31 | 42 11 31

    After updating rules 8 and 11, how many messages completely match rule 0?
    """
    rules = {}
    _parse_rule(inp_rules, 42, rules)
    _parse_rule(inp_rules, 31, rules)

    result = len(messages)
    for message in messages:
        # Check if parts of the message are in one of the possibilities of rules 42 and
        # 31 because rule 0 is 8 11. Valid messages for rule 8 are parts of 42 because
        # rule 8 only contains rule 42 (rule 8 itself is one or more times rule 42).
        # Valid messages for rule 11 are beginning with rule 42 and ending with rule 31.
        if (
            not message[:8] in rules[42]
            or not message[8:16] in rules[42]
            or not message[-8:] in rules[31]
        ):
            result -= 1
            continue

        temp = message[16:-8]
        count = 0
        while len(temp) > 0 and temp[:8] in rules[42]:
            temp = temp[8:]
            count += 1

        sec_count = 0
        while len(temp) > 0 and temp[:8] in rules[31]:
            temp = temp[8:]
            sec_count += 1

        if len(temp) > 0 or sec_count > count:
            result -= 1

    return result


def _parse_rule(
    all_rules: List[str], rule_to_parse: int, rules: Dict[int, Set[str]]
) -> Set[str]:
    if rule_to_parse in rules:
        return rules[rule_to_parse]

    rule = next(
        split_rule
        for rule in all_rules
        if int((split_rule := rule.split())[0][:-1]) == rule_to_parse
    )[1:]

    if rule[0].startswith('"'):
        rules[rule_to_parse] = {rule[0].replace('"', "")}
        return rules[rule_to_parse]

    rules[rule_to_parse] = _parse(rule, all_rules, rules)
    return rules[rule_to_parse]


def _parse(
    rule: List[str], all_rules: List[str], rules: Dict[int, Set[str]]
) -> Set[str]:
    result = set()
    rule = ",".join(rule).split("|")
    for r in rule:
        possibilities = itertools.product(
            *(
                _parse_rule(all_rules, int(num), rules)
                for num in r.split(",")
                if num != ""
            )
        )
        result |= set(map("".join, possibilities))

    return result


def main() -> None:
    messages = resources.as_str("19")
    rules, messages = messages.split("\n\n")

    print(monster_messages(messages.split("\n"), rules.split("\n")))
    print(part_two(messages.split("\n"), rules.split("\n")))


if __name__ == "__main__":
    main()
