import itertools
from collections import defaultdict
from typing import List

from timer import timer


def conway_cubes(grid: List[str], dimensions: int = 3) -> int:
    """
    Starting with your given initial configuration, simulate six cycles. How many cubes
    are left in the active state after the sixth cycle?

    Part two: 4 dimensions
    """
    zeroes = (0,) * (dimensions - 2)
    cubes = {
        (x, y) + zeroes
        for x, line in enumerate(grid)
        for y, char in enumerate(line[:-1])
        if char == "#"
    }

    for _ in range(6):
        new_cubes = set()
        all_neighbors = defaultdict(int)

        for cube in cubes:
            neighbors = list(
                itertools.product(
                    *([cube[i] - 1, cube[i], cube[i] + 1] for i in range(len(cube)))
                )
            )
            neighbors.remove(cube)

            count = sum(1 for neighbor in neighbors if neighbor in cubes)
            if count == 2 or count == 3:
                new_cubes.add(cube)

            for neighbor in neighbors:
                if neighbor not in cubes:
                    all_neighbors[neighbor] += 1

        for neighbor, num_active in all_neighbors.items():
            if num_active == 3:
                new_cubes.add(neighbor)

        cubes = new_cubes

    return len(cubes)


def main() -> None:
    with open("../resources/17", "r") as _file:
        configuration = _file.readlines()

    timer(lambda: conway_cubes(configuration))
    print(conway_cubes(configuration, dimensions=4))


if __name__ == "__main__":
    main()
