from typing import List, Set

import resources
from timer import timer


def adapter_array(adapters: Set[int]) -> int:
    """
    What is the number of 1-jolt differences multiplied by the number of 3-jolt
    differences?
    """
    sorted_adapters = sorted(adapters)
    # Start with 0
    sorted_adapters.insert(0, 0)

    one_jolt_differences = _count_jolt_differences(sorted_adapters, 1)
    three_jolt_differences = _count_jolt_differences(sorted_adapters, 3)

    # There is one more three jolt difference because the device's built in adapter is
    # always 3 higher than the highest adapter
    return one_jolt_differences * (three_jolt_differences + 1)


def _count_jolt_differences(adapters: List[int], difference_to_count: int) -> int:
    return sum(
        1
        for i, value in enumerate(adapters)
        if i + 1 < len(adapters) and adapters[i + 1] - value == difference_to_count
    )


def part_two(adapters: Set[int]) -> int:
    """
    What is the total number of distinct ways you can arrange the adapters to connect
    the charging outlet to your device?
    """
    sorted_adapters = sorted(adapters)
    paths = {0: 1}
    for adapter in sorted_adapters:
        paths[adapter] = sum(paths.get(adapter - value, 0) for value in range(1, 4))

    return paths[sorted_adapters[-1]]


if __name__ == "__main__":
    adapters = resources.as_int_set("10")

    timer(lambda: adapter_array(adapters))
    timer(lambda: part_two(adapters))
