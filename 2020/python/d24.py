from typing import Generator, List, Tuple

import resources
from timer import timer


def lobby_layout(inp: List[str], days: int = 100) -> int:
    black_tiles = set()

    for line in inp:
        instructions = iter(line)

        x = y = 0.0
        while True:
            x_step = 1.0

            try:
                current = next(instructions)
                if current in "ns":
                    if current == "n":
                        y -= 0.5
                    else:
                        y += 0.5

                    current = next(instructions)
                    x_step = 0.5
            except StopIteration:
                break

            if current == "e":
                x += x_step
            else:
                x -= x_step

        coords = x, y
        if coords in black_tiles:
            black_tiles.remove(coords)
        else:
            black_tiles.add(coords)

    print("part 1: ", len(black_tiles))

    # Part 2:
    for _ in range(days):
        new = set()
        for tile in black_tiles:
            num_black_neighbor_tiles = sum(
                1 for neighbor in _get_neighbors(tile) if neighbor in black_tiles
            )
            # Black tiles in this range stay black
            if 0 < num_black_neighbor_tiles <= 2:
                new.add(tile)

            # All whites with 2 black neighbor tiles turn black
            new.update(
                neighbor
                for neighbor in _get_neighbors(tile)
                if neighbor not in black_tiles
                and sum(1 for n in _get_neighbors(neighbor) if n in black_tiles) == 2
            )

        black_tiles = new
    return len(black_tiles)


POSSIBILITIES = {
    (-1.0, 0.0),
    (-0.5, -0.5),
    (0.5, -0.5),
    (1.0, 0.0),
    (0.5, 0.5),
    (-0.5, 0.5),
}


def _get_neighbors(
    tile: Tuple[float, float]
) -> Generator[Tuple[float, float], None, None]:
    return ((tile[0] + x, tile[1] + y) for x, y in POSSIBILITIES)


def main() -> None:
    inp = resources.as_list("24")

    print(lobby_layout(inp))


if __name__ == "__main__":
    main()
