from typing import List

import resources
from timer import timer


def crab_combat(decks: str) -> int:
    """
    Play the small crab in a game of Combat using the two decks you just dealt. What is
    the winning player's score?
    """
    one, two = decks.split("\n\n")
    one = [int(card) for card in one.split("\n")[1:]]
    two = [int(card) for card in two.split("\n")[1:-1]]

    while len(one) > 0 and len(two) > 0:
        card_one = one[0]
        card_two = two[0]
        one = one[1:]
        two = two[1:]

        if card_one > card_two:
            one.append(card_one)
            one.append(card_two)
        else:
            two.append(card_two)
            two.append(card_one)

    deck = one if len(one) > 0 else two
    multipliers = range(len(deck), 0, -1)
    return sum(deck[i] * multipliers[i] for i in range(len(deck)))


def part_two(decks: str) -> int:
    """
    What is the winning player's score with the new rules of the game Recursive Combat?
    """
    one, two = decks.split("\n\n")
    one = [int(card) for card in one.split("\n")[1:]]
    two = [int(card) for card in two.split("\n")[1:-1]]

    winner = _recursive_combat(one, two)
    print(winner, one, two)

    deck = one if winner else two
    multipliers = range(len(deck), 0, -1)
    return sum(deck[i] * multipliers[i] for i in range(len(deck)))


def _recursive_combat(one: List[int], two: List[int]) -> bool:
    """Returns True if player one won. If player two won, False is returned."""
    previous_one = []
    previous_two = []

    while len(one) > 0 and len(two) > 0:
        current_deck_one = ",".join(map(str, one))
        current_deck_two = ",".join(map(str, two))
        if current_deck_one in previous_one and current_deck_two in previous_two:
            return True

        previous_one.append(current_deck_one)
        previous_two.append(current_deck_two)

        card_one = one[0]
        card_two = two[0]
        one[:] = one[1:]
        two[:] = two[1:]

        sub_game = None
        if len(one) >= card_one and len(two) >= card_two:
            sub_game = _recursive_combat(one[:card_one], two[:card_two])

        if (
            (sub_game is not None and sub_game)
            or sub_game is None
            and card_one > card_two
        ):
            one.append(card_one)
            one.append(card_two)
        else:
            two.append(card_two)
            two.append(card_one)

    return len(one) > 0


def main() -> None:
    decks = resources.as_str("22")

    print(crab_combat(decks))
    print(part_two(decks))


if __name__ == "__main__":
    main()
