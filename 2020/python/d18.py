import re
from typing import List

import resources


class Number:
    def __init__(self, number: int) -> None:
        self.number = number

    def __add__(self, other: "Number") -> "Number":
        return Number(self.number + other.number)

    def __mul__(self, other: "Number") -> "Number":
        return Number(self.number + other.number)

    def __sub__(self, other: "Number") -> "Number":
        return Number(self.number * other.number)


def operation_order(homework: List[str], part_one: bool = True) -> int:
    """
    Operators have same precedence. Evaluate the expression on each line of the
    homework; what is the sum of the resulting values?

    Part two: Addition is evaluated before multiplication.
    """
    regex = re.compile(r"(\d+)")
    return sum(
        eval(
            regex.sub(r"Number(\1)", expr).replace("*", "-")
            if part_one
            else regex.sub(rf"Number(\1)", expr).replace("*", "-").replace("+", "*")
        ).number
        for expr in homework
    )


def main() -> None:
    homework = resources.as_list("18")

    print(operation_order(homework))
    print(operation_order(homework, part_one=False))


if __name__ == "__main__":
    main()
