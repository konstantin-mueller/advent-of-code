import math
from typing import List, Tuple

import resources
from timer import timer


def toboggan_trajectory(grid: List[str], x: int = 3, y: int = 1) -> int:
    """
    Starting at the top-left corner of your map and following a slope of
    right 3 and down 1, how many trees would you encounter?
    """
    x_counter = 0
    y_counter = 0
    tree_counter = 0

    while True:
        x_counter += x
        y_counter += y
        if y_counter >= len(grid):
            break
        if x_counter >= len(grid[0]):
            x_counter -= len(grid[0])

        if grid[y_counter][x_counter] == "#":
            tree_counter += 1

    return tree_counter


def part_two(grid: List[str], traverse_xy: List[Tuple[int, int]]) -> int:
    """
    What do you get if you multiply together the number of trees encountered on each of
    the listed slopes? (parameter)
    """
    return math.prod(toboggan_trajectory(grid, x, y) for x, y in traverse_xy)


if __name__ == "__main__":
    grid = resources.as_list("3")
    timer(lambda: toboggan_trajectory(grid))
    timer(lambda: part_two(grid, [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]))
