use std::cmp;

pub fn encoding_error(data: &Vec<u64>, preamble: usize) -> u64 {
    let sum_exists = |vec: &[u64], i: usize| {
        vec.into_iter()
            .enumerate()
            .flat_map(|(j, value)| vec.iter().skip(j + 1).map(move |second| value + second))
            .find(|x| *x == data[i])
            .is_some()
    };

    let index = (preamble..data.len())
        .find(|i| !sum_exists(&data[i - preamble..*i], *i))
        .unwrap();
    data[index]
}

pub fn part_two(data: &Vec<u64>, find_set_with_sum_of: u64) -> u64 {
    for i in 0..data.len() - 2 {
        for j in cmp::max(2, i + 1)..data.len() {
            let combined = &data[i..j];
            let combined_sum: u64 = combined.into_iter().sum();
            if find_set_with_sum_of == combined_sum {
                return combined.into_iter().min().unwrap() + combined.into_iter().max().unwrap();
            } else if combined_sum > find_set_with_sum_of {
                break;
            }
        }
    }

    panic!("No solution found");
}
