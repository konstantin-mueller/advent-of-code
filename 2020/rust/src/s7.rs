use std::collections::BTreeSet;

use regex::Regex;

pub fn handy_haversacks(rules: &Vec<String>, bag: &str) -> usize {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(.+) bags contain").unwrap();
    }
    find_bag(rules, bag, &RE).len()
}

fn find_bag(rules: &Vec<String>, bag: &str, reg_ex: &Regex) -> BTreeSet<String> {
    let result: BTreeSet<String> = rules
        .into_iter()
        .filter(|rule| rule.contains(bag) && !rule.starts_with(bag))
        .map(|rule| reg_ex.captures(rule).unwrap()[1].to_string())
        .collect();

    let other_bags: BTreeSet<String> = result
        .iter()
        .flat_map(|current_bag| find_bag(rules, &current_bag, reg_ex))
        .collect();

    return result.union(&other_bags).cloned().collect();
}

pub fn part_two(rules: &Vec<String>, bag: &str) -> usize {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(?:(\d+) (.+?) bag[^\d\.]*?)+").unwrap();
    }
    find_containing_bags(rules, bag, &RE)
}

fn find_containing_bags(rules: &Vec<String>, bag: &str, reg_ex: &Regex) -> usize {
    let start_of_rule = format!("{} bags contain", bag);
    let rule = rules
        .into_iter()
        .filter(|r| r.starts_with(&start_of_rule))
        .next()
        .unwrap();

    reg_ex
        .captures_iter(rule)
        .map(|current_bag| {
            let num = current_bag[1].parse::<usize>().unwrap();
            num + num * find_containing_bags(rules, &current_bag[2], reg_ex)
        })
        .sum()
}
