use std::collections::BTreeMap;

struct Turns {
    right: BTreeMap<char, i64>,
    left: BTreeMap<char, i64>,
    keys_right: String,
    keys_left: String,
}

pub fn rain_risk(instructions: &Vec<String>) -> i64 {
    let turns = Turns {
        right: [('E', 0), ('S', 1), ('W', 2), ('N', 3)]
            .iter()
            .cloned()
            .collect(),
        left: [('E', 0), ('N', 1), ('W', 2), ('S', 3)]
            .iter()
            .cloned()
            .collect(),
        keys_right: "ESWN".to_string(),
        keys_left: "ENWS".to_string(),
    };

    let mut direction = 'E';
    let (mut north, mut east, mut south, mut west) = (0, 0, 0, 0);

    for instruction in instructions {
        let inst_value = &instruction[1..].parse::<i64>().unwrap();

        let mut inst_to_match = instruction.chars().nth(0).unwrap();
        if inst_to_match == 'F' {
            inst_to_match = direction;
        }

        match inst_to_match {
            'N' => {
                let moved = _move(north, south, *inst_value);
                north = moved.0;
                south = moved.1;
            }
            'S' => {
                let moved = _move(south, north, *inst_value);
                south = moved.0;
                north = moved.1;
            }
            'E' => {
                let moved = _move(east, west, *inst_value);
                east = moved.0;
                west = moved.1;
            }
            'W' => {
                let moved = _move(west, east, *inst_value);
                west = moved.0;
                east = moved.1;
            }
            'L' | 'R' => {
                direction = turn(&direction, instruction.to_string(), &turns);
            }
            _ => {
                panic!("Invalid instruction!");
            }
        }
    }
    north + east + south + west
}

fn _move(dir_to_change: i64, opposite: i64, new_value: i64) -> (i64, i64) {
    if opposite > 0 {
        if opposite >= new_value {
            return (0, opposite - new_value);
        }
        return ((opposite - new_value).abs(), 0);
    }
    (dir_to_change + new_value, 0)
}

fn turn(current_dir: &char, instruction: String, all_turns: &Turns) -> char {
    let num_turns = &instruction[1..].parse::<i64>().unwrap() / 90;
    let action = instruction.chars().nth(0).unwrap();
    let turns = if action == 'R' {
        &all_turns.right
    } else {
        &all_turns.left
    };
    let keys = if action == 'R' {
        &all_turns.keys_right
    } else {
        &all_turns.keys_left
    };

    let turn = turns[current_dir] + num_turns;
    keys.chars().nth((turn % 4) as usize).unwrap()
}

pub fn part_two(instructions: &Vec<String>) -> i64 {
    let turns = Turns {
        right: [('E', 0), ('S', 1), ('W', 2), ('N', 3)]
            .iter()
            .cloned()
            .collect(),
        left: [('E', -4), ('S', -3), ('W', -2), ('N', -1)]
            .iter()
            .cloned()
            .collect(),
        keys_right: String::new(),
        keys_left: String::new(),
    };

    let (mut north, mut east, mut south, mut west) = (0, 0, 0, 0);
    let (mut w_north, mut w_east, mut w_south, mut w_west) = (1, 10, 0, 0);

    for instruction in instructions {
        let inst_value = &instruction[1..].parse::<i64>().unwrap();

        match instruction.chars().nth(0).unwrap() {
            'N' => {
                let moved = _move(w_north, w_south, *inst_value);
                w_north = moved.0;
                w_south = moved.1;
            }
            'S' => {
                let moved = _move(w_south, w_north, *inst_value);
                w_south = moved.0;
                w_north = moved.1;
            }
            'E' => {
                let moved = _move(w_east, w_west, *inst_value);
                w_east = moved.0;
                w_west = moved.1;
            }
            'W' => {
                let moved = _move(w_west, w_east, *inst_value);
                w_west = moved.0;
                w_east = moved.1;
            }
            'L' | 'R' => {
                let turned = part_two_turn(
                    instruction.to_string(),
                    w_east,
                    w_south,
                    w_west,
                    w_north,
                    &turns,
                );
                w_east = turned.0;
                w_south = turned.1;
                w_west = turned.2;
                w_north = turned.3;
            }
            'F' => {
                if w_north > 0 {
                    let moved = _move(north, south, inst_value * w_north);
                    north = moved.0;
                    south = moved.1;
                }
                if w_south > 0 {
                    let moved = _move(south, north, inst_value * w_south);
                    south = moved.0;
                    north = moved.1;
                }
                if w_east > 0 {
                    let moved = _move(east, west, inst_value * w_east);
                    east = moved.0;
                    west = moved.1;
                }
                if w_west > 0 {
                    let moved = _move(west, east, inst_value * w_west);
                    west = moved.0;
                    east = moved.1;
                }
            }
            _ => {
                panic!("Invalid instruction!");
            }
        }
    }
    north + east + south + west
}

fn part_two_turn(
    instruction: String,
    w_east: i64,
    w_south: i64,
    w_west: i64,
    w_north: i64,
    all_turns: &Turns,
) -> (i64, i64, i64, i64) {
    let mut result: Vec<i64> = vec![0, 0, 0, 0];
    let num_turns = &instruction[1..].parse::<i64>().unwrap() / 90;
    let action = instruction.chars().nth(0).unwrap();
    let turns = if action == 'R' {
        &all_turns.right
    } else {
        &all_turns.left
    };

    let get_turn_right =
        &|direction: &char| -> usize { ((turns[direction] + num_turns) % 4) as usize }
            as &dyn Fn(&char) -> usize;
    let get_turn_left = &|direction: &char| -> usize {
        (((-((turns[direction] - num_turns).abs() % 4)) + 4) % 4) as usize
    };

    let get_turn = if action == 'R' {
        get_turn_right
    } else {
        get_turn_left
    };

    result[get_turn(&'E')] = w_east;
    result[get_turn(&'S')] = w_south;
    result[get_turn(&'W')] = w_west;
    result[get_turn(&'N')] = w_north;

    (result[0], result[1], result[2], result[3])
}
