use std::collections::BTreeMap;
use std::collections::BTreeSet;

pub fn ticket_translation(data: &String) -> i64 {
    let mut parts = data.split("\n\n");
    let rules = get_rules(parts.next().unwrap().split("\n")).0;
    parts.next();

    let mut result = 0;
    let lines = parts.next().unwrap().split("\n").collect::<Vec<&str>>();
    for i in 1..lines.len() - 1 {
        for num in lines[i].split(",") {
            let number = num.parse::<i64>().unwrap();
            if !rules
                .iter()
                .any(|rule| rule.0.contains(&number) || rule.1.contains(&number))
            {
                result += number;
            }
        }
    }
    result
}

pub fn part_two(data: &String) -> i64 {
    let mut parts = data.split("\n\n");
    let all_rules = get_rules(parts.next().unwrap().split("\n"));
    let rules = all_rules.0;
    let departure_rules = all_rules.1;
    let ticket_part = parts.next().unwrap();

    let mut results: BTreeMap<i64, BTreeSet<i64>> = BTreeMap::new();
    let lines = parts.next().unwrap().split("\n").collect::<Vec<&str>>();
    for i in 1..lines.len() - 1 {
        for (j, num) in lines[i].split(",").enumerate() {
            let number = num.parse::<i64>().unwrap();

            let valid: BTreeSet<i64> = rules
                .iter()
                .enumerate()
                .filter(|(_, rule)| rule.0.contains(&number) || rule.1.contains(&number))
                .map(|(ind, _)| ind as i64)
                .collect();

            if valid.len() > 0 {
                let index = j as i64;

                results.insert(
                    index,
                    if results.contains_key(&index) {
                        results[&index].intersection(&valid).cloned().collect()
                    } else {
                        valid
                    },
                );
            }
        }
    }

    get_result(&mut results, ticket_part, &departure_rules)
}

fn get_result(
    results: &mut BTreeMap<i64, BTreeSet<i64>>,
    ticket_part: &str,
    departure_rules: &Vec<i64>,
) -> i64 {
    let mut result: BTreeMap<i64, i64> = BTreeMap::new();
    while results.len() > 0 {
        let i_r = results
            .clone()
            .into_iter()
            .filter(|(_, res)| res.len() == 1)
            .next()
            .unwrap();
        let i = i_r.0;
        let r = &i_r.1.into_iter().last().unwrap();
        results.remove(&i);
        result.insert(i, *r);

        for (j, res) in results.clone() {
            let tem: BTreeSet<i64> = res.into_iter().filter(|num| num != r).collect();
            results.insert(j, tem);
        }
    }

    let ticket: Vec<i64> = ticket_part
        .split("\n")
        .nth(1)
        .unwrap()
        .split(",")
        .map(|number| number.parse::<i64>().unwrap())
        .collect();
    result
        .into_iter()
        .filter(|(_, r)| departure_rules.contains(r))
        .map(|(i, _)| ticket[i as usize])
        .product()
}

fn get_rules(
    inp_rules: std::str::Split<'_, &str>,
) -> (
    std::vec::Vec<(std::ops::Range<i64>, std::ops::Range<i64>)>,
    std::vec::Vec<i64>,
) {
    let mut rules = vec![];
    let mut departure_rules = vec![];

    for (i, line) in inp_rules.enumerate() {
        let rule = line.split(":").collect::<Vec<&str>>();
        if rule[0].starts_with("departure") {
            departure_rules.push(i as i64);
        }

        let cur_line = rule[1].split_whitespace().collect::<Vec<&str>>();
        let a = cur_line[0].split("-").collect::<Vec<&str>>();
        let b = cur_line[2].split("-").collect::<Vec<&str>>();

        rules.push((
            a[0].parse::<i64>().unwrap()..a[1].parse::<i64>().unwrap() + 1,
            b[0].parse::<i64>().unwrap()..b[1].parse::<i64>().unwrap() + 1,
        ));
    }
    (rules, departure_rules)
}
