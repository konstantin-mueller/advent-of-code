pub fn custom_customs(answers: &Vec<String>) -> usize {
    answers_per_group(answers)
        .map(|(group, _)| {
            let mut group_vec: Vec<_> = group.chars().collect();
            group_vec.sort();
            group_vec.dedup();
            group_vec.len()
        })
        .sum()
}

pub fn part_two(answers: &Vec<String>) -> usize {
    answers_per_group(answers)
        .map(|(mut group_answers, count)| {
            let mut result = 0;

            loop {
                if group_answers.is_empty() {
                    break;
                }

                let current_char = group_answers.chars().nth(0).unwrap();
                if group_answers.matches(current_char).count() == count {
                    result += 1;
                }
                group_answers = group_answers.replace(current_char, "");
            }

            result
        })
        .sum()
}

struct AnswersPerGroup<'a> {
    i: usize,
    counter: usize,
    group: String,
    answers: &'a Vec<String>,
}

impl Iterator for AnswersPerGroup<'_> {
    type Item = (String, usize);

    fn next(&mut self) -> Option<(String, usize)> {
        if self.i >= self.answers.len() {
            return None;
        }

        if self.answers[self.i].is_empty() {
            let counter = self.counter;
            let group = self.group.to_string();
            self.counter = 0;
            self.group = String::new();
            self.i += 1;
            Some((group, counter))
        } else {
            self.group.push_str(&self.answers[self.i]);
            self.counter += 1;
            self.i += 1;
            self.next()
        }
    }
}

fn answers_per_group(answers: &Vec<String>) -> AnswersPerGroup {
    AnswersPerGroup {
        i: 0,
        counter: 0,
        group: String::new(),
        answers: answers,
    }
}
