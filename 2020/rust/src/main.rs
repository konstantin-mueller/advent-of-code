#[macro_use]
extern crate lazy_static;
extern crate regex;

use std::time::Instant;

mod resources;
mod s1;
mod s10;
mod s11;
mod s12;
mod s13;
mod s14;
mod s15;
mod s16;
mod s17;
mod s19;
mod s2;
mod s3;
mod s4;
mod s5;
mod s6;
mod s7;
mod s8;
mod s9;

fn main() {
    // println!("Day 1");
    // one();
    // println!();

    // println!("Day 2");
    // two();
    // println!();

    // println!("Day 3");
    // three();
    // println!();

    // println!("Day 4");
    // four();
    // println!();

    // println!("Day 5");
    // five();
    // println!();

    // println!("Day 6");
    // six();
    // println!();

    // println!("Day 7");
    // seven();
    // println!();

    // println!("Day 8");
    // eight();
    // println!();

    // println!("Day 9");
    // nine();
    // println!();

    // println!("Day 10");
    // ten();
    // println!();

    // println!("Day 11");
    // eleven();
    // println!();

    // println!("Day 12");
    // twelve();
    // println!();

    // println!("Day 13");
    // thirteen();
    // println!();

    // println!("Day 14");
    // fourteen();
    // println!();

    // println!("Day 15");
    // fifteen();
    // println!();

    // println!("Day 16");
    // sixteen();
    // println!();

    // println!("Day 17");
    // seventeen();
    // println!();

    println!("Day 19");
    nineteen();
    println!();
}

fn timer<T>(func: &dyn Fn() -> T)
where
    T: std::fmt::Display,
{
    let mut times = Vec::new();
    for _ in 0..1000 {
        let start = Instant::now();
        func();
        times.push(start.elapsed().as_micros());
    }

    println!(
        "1000 iterations\tBest: {:.5}\tAverage: {:.5}\t(milliseconds)",
        *(times.iter().min().unwrap()) as f32 / 1000.0,
        times.iter().sum::<u128>() as f32 / times.len() as f32 / 1000.0
    );
    println!("Function result: {}\n\n", func());
}

fn one() {
    let report = resources::as_int_set(include_bytes!("../../resources/1"));

    timer(&|| s1::report_repair(&report));
    timer(&|| s1::part_two(&report));
}

fn two() {
    let passwords = resources::as_list(include_bytes!("../../resources/2"));

    timer(&|| s2::password_philosophy(&passwords).unwrap());
    timer(&|| s2::part_two(&passwords).unwrap());
}

fn three() {
    let grid = resources::as_list(include_bytes!("../../resources/3"));

    timer(&|| s3::toboggan_trajectory(&grid, s3::Path::default()).unwrap());
    timer(&|| s3::part_two(&grid, &vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]).unwrap());
}

fn four() {
    let mut passports = resources::as_list(include_bytes!("../../resources/4"));
    if passports.last().unwrap() != "" {
        passports.push("".to_string());
    }

    timer(&|| s4::passport_processing(&passports));
    println!("{}", s4::part_two(&passports).unwrap());
    // TODO: s4::part_two is very slow!
    // timer(&|| s4::part_two(&passports).unwrap());
}

fn five() {
    let passes = resources::as_list(include_bytes!("../../resources/5"));

    timer(&|| s5::binary_boarding(&passes).unwrap());
    // TODO: slow
    // timer(&|| s5::part_two(&passes).unwrap());
    println!("{}", s5::part_two(&passes).unwrap());
}

fn six() {
    let mut answers = resources::as_list(include_bytes!("../../resources/6"));
    answers.push(String::new());

    timer(&|| s6::custom_customs(&answers));
    timer(&|| s6::part_two(&answers));
}

fn seven() {
    let rules = resources::as_list(include_bytes!("../../resources/7"));

    println!("{}", s7::handy_haversacks(&rules, "shiny gold"));
    // TODO: very slow
    // timer(&|| s7::handy_haversacks(&rules, "shiny gold"));
    timer(&|| s7::part_two(&rules, "shiny gold"));
}

fn eight() {
    let instructions = resources::as_list(include_bytes!("../../resources/8"));

    timer(&|| s8::handheld_halting(&instructions));
    println!("{}", s8::part_two(&instructions));
    // TODO: slow
    // timer(&|| s8::part_two(&instructions));
}

fn nine() {
    let data = resources::as_list(include_bytes!("../../resources/9"));

    let result = s9::encoding_error(&data, 25);
    timer(&|| s9::encoding_error(&data, 25));
    println!("{}", s9::part_two(&data, result));
    // TODO: slow
    // timer(&|| s9::part_two(&data, result));
}

fn ten() {
    let adapters = resources::as_list(include_bytes!("../../resources/10"));

    timer(&|| s10::adapter_array(&adapters));
    timer(&|| s10::part_two(&adapters));
}

fn eleven() {
    let seats = resources::as_list_of_lists(include_bytes!("../../resources/11"));

    println!("{}", s11::seating_system(&seats));
    println!("{}", s11::part_two(&seats));
}

fn twelve() {
    let instructions = resources::as_list(include_bytes!("../../resources/12"));

    timer(&|| s12::rain_risk(&instructions));
    timer(&|| s12::part_two(&instructions));
}

fn thirteen() {
    let busses = resources::as_list(include_bytes!("../../resources/13"));

    timer(&|| s13::shuttle_search(&busses));
    timer(&|| s13::part_two(&busses));
}

fn fourteen() {
    let program = resources::as_list(include_bytes!("../../resources/14"));

    println!("{}", s14::docking_data(&program));
    println!("{}", s14::part_two(&program));
    // timer(&|| s14::docking_data(&program));
    // timer(&|| s14::part_two(&program));
}

fn fifteen() {
    let numbers = resources::as_list(include_bytes!("../../resources/15"));

    timer(&|| s15::rambunctious_recitation(&numbers, 2020));
    println!("{}", s15::rambunctious_recitation(&numbers, 30000000));
}

fn sixteen() {
    let data = String::from_utf8_lossy(include_bytes!("../../resources/16")).to_string();

    timer(&|| s16::ticket_translation(&data));
    println!("{}", s16::part_two(&data));
}

fn seventeen() {
    let configuration = resources::as_list(include_bytes!("../../resources/17"));

    println!("{}", s17::conway_cubes(&configuration, 3));
    println!("{}", s17::conway_cubes(&configuration, 4));
}

fn nineteen() {
    let inp = resources::as_string(include_bytes!("../../resources/19"));
    let mut iter_messages = inp.split("\n\n");
    let rules = iter_messages.next().unwrap();
    let messages = iter_messages.next().unwrap();

    println!(
        "{}",
        s19::monster_messages(messages.split("\n"), rules.split("\n"))
    );
    println!(
        "{}",
        s19::part_two(
            &messages.split("\n").map(|s| s.to_string()).collect(),
            rules.split("\n")
        )
    );
}
