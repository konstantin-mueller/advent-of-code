pub fn binary_boarding(passes: &Vec<String>) -> Option<usize> {
    passes
        .into_iter()
        .map(|pass| get_seat_id(pass.to_string()).unwrap())
        .max()
}

pub fn part_two(passes: &Vec<String>) -> Option<usize> {
    let all_ids: Vec<usize> = passes
        .into_iter()
        .map(|pass| get_seat_id(pass.to_string()).unwrap())
        .collect();
    Some(
        1 + all_ids
            .iter()
            .filter(|seat_id| {
                (all_ids.contains(&(*seat_id - 2)) && !all_ids.contains(&(*seat_id - 1)))
                    || (all_ids.contains(&(*seat_id + 2)) && !all_ids.contains(&(*seat_id + 1)))
            })
            .min()?,
    )
}

fn get_seat_id(boarding_pass: String) -> Option<usize> {
    let row = find(boarding_pass[..7].to_string(), Numbers::default());
    let column = find(
        boarding_pass[boarding_pass.len() - 3..].to_string(),
        Numbers {
            max: 7,
            ..Numbers::default()
        },
    );
    Some(row? * 8 + column?)
}

struct Numbers {
    min: usize,
    max: usize,
}

impl Default for Numbers {
    fn default() -> Self {
        Numbers { min: 0, max: 127 }
    }
}

fn find(boarding_pass: String, numbers: Numbers) -> Option<usize> {
    if boarding_pass.is_empty() {
        return Some(numbers.max);
    }

    let next_num = (numbers.max - numbers.min) as f64 / 2.0 + numbers.min as f64;

    match boarding_pass.chars().next()? {
        'F' | 'L' => find(
            boarding_pass[1..].to_string(),
            Numbers {
                min: numbers.min,
                max: next_num.floor() as usize,
            },
        ),
        'B' | 'R' => find(
            boarding_pass[1..].to_string(),
            Numbers {
                min: next_num.ceil() as usize,
                max: numbers.max,
            },
        ),
        _ => None,
    }
}
