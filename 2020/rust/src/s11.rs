pub fn seating_system(seats: &Vec<Vec<char>>) -> u64 {
    return get_occupied_seats(seats, true);
}

pub fn part_two(seats: &Vec<Vec<char>>) -> u64 {
    return get_occupied_seats(seats, false);
}

fn get_occupied_seats(seats: &Vec<Vec<char>>, is_part_one: bool) -> u64 {
    let mut previous = seats.clone();
    let mut result = seats.clone();
    let mut stabilized = true;

    let indices = if is_part_one {
        product(&[-1, 0, 1], &[-1, 0, 1])
    } else {
        vec![]
    };

    let movement_funcs = if is_part_one {
        vec![]
    } else {
        get_movement_funcs()
    };

    loop {
        for i in 0..seats.len() {
            for j in 0..seats[0].len() {
                let changed: bool;

                if is_part_one {
                    let applied = apply_rules(
                        i,
                        j,
                        &previous,
                        &mut || part_one_adjacent_fun(i, j, &indices, &previous),
                        4,
                    );
                    result[i][j] = applied.0;
                    changed = applied.1;
                } else {
                    let applied = apply_rules(
                        i,
                        j,
                        &previous,
                        &mut || {
                            part_two_adjacent_fun(
                                i as isize,
                                j as isize,
                                &movement_funcs,
                                &previous,
                            )
                        },
                        5,
                    );
                    result[i][j] = applied.0;
                    changed = applied.1;
                }

                if stabilized && changed {
                    stabilized = false;
                }
            }
        }
        if stabilized {
            return result
                .into_iter()
                .map(|row| row.into_iter().filter(|seat| seat == &'#').count())
                .sum::<usize>() as u64;
        }
        stabilized = true;
        previous = result.clone();
    }
}

fn part_one_adjacent_fun(
    i: usize,
    j: usize,
    indices: &Vec<(i64, i64)>,
    current_seats: &Vec<Vec<char>>,
) -> Vec<char> {
    indices
        .iter()
        .filter(move |(x, y)| {
            !(*x == 0 && *y == 0)
                && 0 <= x + i as i64
                && x + (i as i64) < current_seats.len() as i64
                && 0 <= y + j as i64
                && y + (j as i64) < current_seats[0].len() as i64
        })
        .map(move |(x, y)| current_seats[(x + i as i64) as usize][(y + j as i64) as usize])
        .collect()
}

fn part_two_adjacent_fun(
    i: isize,
    j: isize,
    movement_funcs: &Vec<Box<dyn Fn(isize, isize) -> (isize, isize)>>,
    current_seats: &Vec<Vec<char>>,
) -> Vec<char> {
    movement_funcs
        .iter()
        .filter_map(|func| find_first_seat(i, j, current_seats, func))
        .collect()
}

fn product(a: &[i64], b: &[i64]) -> Vec<(i64, i64)> {
    let mut result = Vec::new();
    for x in a {
        for y in b {
            result.push((*x, *y));
        }
    }
    result
}

fn get_movement_funcs() -> Vec<Box<dyn Fn(isize, isize) -> (isize, isize)>> {
    let up = |i: isize, j: isize| (i - 1, j);
    let up_right = |i: isize, j: isize| (i - 1, j + 1);
    let right = |i: isize, j: isize| (i, j + 1);
    let down_right = |i: isize, j: isize| (i + 1, j + 1);
    let down = |i: isize, j: isize| (i + 1, j);
    let down_left = |i: isize, j: isize| (i + 1, j - 1);
    let left = |i: isize, j: isize| (i, j - 1);
    let up_left = |i: isize, j: isize| (i - 1, j - 1);

    vec![
        Box::new(up),
        Box::new(up_right),
        Box::new(right),
        Box::new(down_right),
        Box::new(down),
        Box::new(down_left),
        Box::new(left),
        Box::new(up_left),
    ]
}

fn find_first_seat(
    i: isize,
    j: isize,
    seats: &Vec<Vec<char>>,
    get_next_index: &dyn Fn(isize, isize) -> (isize, isize),
) -> Option<char> {
    let (mut i, mut j) = get_next_index(i, j);
    while 0 <= i && i < seats.len() as isize && 0 <= j && j < seats[0].len() as isize {
        if "#L".contains(seats[i as usize][j as usize]) {
            return Some(seats[i as usize][j as usize]);
        }
        let index = get_next_index(i, j);
        i = index.0;
        j = index.1;
    }
    None
}

fn apply_rules(
    i: usize,
    j: usize,
    seats: &Vec<Vec<char>>,
    get_adjacent_func: &dyn Fn() -> Vec<char>,
    num_occupied_seats_for_empty: usize,
) -> (char, bool) {
    let seat = seats[i][j];
    if seat == '.' {
        return ('.', false);
    }

    let adjacent = get_adjacent_func();
    if seat == 'L' && adjacent.iter().all(|adj| *adj != '#') {
        return ('#', true);
    }
    if seat == '#'
        && adjacent.into_iter().filter(|adj| *adj == '#').count() >= num_occupied_seats_for_empty
    {
        return ('L', true);
    }
    (seat, false)
}
