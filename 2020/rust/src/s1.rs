use std::collections::BTreeSet;

pub fn report_repair(report: &BTreeSet<i32>) -> i32 {
    for value in report {
        if report.contains(&(2020 - value)) {
            return value * (2020 - value);
        }
    }

    panic!("No solution found");
}

pub fn part_two(report: &BTreeSet<i32>) -> i32 {
    for first in report {
        for second in report {
            if report.contains(&(2020 - (first + second))) {
                return first * second * (2020 - (first + second));
            }
        }
    }

    panic!("No solution found");
}
