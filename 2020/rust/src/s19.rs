use std::collections::{BTreeMap, BTreeSet};
use std::str::Split;

pub fn monster_messages(messages: Split<&str>, inp_rules: Split<&str>) -> usize {
    let mut rules = BTreeMap::new();
    parse_rule(inp_rules, 0, &mut rules).unwrap();
    messages
        .filter(|message| rules.get(&0).unwrap().contains(&message.to_string()))
        .count()
}

pub fn part_two(messages: &Vec<String>, inp_rules: Split<&str>) -> usize {
    let mut rules = BTreeMap::new();
    parse_rule(inp_rules.clone(), 42, &mut rules).unwrap();
    parse_rule(inp_rules, 31, &mut rules).unwrap();

    let mut result = messages.len();
    for message in messages {
        if !rules[&42].contains(&message.chars().take(8).collect::<String>())
            || !rules[&42].contains(&message.chars().skip(8).take(16).collect::<String>())
            || !rules[&31].contains(&message.chars().skip(message.len() - 8).collect::<String>())
        {
            result -= 1;
            continue;
        }

        let mut temp: String = message
            .chars()
            .skip(16)
            .take(message.len() - 16 - 8)
            .collect();
        let mut count = 0;
        while temp.len() > 0 && rules[&42].contains(&temp.chars().take(8).collect::<String>()) {
            temp = temp.chars().take(8).collect();
            count += 1;
        }

        let mut sec_count = 0;
        while temp.len() > 0 && rules[&31].contains(&temp.chars().take(8).collect::<String>()) {
            temp = temp.chars().take(8).collect();
            sec_count += 1;
        }

        if temp.len() > 0 || sec_count > count {
            result -= 1;
        }
    }
    result
}

fn parse_rule<'a>(
    all_rules: Split<&str>,
    rule_to_parse: u64,
    rules: &'a mut BTreeMap<u64, BTreeSet<String>>,
) -> std::option::Option<&'a BTreeSet<String>> {
    if rules.contains_key(&rule_to_parse) {
        return rules.get(&rule_to_parse);
    }

    let rule: Vec<&str> = all_rules
        .clone()
        .filter(|r| {
            r.split_whitespace()
                .next()
                .unwrap()
                .trim_matches(':')
                .parse::<u64>()
                .unwrap()
                == rule_to_parse
        })
        .next()?
        .split_whitespace()
        .skip(1)
        .collect();

    if rule[0].starts_with("\"") {
        let mut set = BTreeSet::new();
        set.insert(rule[0].replace("\"", ""));
        rules.insert(rule_to_parse, set);
        return rules.get(&rule_to_parse);
    }

    let mut parsed = BTreeSet::new();
    let joined = rule.join(",");
    let current_rule = joined.split("|");
    for r in current_rule {
        let mut numbers = Vec::new();
        for num in r.split(",") {
            if num.is_empty() {
                continue;
            }

            match parse_rule(all_rules.clone(), num.parse().ok()?, rules) {
                None => continue,
                Some(res) => numbers.push(res.into_iter().cloned().collect()),
            }
        }

        let possibilities = cartesian_product(&numbers)
            .into_iter()
            .map(|p| p.join(""))
            .collect();

        parsed = parsed.union(&possibilities).cloned().collect();
    }

    rules.insert(rule_to_parse, parsed);
    rules.get(&rule_to_parse)
}

// ----------------------------------------------------------------------------------
// From https://gist.github.com/kylewlacy/115965b40e02a3325558 (slightly modified)

// Given a vector containing a partial Cartesian product, and a list of items,
/// return a vector adding the list of items to the partial Cartesian product.
///
/// # Example
///
/// ```
/// let partial_product = vec![vec![1, 4], vec![1, 5], vec![2, 4], vec![2, 5]];
/// let items = &[6, 7];
/// let next_product = partial_cartesian(partial_product, items);
/// assert_eq!(next_product, vec![vec![1, 4, 6],
///                               vec![1, 4, 7],
///                               vec![1, 5, 6],
///                               vec![1, 5, 7],
///                               vec![2, 4, 6],
///                               vec![2, 4, 7],
///                               vec![2, 5, 6],
///                               vec![2, 5, 7]]);
/// ```
pub fn partial_cartesian<T: Clone>(a: Vec<Vec<T>>, b: &Vec<T>) -> Vec<Vec<T>> {
    a.into_iter()
        .flat_map(|xs| {
            b.iter()
                .cloned()
                .map(|y| {
                    let mut vec = xs.clone();
                    vec.push(y);
                    vec
                })
                .collect::<Vec<_>>()
        })
        .collect()
}

/// Computes the Cartesian product of lists[0] * lists[1] * ... * lists[n].
///
/// # Example
///
/// ```
/// let lists: &[&[_]] = &[&[1, 2], &[4, 5], &[6, 7]];
/// let product = cartesian_product(lists);
/// assert_eq!(product, vec![vec![1, 4, 6],
///                          vec![1, 4, 7],
///                          vec![1, 5, 6],
///                          vec![1, 5, 7],
///                          vec![2, 4, 6],
///                          vec![2, 4, 7],
///                          vec![2, 5, 6],
///                          vec![2, 5, 7]]);
/// ```
pub fn cartesian_product<T: Clone>(lists: &Vec<Vec<T>>) -> Vec<Vec<T>> {
    match lists.split_first() {
        Some((first, rest)) => {
            let init: Vec<Vec<T>> = first.iter().cloned().map(|n| vec![n]).collect();

            rest.iter()
                .cloned()
                .fold(init, |vec, list| partial_cartesian(vec, &list))
        }
        None => {
            vec![]
        }
    }
}
