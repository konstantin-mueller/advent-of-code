use std::collections::HashMap;

use regex::Regex;

static REQUIRED_FIELDS: [&str; 7] = ["byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:"];

pub fn passport_processing(passports: &Vec<String>) -> isize {
    let mut valid_passports = 0;

    let mut field_counter = 0;
    for i in 0..passports.len() {
        if passports[i] == "" {
            if field_counter == REQUIRED_FIELDS.len() {
                valid_passports += 1;
            }

            field_counter = 0;
            continue;
        }

        field_counter += REQUIRED_FIELDS
            .iter()
            .filter(|&field| passports[i].contains(field))
            .count();
    }

    valid_passports
}

pub fn part_two(passports: &Vec<String>) -> Option<isize> {
    type Validator = Box<dyn Fn(&str) -> Option<bool>>;
    static VALID_COLORS: [&str; 7] = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];

    let rules: HashMap<&str, Validator> = vec![
        (
            "byr:",
            Box::new(|value: &str| {
                Some(1920 <= value.parse::<isize>().ok()? && value.parse::<isize>().ok()? <= 2002)
            }) as Validator,
        ),
        (
            "iyr:",
            Box::new(|value: &str| {
                Some(2010 <= value.parse::<isize>().ok()? && value.parse::<isize>().ok()? <= 2020)
            }),
        ),
        (
            "eyr:",
            Box::new(|value: &str| {
                Some(2020 <= value.parse::<isize>().ok()? && value.parse::<isize>().ok()? <= 2030)
            }),
        ),
        (
            "hgt:",
            Box::new(|value: &str| {
                Some(
                    (value.ends_with("cm")
                        && 150 <= value.replace("cm", "").parse::<isize>().ok()?
                        && value.replace("cm", "").parse::<isize>().ok()? <= 193)
                        || (value.ends_with("in")
                            && 59 <= value.replace("in", "").parse::<isize>().ok()?
                            && value.replace("in", "").parse::<isize>().ok()? <= 76),
                )
            }),
        ),
        (
            "hcl:",
            Box::new(|value: &str| Some(Regex::new(r"^#[0-9a-f]{6}$").ok()?.is_match(value))),
        ),
        (
            "ecl:",
            Box::new(|value: &str| Some(VALID_COLORS.contains(&value))),
        ),
        (
            "pid:",
            Box::new(|value: &str| Some(Regex::new(r"^\d{9}$").ok()?.is_match(value))),
        ),
    ]
    .into_iter()
    .collect();

    let mut joined_fields = String::from("(") + &REQUIRED_FIELDS.join("|");
    joined_fields.push_str(r")(\S+)");
    let regex_get_value = Regex::new(&joined_fields).ok()?;

    let mut valid_passports = 0;

    let mut field_counter = 0;
    for i in 0..passports.len() {
        if passports[i] == "" {
            if field_counter == REQUIRED_FIELDS.len() {
                valid_passports += 1;
            }

            field_counter = 0;
            continue;
        }

        field_counter += regex_get_value
            .captures_iter(&passports[i])
            .filter(|rmatch| rules[&rmatch[1]](&rmatch[2]).unwrap())
            .count();
    }

    Some(valid_passports)
}
