use std::collections::HashMap;

pub fn rambunctious_recitation(numbers: &Vec<String>, n: u64) -> u64 {
    let split_turns: Vec<&str> = numbers[0].split(",").collect();

    let mut turns: HashMap<u64, u64> = split_turns[0..split_turns.len() - 1]
        .into_iter()
        .enumerate()
        .map(|(i, num)| (num.parse::<u64>().unwrap(), i as u64))
        .collect();

    let mut number = split_turns.iter().last().unwrap().parse::<u64>().unwrap();
    // Substract one because of zero based index
    for turn in split_turns.into_iter().count() as u64 - 1..n - 1 {
        let prev_num = number;
        number = match turns.get(&number) {
            None => 0,
            Some(previous) => turn - previous,
        };
        *turns.entry(prev_num).or_insert(prev_num) = turn;
    }
    number
}
