use std::collections::HashMap;

use regex::Regex;

pub fn docking_data(program: &Vec<String>) -> i64 {
    let mut result: HashMap<i64, i64> = HashMap::new();

    lazy_static! {
        static ref RE: Regex = Regex::new(r"mem\[(\d+)\] = (\d+)").unwrap();
    }

    let mut i = 0;
    while i < program.len() {
        let mask: String = program[i].replace("mask = ", "").chars().rev().collect();
        let mut mem_count = 1;
        for (mem_counter, mem) in program[i + 1..].into_iter().enumerate() {
            if mem.starts_with("mask") {
                mem_count = mem_counter;
                break;
            }

            let r_match = RE.captures(mem).unwrap();
            let index = r_match[1].parse::<i64>().unwrap();
            let value = format!("{:b}", r_match[2].parse::<i64>().unwrap())
                .chars()
                .rev()
                .collect::<String>();

            let bin_result: String = value
                .chars()
                .enumerate()
                .map(|(j, bit)| (mask.chars().nth(j).unwrap(), bit))
                .map(|(mask_char, bit)| {
                    if mask_char == 'X' || mask_char == bit {
                        bit
                    } else {
                        mask_char
                    }
                })
                .collect::<String>()
                + &mask[value.len()..].replace("X", "0");

            let binary = bin_result.chars().rev().collect::<String>();
            result.insert(index, i64::from_str_radix(&binary, 2).unwrap());

            mem_count = mem_counter;
        }
        i += mem_count + 1;
    }
    result.values().sum()
}

pub fn part_two(program: &Vec<String>) -> i64 {
    let mut result: HashMap<i64, i64> = HashMap::new();

    lazy_static! {
        static ref RE: Regex = Regex::new(r"mem\[(\d+)\] = (\d+)").unwrap();
    }

    let mut i = 0;
    while i < program.len() {
        let mask: String = program[i].replace("mask = ", "").chars().rev().collect();
        let mut mem_count = 1;
        for (mem_counter, mem) in program[i + 1..].into_iter().enumerate() {
            if mem.starts_with("mask") {
                mem_count = mem_counter;
                break;
            }

            let r_match = RE.captures(mem).unwrap();
            let address = format!("{:b}", r_match[1].parse::<i64>().unwrap())
                .chars()
                .rev()
                .collect::<String>();
            let value = r_match[2].parse::<i64>().unwrap();

            let mut bin_results = vec![String::new()];
            for (j, bit) in mask.chars().enumerate() {
                let mut x = false;
                let current_address = if j < address.len() {
                    address.chars().nth(j).unwrap().to_string()
                } else {
                    "0".to_string()
                };

                for k in 0..bin_results.len() {
                    bin_results[k] += match bit {
                        '0' => &current_address,
                        '1' => "1",
                        _ => {
                            x = true;
                            "0"
                        }
                    };
                }

                if x {
                    bin_results.extend(
                        bin_results
                            .clone()
                            .into_iter()
                            .map(|addr| addr[..addr.len() - 1].to_string() + "1"),
                    )
                }
            }

            for j in bin_results
                .into_iter()
                .map(|res| i64::from_str_radix(&res.chars().rev().collect::<String>(), 2).unwrap())
            {
                result.insert(j, value);
            }
            mem_count = mem_counter;
        }
        i += mem_count + 1;
    }
    result.values().sum()
}
