pub struct Path {
    x: usize,
    y: usize,
}

impl Default for Path {
    fn default() -> Self {
        Path { x: 3, y: 1 }
    }
}

pub fn toboggan_trajectory(grid: &Vec<String>, path: Path) -> Option<usize> {
    let mut x_counter = 0;
    let mut y_counter = 0;
    let mut tree_counter = 0;

    loop {
        x_counter += path.x;
        y_counter += path.y;
        if y_counter >= grid.len() {
            break;
        }
        if x_counter >= grid[0].len() {
            x_counter -= grid[0].len();
        }

        if grid[y_counter].chars().nth(x_counter)? == '#' {
            tree_counter += 1;
        }
    }

    Some(tree_counter)
}

pub fn part_two(grid: &Vec<String>, traverse_xy: &Vec<(usize, usize)>) -> Option<usize> {
    traverse_xy
        .iter()
        .map(|(x, y)| toboggan_trajectory(grid, Path { x: *x, y: *y }))
        .product()
}
