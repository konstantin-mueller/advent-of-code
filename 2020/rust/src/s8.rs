pub fn handheld_halting(instructions: &Vec<String>) -> i32 {
    run_code(instructions, -1).1
}

pub fn part_two(instructions: &Vec<String>) -> i32 {
    instructions
        .into_iter()
        .enumerate()
        .filter(|(_, instruction)| instruction.starts_with("nop") || instruction.starts_with("jmp"))
        .map(|(i, _)| run_code(instructions, i as i32))
        .filter_map(|result| if result.0 { Some(result.1) } else { None })
        .next()
        .unwrap()
}

fn run_code(instructions: &Vec<String>, switch_nop_jmp_at_index: i32) -> (bool, i32) {
    let mut counters = vec![0; instructions.len()];
    let mut accumulator: i32 = 0;
    let mut i = 0;
    while i < instructions.len() {
        if counters[i] > 0 {
            return (false, accumulator);
        }
        counters[i] += 1;

        let mut current = instructions[i].split_whitespace();
        let mut instruction = current.next().unwrap();
        let action = current.next().unwrap().parse::<isize>().unwrap();
        if switch_nop_jmp_at_index >= 0 && i as i32 == switch_nop_jmp_at_index {
            if instruction == "nop" {
                instruction = "jmp";
            } else if instruction == "jmp" {
                instruction = "nop";
            }
        }

        if instruction == "acc" {
            accumulator += action as i32;
        } else if instruction == "jmp" {
            i = (i as isize + action) as usize;
            continue;
        }
        i += 1;
    }
    (true, accumulator)
}
