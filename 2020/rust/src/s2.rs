use regex::Regex;

pub fn password_philosophy(passwords: &Vec<String>) -> Option<i32> {
    let mut valid_counter = 0;

    let regex = Regex::new(r"-| ").unwrap();

    for pw in passwords {
        let mut pw_it = pw.split(": ");
        let policy = pw_it.next()?;
        let password = pw_it.next()?;

        let mut policy_it = regex.split(policy);
        let at_least = policy_it.next()?.parse::<usize>().unwrap();
        let at_most = policy_it.next()?.parse::<usize>().unwrap();
        let letter = policy_it.next()?;

        let occurrences = password.matches(letter).count();
        if at_least <= occurrences && occurrences <= at_most {
            valid_counter += 1;
        }
    }

    Some(valid_counter)
}

pub fn part_two(passwords: &Vec<String>) -> Option<i32> {
    let mut valid_counter = 0;

    let regex = Regex::new(r"-| ").unwrap();

    for pw in passwords {
        let mut pw_it = pw.split(": ");
        let policy = pw_it.next()?;
        let password = pw_it.next()?.to_string();

        let mut policy_it = regex.split(policy);
        let at_least = policy_it.next()?.parse::<usize>().unwrap();
        let at_most = policy_it.next()?.parse::<usize>().unwrap();
        let letter = policy_it.next()?.chars().next()?;

        let first = password.chars().nth(at_least - 1)?;
        let second = password.chars().nth(at_most - 1)?;

        if (first == letter && second != letter) || (second == letter && first != letter) {
            valid_counter += 1;
        }
    }

    Some(valid_counter)
}
