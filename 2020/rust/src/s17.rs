use std::collections::BTreeMap;
use std::collections::BTreeSet;

pub fn conway_cubes(grid: &Vec<String>, dimensions: u32) -> usize {
    let zeroes: Vec<i64> = vec![0; (dimensions - 2) as usize];
    let mut cubes: BTreeSet<Vec<i64>> = BTreeSet::new();

    for (x, line) in grid.into_iter().enumerate() {
        for (y, cha) in line.chars().enumerate() {
            if cha == '#' {
                let mut temp = vec![x as i64, y as i64];
                temp.extend(&zeroes);
                cubes.insert(temp);
            }
        }
    }

    for _ in 0..6 {
        let mut new_cubes: BTreeSet<Vec<i64>> = BTreeSet::new();
        let mut all_neighbors: BTreeMap<Vec<i64>, i64> = BTreeMap::new();

        for cube in cubes.iter() {
            let mut neighbors: Vec<Vec<i64>> = cartesian_product(
                &(0..cube.len())
                    .map(|i| vec![cube[i] - 1, cube[i], cube[i] + 1])
                    .collect(),
            );
            neighbors.remove(
                neighbors
                    .iter()
                    .position(|x| x == cube)
                    .expect("cube not found"),
            );

            let count = neighbors
                .iter()
                .filter(|neighbor| cubes.contains(neighbor.clone()))
                .count();
            if count == 2 || count == 3 {
                new_cubes.insert(cube.clone());
            }

            for neighbor in neighbors {
                if !cubes.contains(&neighbor) {
                    all_neighbors.insert(
                        neighbor.clone(),
                        all_neighbors.get(&neighbor).unwrap_or(&0) + 1,
                    );
                }
            }
        }

        for (neighbor, num_active) in all_neighbors {
            if num_active == 3 {
                new_cubes.insert(neighbor);
            }
        }
        cubes = new_cubes;
    }
    cubes.len()
}

// ----------------------------------------------------------------------------------
// From https://gist.github.com/kylewlacy/115965b40e02a3325558 (slightly modified)

// Given a vector containing a partial Cartesian product, and a list of items,
/// return a vector adding the list of items to the partial Cartesian product.
///
/// # Example
///
/// ```
/// let partial_product = vec![vec![1, 4], vec![1, 5], vec![2, 4], vec![2, 5]];
/// let items = &[6, 7];
/// let next_product = partial_cartesian(partial_product, items);
/// assert_eq!(next_product, vec![vec![1, 4, 6],
///                               vec![1, 4, 7],
///                               vec![1, 5, 6],
///                               vec![1, 5, 7],
///                               vec![2, 4, 6],
///                               vec![2, 4, 7],
///                               vec![2, 5, 6],
///                               vec![2, 5, 7]]);
/// ```
pub fn partial_cartesian<T: Clone>(a: Vec<Vec<T>>, b: &Vec<T>) -> Vec<Vec<T>> {
    a.into_iter()
        .flat_map(|xs| {
            b.iter()
                .cloned()
                .map(|y| {
                    let mut vec = xs.clone();
                    vec.push(y);
                    vec
                })
                .collect::<Vec<_>>()
        })
        .collect()
}

/// Computes the Cartesian product of lists[0] * lists[1] * ... * lists[n].
///
/// # Example
///
/// ```
/// let lists: &[&[_]] = &[&[1, 2], &[4, 5], &[6, 7]];
/// let product = cartesian_product(lists);
/// assert_eq!(product, vec![vec![1, 4, 6],
///                          vec![1, 4, 7],
///                          vec![1, 5, 6],
///                          vec![1, 5, 7],
///                          vec![2, 4, 6],
///                          vec![2, 4, 7],
///                          vec![2, 5, 6],
///                          vec![2, 5, 7]]);
/// ```
pub fn cartesian_product<T: Clone>(lists: &Vec<Vec<T>>) -> Vec<Vec<T>> {
    match lists.split_first() {
        Some((first, rest)) => {
            let init: Vec<Vec<T>> = first.iter().cloned().map(|n| vec![n]).collect();

            rest.iter()
                .cloned()
                .fold(init, |vec, list| partial_cartesian(vec, &list))
        }
        None => {
            vec![]
        }
    }
}
