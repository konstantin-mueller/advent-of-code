pub fn shuttle_search(data: &Vec<String>) -> i64 {
    let estimate = data[0].parse::<i64>().unwrap();
    let mut minimum = (i64::MAX, i64::MAX);

    for bus_id in data[1].split(",") {
        if bus_id == "x" {
            continue;
        }

        let bus = bus_id.parse::<i64>().unwrap();
        let result = bus - (estimate % bus);

        if result < minimum.0 {
            minimum = (result, bus);
        }
    }

    minimum.0 * minimum.1
}

pub fn part_two(data: &Vec<String>) -> i128 {
    let mut bus_product = 1;
    let busses = data[1]
        .split(",")
        .collect::<Vec<&str>>()
        .into_iter()
        .enumerate()
        .filter(|(_, bus)| *bus != "x")
        .map(|(i, bus)| {
            let b = bus.parse::<i128>().unwrap();
            bus_product *= b;
            (b - i as i128, b)
        })
        .collect();

    chinese_remainder_problem(&busses, bus_product) % bus_product
}

fn chinese_remainder_problem(busses: &Vec<(i128, i128)>, bus_product: i128) -> i128 {
    busses
        .into_iter()
        .map(|(i, bus)| i * modular_inverse(bus_product / bus, bus - 2, *bus) * bus_product / bus)
        .sum()
}

fn modular_inverse(base: i128, exp: i128, modu: i128) -> i128 {
    if exp == 0 {
        return 1;
    } else if exp % 2 == 0 {
        return modular_inverse((base * base) % modu, exp / 2, modu);
    }
    return (base * modular_inverse(base, exp - 1, modu)) % modu;
}
