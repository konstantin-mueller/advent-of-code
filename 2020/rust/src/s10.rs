use std::collections::HashMap;

pub fn adapter_array(adapt: &Vec<u64>) -> u64 {
    let mut adapters = adapt.clone();
    adapters.sort();
    adapters.insert(0, 0);

    let one_jolt_differences = count_jolt_differences(&adapters, 1);
    let three_jolt_differences = count_jolt_differences(&adapters, 3);
    one_jolt_differences * (three_jolt_differences + 1)
}

fn count_jolt_differences(adapters: &Vec<u64>, difference_to_count: u64) -> u64 {
    adapters
        .into_iter()
        .enumerate()
        .filter(|(i, value)| {
            i + 1 < adapters.len() && adapters[i + 1] - *value == difference_to_count
        })
        .count() as u64
}

pub fn part_two(adapt: &Vec<u64>) -> u64 {
    let mut adapters = adapt.clone();
    adapters.sort();

    let mut paths: HashMap<u64, u64> = HashMap::new();
    paths.insert(0, 1);

    for adapter in adapters.iter() {
        let num_paths = (1..4)
            .filter(|value| adapter >= value)
            .filter_map(|value| paths.get(&(adapter - value)))
            .sum();

        paths.insert(*adapter, num_paths);
    }
    paths[&adapters.last().unwrap()]
}
