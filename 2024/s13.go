package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Point struct {
	x uint64
	y uint64
}

func main() {
	lines := read("resources/my")
	result := PartOne(lines)
	fmt.Println("Part one:", result)
}

// only part one brute force. see rust code for better solution with part two.
func PartOne(lines []string) uint64 {
	var aButtons []Point
	var bButtons []Point
	var prizes []Point
	for _, line := range lines {
		if strings.Contains(line, "Button A") {
			split := strings.Split(line, ", Y+")
			x, err := strconv.ParseUint(strings.Split(split[0], "X+")[1], 10, 64)
			panic_if_err(err)

			y, err := strconv.ParseUint(split[1], 10, 64)
			panic_if_err(err)

			point := Point{
				x: x, y: y,
			}
			aButtons = append(aButtons, point)
		} else if strings.Contains(line, "Button B") {
			split := strings.Split(line, ", Y+")
			x, err := strconv.ParseUint(strings.Split(split[0], "X+")[1], 10, 64)
			panic_if_err(err)

			y, err := strconv.ParseUint(split[1], 10, 64)
			panic_if_err(err)

			point := Point{
				x: x, y: y,
			}
			bButtons = append(bButtons, point)
		} else if strings.Contains(line, "Prize: ") {
			split := strings.Split(line, ", Y=")
			x, err := strconv.ParseUint(strings.Split(split[0], "X=")[1], 10, 64)
			panic_if_err(err)

			y, err := strconv.ParseUint(split[1], 10, 64)
			panic_if_err(err)

			point := Point{
				x: x, y: y,
			}
			prizes = append(prizes, point)
		}
	}

	var result uint64 = 0
	for i, prize := range prizes {
		a := aButtons[i]
		b := bButtons[i]

		var possible_results []uint64
		for times_a_pressed := range uint64(100) {
			for times_b_pressed := range uint64(100) {
				if times_a_pressed*a.x+times_b_pressed*b.x == prize.x && times_a_pressed*a.y+times_b_pressed*b.y == prize.y {
					possible_results = append(possible_results, times_a_pressed*3 + times_b_pressed)
				}
			}
		}

		if len(possible_results) == 0 {
			fmt.Println("no result found for", a, b, prize)
			continue
		}

		min := possible_results[0]
		for _, p_result := range possible_results {
			if p_result < min {
				min = p_result
			}
		}

		result += min
	}

	return result
}

func read(filename string) []string {
	contents, err := os.ReadFile(filename)
	panic_if_err(err)

	return strings.Split(string(contents), "\n")
}

func panic_if_err(err error) {
	if err != nil {
		panic(err)
	}
}
