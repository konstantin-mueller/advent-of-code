use std::collections::HashMap;

pub fn day_11(input: &String, blinks: usize) -> usize {
    let mut stones: HashMap<String, usize> = input
        .split_whitespace()
        .map(|s| (s.to_string(), 1))
        .collect();

    let zero = "0";
    let one = "1".to_string();
    for _ in 0..blinks {
        let mut new_stones = HashMap::new();

        for (stone, count) in stones {
            if stone == zero {
                new_stones
                    .entry(one.clone())
                    .and_modify(|c| *c += count)
                    .or_insert(count);
            } else if stone.len() % 2 == 0 {
                let (first, second) = stone.split_at(stone.len() / 2);
                new_stones
                    .entry(first.to_string())
                    .and_modify(|c| *c += count)
                    .or_insert(count);

                let second_str = second.parse::<usize>().unwrap().to_string();
                new_stones
                    .entry(second_str)
                    .and_modify(|c| *c += count)
                    .or_insert(count);
            } else {
                new_stones
                    .entry((stone.parse::<usize>().unwrap() * 2024).to_string())
                    .and_modify(|c| *c += count)
                    .or_insert(count);
            }
        }
        stones = new_stones;
    }

    stones.values().sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        assert_eq!(day_11(&"125 17".to_string(), 25), 55312);
    }
}
