use std::collections::HashSet;

use crate::utils::Point;

pub fn part_one(input: &Vec<String>) -> usize {
    let mut boxes = HashSet::new();
    let mut walls = HashSet::new();
    let mut robot_pos = Point::new(0, 0);
    let mut robot_instructions_start = 0;
    for (y, line) in input.iter().enumerate() {
        if line.is_empty() {
            robot_instructions_start = y + 1;
            break;
        }

        for (x, c) in line.chars().enumerate() {
            match c {
                '@' => robot_pos = Point::new(x, y),
                'O' => {
                    let pos = Point::new(x, y);
                    boxes.insert(pos);
                }
                '#' => {
                    walls.insert(Point::new(x, y));
                }
                _ => {}
            }
        }
    }

    if robot_instructions_start == 0 || robot_pos.x == 0 && robot_pos.y == 0 {
        panic!("Parsing failed. Could not find robot instructions or robot position.")
    }

    for line in input.iter().skip(robot_instructions_start - 1) {
        for c in line.chars() {
            let mut x = 0;
            let mut y = 0;
            match c {
                '^' => y -= 1,
                '>' => x += 1,
                'v' => y += 1,
                '<' => x -= 1,
                _ => continue,
            }
            move_robot(&mut robot_pos, x, y, &mut boxes, &walls);
        }
    }

    boxes.iter().map(|pos| pos.y * 100 + pos.x).sum()
}

fn move_robot(
    pos: &mut Point<usize>,
    x: i32,
    y: i32,
    boxes: &mut HashSet<Point<usize>>,
    walls: &HashSet<Point<usize>>,
) {
    let new_x = pos.x as i32 + x;
    let new_y = pos.y as i32 + y;
    if new_x < 0 || new_y < 0 {
        panic!("new robot position is below 0")
    }
    let new_pos = Point::new(new_x as usize, new_y as usize);

    if walls.contains(&new_pos) {
        return;
    }

    let mut new_box_pos = new_pos.clone();
    let mut new_boxes = HashSet::new();
    while let Some(box_) = boxes.get(&new_box_pos) {
        let new_x = box_.x as i32 + x;
        let new_y = box_.y as i32 + y;
        if new_x < 0 || new_y < 0 || walls.contains(&Point::new(new_x as usize, new_y as usize)) {
            return;
        }

        new_boxes.insert(box_.clone());
        new_box_pos.x = new_x as usize;
        new_box_pos.y = new_y as usize;
    }

    for npos in &new_boxes {
        boxes.remove(&npos);
    }
    for npos in new_boxes {
        let new = Point::new(npos.x + x as usize, npos.y + y as usize);
        boxes.insert(new);
    }

    pos.x = new_pos.x;
    pos.y = new_pos.y;
}

pub fn part_two(input: &Vec<String>) -> usize {
    let mut boxes = HashSet::new();
    let mut walls = HashSet::new();
    let mut robot_pos = Point::new(0, 0);
    let mut robot_instructions_start = 0;
    for (y, line) in input.iter().enumerate() {
        if line.is_empty() {
            robot_instructions_start = y + 1;
            break;
        }

        for (x, c) in line.chars().enumerate() {
            match c {
                '@' => robot_pos = Point::new(x as i32 * 2, y as i32),
                'O' => {
                    let pos = Point::new(x as i32 * 2, y as i32);
                    boxes.insert(pos);
                }
                '#' => {
                    walls.insert(Point::new(x as i32 * 2, y as i32));
                    walls.insert(Point::new(x as i32 * 2 + 1, y as i32));
                }
                _ => {}
            }
        }
    }

    if robot_instructions_start == 0 || robot_pos.x == 0 && robot_pos.y == 0 {
        panic!("Parsing failed. Could not find robot instructions or robot position.")
    }

    for line in input.iter().skip(robot_instructions_start - 1) {
        for c in line.chars() {
            let mut x = 0;
            let mut y = 0;
            match c {
                '^' => y -= 1,
                '>' => x += 1,
                'v' => y += 1,
                '<' => x -= 1,
                _ => continue,
            }
            move_robot_part_two(&mut robot_pos, x, y, &mut boxes, &walls);
        }
    }

    boxes.iter().map(|pos| pos.y * 100 + pos.x).sum::<i32>() as usize
}

fn move_robot_part_two(
    pos: &mut Point<i32>,
    x: i32,
    y: i32,
    boxes: &mut HashSet<Point<i32>>,
    walls: &HashSet<Point<i32>>,
) {
    let mut new_boxes = HashSet::new();
    if x == 0 {
        let mut new_pos = HashSet::from([Point::new(pos.x, pos.y + y)]);

        loop {
            if walls.intersection(&new_pos).next().is_some() {
                return;
            }

            let mut visited = HashSet::with_capacity(new_pos.len());
            for npos in new_pos {
                if boxes.contains(&npos) {
                    visited.insert(npos.clone());
                }

                if let Some(box_) = boxes.get(&Point::new(npos.x - 1, npos.y)) {
                    visited.insert(box_.clone());
                }
            }

            if visited.is_empty() {
                pos.y += y;
                break;
            }

            let mut new_positions = HashSet::with_capacity(visited.len() * 2);
            for vpos in visited {
                new_positions.insert(Point::new(vpos.x, vpos.y + y));
                new_positions.insert(Point::new(vpos.x + 1, vpos.y + y));
                new_boxes.insert(vpos);
            }
            new_pos = new_positions;
        }
    } else {
        let mut new_pos = Point::new(pos.x + x, pos.y);

        loop {
            if walls.contains(&new_pos) {
                return;
            }

            if boxes.contains(&new_pos) {
                new_boxes.insert(new_pos.clone());
                new_pos = Point::new(new_pos.x + 2 * x, new_pos.y);
            } else if boxes.contains(&Point::new(new_pos.x - 1, new_pos.y)) {
                new_boxes.insert(Point::new(new_pos.x - 1, new_pos.y));
                new_pos = Point::new(new_pos.x + 2 * x, new_pos.y);
            } else {
                pos.x += x;
                break;
            }
        }
    }

    for npos in &new_boxes {
        boxes.remove(&npos);
    }
    for npos in new_boxes {
        boxes.insert(Point::new(npos.x + x, npos.y + y));
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = r#"########
#..O.O.#
##@.O..#
#...O..#
#.#.O..#
#...O..#
#......#
########

<^^>>>vv<v>>v<<"#;

        assert_eq!(part_one(&to_vec(input)), 2028);
    }

    #[test]
    fn test_part_one_2() {
        let input = r#"##########
#..O..O.O#
#......O.#
#.OO..O.O#
#..O@..O.#
#O#..O...#
#O..O..O.#
#.OO.O.OO#
#....O...#
##########

<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^
vvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v
><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<
<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^
^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><
^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^
>^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^
<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>
^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>
v^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^"#;

        assert_eq!(part_one(&to_vec(input)), 10092);
    }

    #[test]
    fn test_part_two() {
        let input = r#"#######
#...#.#
#.....#
#..OO@#
#..O..#
#.....#
#######

<vv<<^^<<^^"#;

        assert_eq!(part_two(&to_vec(input)), 618);
    }

    #[test]
    fn test_part_two_2() {
        let input = r#"##########
#..O..O.O#
#......O.#
#.OO..O.O#
#..O@..O.#
#O#..O...#
#O..O..O.#
#.OO.O.OO#
#....O...#
##########

<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^
vvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v
><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<
<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^
^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><
^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^
>^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^
<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>
^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>
v^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^"#;

        assert_eq!(part_two(&to_vec(input)), 9021);
    }

    #[test]
    fn test_part_two_3() {
        let input = r#"#######
#...#.#
#.....#
#.@OO.#
#...O.#
#.....#
#######

>>^>vvv>>>"#;

        assert_eq!(part_two(&to_vec(input)), 1225);
    }

    #[test]
    fn test_part_two_4() {
        let input = r#"#######
#...#.#
#.....#
#.@OO.#
#..OO.#
#.OOOO#
#.....#
#######

>>^>vvv"#;

        assert_eq!(part_two(&to_vec(input)), 3958);
    }

    fn to_vec(input: &str) -> Vec<String> {
        input.lines().map(|line| line.to_string()).collect()
    }
}
