use std::iter;

pub fn solve(input: &Vec<String>, part_two: bool) -> usize {
    let mut results = Vec::with_capacity(1_000_000_000);

    input
        .iter()
        .filter_map(|line| {
            let (result, numbers) = line.split_once(": ").unwrap();
            let expected_result: usize = result.parse().unwrap();
            let mut numbers = numbers
                .split_whitespace()
                .map(|number| number.parse::<usize>().unwrap());

            results.clear();
            results.push(numbers.next().unwrap());

            for number in numbers {
                results = if part_two {
                    get_results_part_two(&results, number, expected_result)
                } else {
                    get_results_part_one(&results, number, expected_result)
                }
            }

            if results.contains(&expected_result) {
                Some(expected_result)
            } else {
                None
            }
        })
        .sum()
}

fn get_results_part_one(results: &Vec<usize>, number: usize, expected: usize) -> Vec<usize> {
    results
        .iter()
        .flat_map(|&current| {
            iter::once(current + number)
                .chain(iter::once(current * number))
                .filter(|next| next <= &expected)
        })
        .collect()
}

fn get_results_part_two(results: &Vec<usize>, number: usize, expected: usize) -> Vec<usize> {
    results
        .iter()
        .flat_map(|&current| {
            iter::once(current + number)
                .chain(iter::once(current * number))
                .chain(iter::once(concatenate(current, number)))
                .filter(|next| next <= &expected)
        })
        .collect()
}

fn concatenate(current: usize, number: usize) -> usize {
    let mut temp = number;
    let mut multiplier = 1;

    while temp > 0 {
        temp /= 10;
        multiplier *= 10;
    }

    current * multiplier + number
}
