pub fn part_one(input: &String) -> usize {
    let mut locks = Vec::new();
    let mut keys = Vec::new();

    for part in input.split("\n\n") {
        let lines: Vec<&str> = part.lines().collect();

        let is_lock = lines[0] == "#####";

        let mut current = vec![0; lines[0].len()];
        for (j, line) in lines.iter().enumerate() {
            if is_lock && j == 0 {
                continue;
            } else if !is_lock && j == lines.len() - 1 {
                continue;
            }

            for (i, c) in line.chars().enumerate() {
                if c == '#' {
                    current[i] += 1;
                }
            }
        }

        if is_lock {
            locks.push(current);
        } else {
            keys.push(current);
        }
    }

    let mut result = 0;
    for lock in locks {
        result += keys
            .iter()
            .filter(|&key| key.iter().enumerate().all(|(i, &k)| k + lock[i] < 6))
            .count();
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = r"#####
.####
.####
.####
.#.#.
.#...
.....

#####
##.##
.#.##
...##
...#.
...#.
.....

.....
#....
#....
#...#
#.#.#
#.###
#####

.....
.....
#.#..
###..
###.#
###.#
#####

.....
.....
.....
#....
#.#..
#.#.#
#####";

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(&INPUT.to_string()), 3);
    }
}
