use crate::utils::{AppResult, Point};

#[derive(Debug)]
struct Robot {
    pos: Point<i32>,
    vel: Point<i32>,
}

impl Robot {
    fn walk(&mut self, map_size: &Point<i32>) {
        self.pos.add(&self.vel);

        if self.pos.x < 0 {
            self.pos.x += map_size.x + 1;
        } else if self.pos.x > map_size.x {
            self.pos.x -= map_size.x + 1;
        }

        if self.pos.y < 0 {
            self.pos.y += map_size.y + 1;
        } else if self.pos.y > map_size.y {
            self.pos.y -= map_size.y + 1;
        }
    }
}

// map_size is zero-based
pub fn part_one(input: &Vec<String>, map_size: Point<i32>) -> usize {
    let mut robots: Vec<Robot> = input
        .iter()
        .map(|line| {
            let mut parts = line.split_whitespace();
            let pos = parse_numbers(&mut parts, "p=").unwrap();
            let vel = parse_numbers(&mut parts, "v=").unwrap();

            Robot { pos, vel }
        })
        .collect();

    for _ in 0..100 {
        for robot in robots.iter_mut() {
            robot.walk(&map_size);
        }
    }

    let middle = Point::new(map_size.x / 2, map_size.y / 2);
    let mut first_quadrant = 0;
    let mut second_quadrant = 0;
    let mut third_quadrant = 0;
    let mut fourth_quadrant = 0;
    for robot in robots {
        if robot.pos.x == middle.x || robot.pos.y == middle.y {
            continue;
        }

        if robot.pos.x < middle.x && robot.pos.y < middle.y {
            first_quadrant += 1;
        } else if robot.pos.x > middle.x && robot.pos.y < middle.y {
            second_quadrant += 1;
        } else if robot.pos.x > middle.x && robot.pos.y > middle.y {
            third_quadrant += 1;
        } else if robot.pos.x < middle.x && robot.pos.y > middle.y {
            fourth_quadrant += 1;
        }
    }

    first_quadrant * second_quadrant * third_quadrant * fourth_quadrant
}

pub fn part_two(input: &Vec<String>, map_size: Point<i32>) -> usize {
    let mut robots: Vec<Robot> = input
        .iter()
        .map(|line| {
            let mut parts = line.split_whitespace();
            let pos = parse_numbers(&mut parts, "p=").unwrap();
            let vel = parse_numbers(&mut parts, "v=").unwrap();

            Robot { pos, vel }
        })
        .collect();

    for i in 0..100000 {
        for robot in robots.iter_mut() {
            robot.walk(&map_size);
        }

        // check if there is a x with 10 robots in a column
        for x in 0..map_size.x {
            if ((map_size.y / 2 - 5)..(map_size.y / 2 + 5)).all(|y| {
                robots
                    .iter()
                    .find(|r| r.pos.y == y && r.pos.x == x)
                    .is_some()
            }) {
                // print map that contains the tree
                let mut map = vec![vec![' '; (map_size.x + 1) as usize]; (map_size.y + 1) as usize];
                for robot in robots {
                    map[robot.pos.y as usize][robot.pos.x as usize] = 'X';
                }

                for line in map {
                    println!("{}", line.iter().collect::<String>());
                }

                return i + 1;
            }
        }
    }

    panic!("No solution found")
}

fn parse_numbers(parts: &mut std::str::SplitWhitespace, trim: &str) -> AppResult<Point<i32>> {
    let mut numbers = parts.next().ok_or("no numbers")?.split(",");
    let x = numbers
        .next()
        .ok_or("no x")?
        .trim_start_matches(trim)
        .parse::<i32>()?;
    let y = numbers.next().ok_or("no y")?.parse::<i32>()?;
    Ok(Point::new(x, y))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        assert_eq!(
            part_one(
                &vec![
                    "p=0,4 v=3,-3".to_string(),
                    "p=6,3 v=-1,-3".to_string(),
                    "p=10,3 v=-1,2".to_string(),
                    "p=2,0 v=2,-1".to_string(),
                    "p=0,0 v=1,3".to_string(),
                    "p=3,0 v=-2,-2".to_string(),
                    "p=7,6 v=-1,-3".to_string(),
                    "p=3,0 v=-1,-2".to_string(),
                    "p=9,3 v=2,3".to_string(),
                    "p=7,3 v=-1,2".to_string(),
                    "p=2,4 v=2,-3".to_string(),
                    "p=9,5 v=-3,-3".to_string(),
                ],
                Point::new(10, 6)
            ),
            12
        );
    }
}
