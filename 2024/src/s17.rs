use std::usize;

use crate::utils::AppResult;

pub fn part_one(input: &Vec<String>) -> String {
    let register_a = parse_number(&input[0]).unwrap();
    let register_b = parse_number(&input[1]).unwrap();
    let register_c = parse_number(&input[2]).unwrap();
    let program = input[4]
        .split_whitespace()
        .last()
        .unwrap()
        .split(",")
        .map(|n| n.parse().unwrap())
        .collect::<Vec<usize>>();

    solve(&program, register_a, register_b, register_c)
        .iter()
        .map(|&n| format!("{n},"))
        .collect::<String>()
        .trim_end_matches(",")
        .to_string()
}

fn parse_number(input: &str) -> AppResult<usize> {
    let number = input
        .split_whitespace()
        .last()
        .ok_or("Invalid input")?
        .parse::<usize>()?;

    Ok(number)
}

fn solve(program: &Vec<usize>, reg_a: usize, reg_b: usize, reg_c: usize) -> Vec<usize> {
    let mut register_a = reg_a;
    let mut register_b = reg_b;
    let mut register_c = reg_c;

    let mut result = vec![];
    let mut i = 0;
    while i < program.len() {
        match program[i] {
            0 => {
                let operand = combo(program[i + 1], register_a, register_b, register_c);
                register_a /= 2_usize.pow(operand as u32)
            }
            1 => register_b ^= program[i + 1],
            2 => {
                let operand = combo(program[i + 1], register_a, register_b, register_c);
                register_b = operand % 8
            }
            3 => {
                if register_a != 0 {
                    i = program[i + 1];
                    continue;
                }
            }
            4 => register_b ^= register_c,
            5 => {
                let operand = combo(program[i + 1], register_a, register_b, register_c);
                result.push(operand % 8);
            }
            6 => {
                let operand = combo(program[i + 1], register_a, register_b, register_c);
                register_b = register_a / 2_usize.pow(operand as u32)
            }
            7 => {
                let operand = combo(program[i + 1], register_a, register_b, register_c);
                register_c = register_a / 2_usize.pow(operand as u32)
            }
            _ => unreachable!(),
        }

        i += 2;
    }

    result
}

fn combo(operand: usize, register_a: usize, register_b: usize, register_c: usize) -> usize {
    match operand {
        0..=3 => operand,
        4 => register_a,
        5 => register_b,
        6 => register_c,
        _ => unreachable!(),
    }
}

pub fn part_two(input: &Vec<String>) -> usize {
    let register_b = parse_number(&input[1]).unwrap();
    let register_c = parse_number(&input[2]).unwrap();
    let program = input[4]
        .split_whitespace()
        .last()
        .unwrap()
        .split(",")
        .map(|n| n.parse().unwrap())
        .collect::<Vec<usize>>();

    // the program always ends with 3,0. register_a is divided by 8 every time (0,3), then the program is executed
    // from the start again (3,0). the outputs (result) are always based on register_a (which is divided by 8).
    // so register_a is always a multiple of 8. we are trying to find a result that is equal to the program.
    // that means that register_a has to be between 8^program.len()-1 and 8^program.len().
    let mut register_a = 8_usize.pow(program.len() as u32 - 1);

    loop {
        let result = solve(&program, register_a, register_b, register_c);

        if result == program {
            break;
        }

        // try the next muliple of 8 for register_a based on the correct results (backwards)
        for i in (0..result.len()).rev() {
            if result[i] != program[i] {
                register_a += 8_usize.pow(i as u32);
                break;
            }
        }
    }

    register_a
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = r"Register A: 729
Register B: 0
Register C: 0

Program: 0,1,5,4,3,0";

        assert_eq!(part_one(&to_vec(input)), "4,6,3,5,6,3,5,2,1,0".to_string());
    }

    #[test]
    fn test_part_two() {
        let input = r"Register A: 2024
Register B: 0
Register C: 0

Program: 0,3,5,4,3,0";

        assert_eq!(part_two(&to_vec(input)), 117440);
    }

    fn to_vec(input: &str) -> Vec<String> {
        input.lines().map(|line| line.to_string()).collect()
    }
}
