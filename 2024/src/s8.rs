use std::collections::{HashMap, HashSet};

pub fn part_one(input: &Vec<Vec<char>>) -> usize {
    let antennas = parse_antennas(input);
    let mut antinodes = HashSet::new();

    for antenna_positions in antennas.values() {
        for (i, &(x1, y1)) in antenna_positions.iter().enumerate() {
            for &(x2, y2) in antenna_positions[i + 1..].iter() {
                let dx = x1.abs_diff(x2);
                let dy = y1.abs_diff(y2);

                // top left to bottom right
                if x1 < x2 && y1 < y2 {
                    if x1.checked_sub(dx).is_some() && y1.checked_sub(dy).is_some() {
                        antinodes.insert((x1 - dx, y1 - dy));
                    }
                    if x2 + dx < input[0].len() && y2 + dy < input.len() {
                        antinodes.insert((x2 + dx, y2 + dy));
                    }
                // top right to bottom left
                } else if x1 >= x2 && y1 < y2 {
                    if x1 + dx < input[0].len() && y1.checked_sub(dy).is_some() {
                        antinodes.insert((x1 + dx, y1 - dy));
                    }
                    if x2.checked_sub(dx).is_some() && y2 + dy < input.len() {
                        antinodes.insert((x2 - dx, y2 + dy));
                    }
                // bottom left to top right
                } else if x1 < x2 && y1 >= y2 {
                    if x1.checked_sub(dx).is_some() && y1 + dy < input.len() {
                        antinodes.insert((x1 - dx, y1 + dy));
                    }
                    if x2 + dx < input[0].len() && y2.checked_sub(dy).is_some() {
                        antinodes.insert((x2 + dx, y2 - dy));
                    }
                // bottom right to top left
                } else if x1 >= x2 && y1 >= y2 {
                    if x1 + dx < input[0].len() && y1 + dy < input.len() {
                        antinodes.insert((x1 + dx, y1 + dy));
                    }
                    if x2.checked_sub(dx).is_some() && y2.checked_sub(dy).is_some() {
                        antinodes.insert((x2 - dx, y2 - dy));
                    }
                }
            }
        }
    }

    antinodes.len()
}

fn parse_antennas(input: &Vec<Vec<char>>) -> HashMap<char, Vec<(usize, usize)>> {
    let mut antennas = HashMap::new();
    for y in 0..input.len() {
        for x in 0..input[y].len() {
            let c = input[y][x];
            if c != '.' {
                antennas
                    .entry(c)
                    .and_modify(|v: &mut Vec<(usize, usize)>| v.push((x, y)))
                    .or_insert(vec![(x, y)]);
            }
        }
    }

    antennas
}

pub fn part_two(input: &Vec<Vec<char>>) -> usize {
    let antennas = parse_antennas(input);
    let mut antinodes = HashSet::new();

    for antenna_positions in antennas.values() {
        for (i, &(x1, y1)) in antenna_positions.iter().enumerate() {
            antinodes.insert((x1, y1));

            for &(x2, y2) in antenna_positions[i + 1..].iter() {
                let dx = x1.abs_diff(x2);
                let dy = y1.abs_diff(y2);

                // top left to bottom right
                if x1 < x2 && y1 < y2 {
                    let mut x = x1;
                    let mut y = y1;
                    while x.checked_sub(dx).is_some() && y.checked_sub(dy).is_some() {
                        x -= dx;
                        y -= dy;
                        antinodes.insert((x, y));
                    }

                    x = x2;
                    y = y2;
                    while x + dx < input[0].len() && y + dy < input.len() {
                        x += dx;
                        y += dy;
                        antinodes.insert((x, y));
                    }
                // top right to bottom left
                } else if x1 >= x2 && y1 < y2 {
                    let mut x = x1;
                    let mut y = y1;
                    while x + dx < input[0].len() && y.checked_sub(dy).is_some() {
                        x += dx;
                        y -= dy;
                        antinodes.insert((x, y));
                    }

                    x = x2;
                    y = y2;
                    while x.checked_sub(dx).is_some() && y + dy < input.len() {
                        x -= dx;
                        y += dy;
                        antinodes.insert((x, y));
                    }
                // bottom left to top right
                } else if x1 < x2 && y1 >= y2 {
                    let mut x = x1;
                    let mut y = y1;
                    while x.checked_sub(dx).is_some() && y + dy < input.len() {
                        x -= dx;
                        y += dy;
                        antinodes.insert((x, y));
                    }

                    x = x2;
                    y = y2;
                    while x + dx < input[0].len() && y.checked_sub(dy).is_some() {
                        x += dx;
                        y -= dy;
                        antinodes.insert((x, y));
                    }
                // bottom right to top left
                } else if x1 >= x2 && y1 >= y2 {
                    let mut x = x1;
                    let mut y = y1;
                    while x + dx < input[0].len() && y + dy < input.len() {
                        x += dx;
                        y += dy;
                        antinodes.insert((x, y));
                    }

                    x = x2;
                    y = y2;
                    while x.checked_sub(dx).is_some() && y.checked_sub(dy).is_some() {
                        x -= dx;
                        y -= dy;
                        antinodes.insert((x, y));
                    }
                }
            }
        }
    }

    antinodes.len()
}
