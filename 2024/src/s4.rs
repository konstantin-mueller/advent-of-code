const DIRECTIONS: [(i32, i32); 8] = [
    (0, 1),
    (0, -1),
    (1, 0),
    (-1, 0),
    (1, 1),
    (-1, -1),
    (1, -1),
    (-1, 1),
];

pub fn part_one(input: &Vec<Vec<char>>) -> usize {
    input
        .iter()
        .enumerate()
        .map(|(i, line)| {
            line.iter()
                .enumerate()
                .filter(|&(_, &letter)| letter == 'X')
                .map(|(j, _)| {
                    let i = i as i32;
                    let j = j as i32;

                    DIRECTIONS
                        .iter()
                        .filter(|&(di, dj)| {
                            is_char(input, i + di, j + dj, 'M')
                                && is_char(input, i + 2 * di, j + 2 * dj, 'A')
                                && is_char(input, i + 3 * di, j + 3 * dj, 'S')
                        })
                        .count()
                })
                .sum::<usize>()
        })
        .sum()
}

fn is_char(input: &Vec<Vec<char>>, i: i32, j: i32, c: char) -> bool {
    if i < 0 || j < 0 {
        return false;
    }

    let i = i as usize;
    let j = j as usize;
    i < input.len() && j < input[i].len() && input[i][j] == c
}

pub fn part_two(input: &Vec<Vec<char>>) -> usize {
    input
        .iter()
        .enumerate()
        .map(|(i, line)| {
            line.iter()
                .enumerate()
                .filter_map(|(j, &letter)| {
                    if letter == 'A' {
                        if i < 1 || j < 1 || i + 1 >= input.len() || j + 1 >= input[i + 1].len() {
                            return None;
                        }

                        let top_left = input[i - 1][j - 1];
                        let top_right = input[i - 1][j + 1];
                        let bottom_left = input[i + 1][j - 1];
                        let bottom_right = input[i + 1][j + 1];

                        if ((top_left == 'M' && bottom_right == 'S')
                            || (top_left == 'S' && bottom_right == 'M'))
                            && ((top_right == 'M' && bottom_left == 'S')
                                || (top_right == 'S' && bottom_left == 'M'))
                        {
                            return Some(());
                        }
                    }

                    None
                })
                .count()
        })
        .sum()
}
