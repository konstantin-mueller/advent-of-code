use std::collections::HashMap;

use crate::utils::Point;

const DIRECTIONS: [(i32, i32); 4] = [(0, 1), (1, 0), (0, -1), (-1, 0)];

pub fn part_one(input: &Vec<Vec<char>>) -> i32 {
    let start = input
        .iter()
        .enumerate()
        .filter_map(|(y, line)| {
            if let Some(x) = line.iter().position(|c| *c == 'S') {
                Some(Point::new(x, y))
            } else {
                None
            }
        })
        .next()
        .unwrap();

    // get path without cheats and with costs
    let end;
    let mut path: HashMap<Point<i32>, i32> = HashMap::new();
    path.insert(Point::new(start.x as i32, start.y as i32), 0);
    let mut pos = start;
    let mut prev_pos = start;
    let mut count = 1;
    'outer: loop {
        for (dx, dy) in DIRECTIONS {
            let x = pos.x as i32 + dx;
            let y = pos.y as i32 + dy;
            if x < 0 || y < 0 || x >= input[0].len() as i32 || y >= input.len() as i32 {
                continue;
            }

            let x = x as usize;
            let y = y as usize;
            if input[y][x] == '#' {
                continue;
            }

            let c = input[y][x];
            if c == 'E' {
                end = Point::new(x as i32, y as i32);
                path.insert(end, count);
                break 'outer;
            }
            if c == '.' && prev_pos != Point::new(x, y) {
                prev_pos = pos;
                pos = Point::new(x, y);
                path.entry(Point::new(x as i32, y as i32)).or_insert(count);
                break;
            }
        }
        count += 1;
    }

    let mut result = 0;
    let mut current_value = 0;
    let mut pos = Point::new(start.x as i32, start.y as i32);
    while pos != end {
        let mut new_pos = pos;

        for (dx, dy) in DIRECTIONS {
            let x = pos.x + dx;
            let y = pos.y + dy;
            if let Some(value) = path.get(&Point::new(x, y)) {
                if *value > current_value {
                    new_pos = Point::new(x, y);
                    current_value = *value;
                    continue;
                }
            } else {
                if let Some(value) = path.get(&Point::new(x + dx, y + dy)) {
                    if *value - path[&pos] - 2 >= 100 {
                        result += 1;
                    }
                } else if let Some(value) = path.get(&Point::new(x + dx * 2, y + dy * 2)) {
                    if *value - path[&pos] - 3 >= 100 {
                        result += 1;
                    }
                }
            }
        }
        pos = new_pos;
    }

    result
}

pub fn part_two(input: &Vec<Vec<char>>) -> i32 {
    let start = input
        .iter()
        .enumerate()
        .filter_map(|(y, line)| {
            if let Some(x) = line.iter().position(|c| *c == 'S') {
                Some(Point::new(x, y))
            } else {
                None
            }
        })
        .next()
        .unwrap();

    // get path without cheats
    let end;
    let mut path = Vec::new();
    path.push(Point::new(start.x as i32, start.y as i32));
    let mut pos = start;
    let mut prev_pos = start;
    'outer: loop {
        for (dx, dy) in DIRECTIONS {
            let x = pos.x as i32 + dx;
            let y = pos.y as i32 + dy;
            if x < 0 || y < 0 || x >= input[0].len() as i32 || y >= input.len() as i32 {
                continue;
            }

            let x = x as usize;
            let y = y as usize;
            if input[y][x] == '#' {
                continue;
            }

            let c = input[y][x];
            if c == 'E' {
                end = Point::new(x as i32, y as i32);
                path.push(end);
                break 'outer;
            }
            if c == '.' && prev_pos != Point::new(x, y) {
                prev_pos = pos;
                pos = Point::new(x, y);
                path.push(Point::new(x as i32, y as i32));
                break;
            }
        }
    }

    // calculate paths with cheating by using manhattan distance
    let mut result = 0;
    for i in 0..path.len() {
        for j in i + 101..path.len() {
            let x = path[i].x.abs_diff(path[j].x);
            let y = path[i].y.abs_diff(path[j].y);

            let distance = (x + y) as usize;
            if distance <= 20 && j - i - distance >= 100 {
                result += 1;
            }
        }
    }

    result
}
