pub type AppResult<T> = Result<T, Box<dyn std::error::Error>>;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub struct Point<T> {
    pub x: T,
    pub y: T,
}

impl<T: std::ops::AddAssign + Clone + Copy> Point<T> {
    pub fn new(x: T, y: T) -> Point<T> {
        Point { x, y }
    }

    pub fn add(&mut self, other: &Point<T>) {
        self.x += other.x;
        self.y += other.y;
    }
}

impl<T: Ord> Ord for Point<T> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.x.cmp(&self.x).then_with(|| other.y.cmp(&self.y))
    }
}

impl<T: Ord> PartialOrd for Point<T> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
