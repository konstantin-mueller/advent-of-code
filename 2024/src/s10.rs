use std::collections::HashSet;

const DIRECTIONS: [(i32, i32); 4] = [(0, 1), (0, -1), (1, 0), (-1, 0)];

pub fn part_one(input: &Vec<String>) -> usize {
    let mut zeroes = Vec::new();
    let numbers = parse_numbers(input, &mut zeroes);

    let mut visited = HashSet::new();
    let mut result = 0;
    for (i, j) in zeroes {
        result += dfs_part_one((i, j), &mut visited, &numbers);
        visited.clear();
    }

    result
}

fn parse_numbers(input: &Vec<String>, zeroes: &mut Vec<(usize, usize)>) -> Vec<Vec<usize>> {
    input
        .iter()
        .enumerate()
        .map(|(i, line)| {
            line.chars()
                .enumerate()
                .map(|(j, c)| {
                    if c == '0' {
                        zeroes.push((i, j))
                    };
                    c.to_digit(10).unwrap() as usize
                })
                .collect()
        })
        .collect()
}

fn dfs_part_one(
    node: (usize, usize),
    visited: &mut HashSet<(usize, usize)>,
    input: &Vec<Vec<usize>>,
) -> usize {
    if visited.contains(&node) {
        return 0;
    }
    visited.insert(node);

    if input[node.0][node.1] == 9 {
        return 1;
    }

    let mut result = 0;
    for direction in DIRECTIONS {
        if let Some(next) = is_trail(node, direction, input) {
            result += dfs_part_one(next, visited, input);
        }
    }

    result
}

fn is_trail(
    node: (usize, usize),
    direction: (i32, i32),
    input: &Vec<Vec<usize>>,
) -> Option<(usize, usize)> {
    let i = node.0 as i32 + direction.0;
    let j = node.1 as i32 + direction.1;

    if i < 0 || j < 0 {
        return None;
    }

    let i = i as usize;
    let j = j as usize;
    if i < input.len() && j < input[i].len() && input[node.0][node.1] + 1 == input[i][j] {
        Some((i, j))
    } else {
        None
    }
}

pub fn part_two(input: &Vec<String>) -> usize {
    let mut zeroes = Vec::new();
    let numbers = parse_numbers(input, &mut zeroes);

    let mut result = 0;
    for (i, j) in zeroes {
        result += dfs_part_two((i, j), &numbers);
    }

    result
}

fn dfs_part_two(node: (usize, usize), input: &Vec<Vec<usize>>) -> usize {
    if input[node.0][node.1] == 9 {
        return 1;
    }

    let mut result = 0;
    for direction in DIRECTIONS {
        if let Some(next) = is_trail(node, direction, input) {
            result += dfs_part_two(next, input);
        }
    }

    result
}

#[cfg(test)]
mod tests {
    use std::vec;

    use super::*;

    #[test]
    fn test_part_one() {
        assert_eq!(
            part_one(&vec![
                "099".to_string(),
                "123".to_string(),
                "654".to_string(),
                "789".to_string()
            ]),
            1
        );
    }

    #[test]
    fn test_part_one_2() {
        assert_eq!(
            part_one(&vec![
                "1190911".to_string(),
                "1111111".to_string(),
                "1192911".to_string(),
                "6543456".to_string(),
                "7111117".to_string(),
                "8111118".to_string(),
                "9111119".to_string()
            ]),
            2
        );
    }

    #[test]
    fn test_part_one_3() {
        assert_eq!(
            part_one(&vec![
                "0123".to_string(),
                "1234".to_string(),
                "8765".to_string(),
                "9876".to_string()
            ]),
            1
        );
    }

    #[test]
    fn test_part_one_example_input() {
        assert_eq!(
            part_one(&vec![
                "89010123".to_string(),
                "78121874".to_string(),
                "87430965".to_string(),
                "96549874".to_string(),
                "45678903".to_string(),
                "32019012".to_string(),
                "01329801".to_string(),
                "10456732".to_string(),
            ]),
            36
        );
    }

    #[test]
    fn test_part_two() {
        assert_eq!(
            part_two(&vec![
                "89010123".to_string(),
                "78121874".to_string(),
                "87430965".to_string(),
                "96549874".to_string(),
                "45678903".to_string(),
                "32019012".to_string(),
                "01329801".to_string(),
                "10456732".to_string(),
            ]),
            81
        );
    }

    #[test]
    fn test_is_trail_valid() {
        let input = vec![vec![1, 2, 3], vec![4, 5, 6], vec![7, 8, 9]];
        assert_eq!(is_trail((0, 0), (0, 1), &input), Some((0, 1)));
        assert_eq!(is_trail((1, 1), (0, 1), &input), Some((1, 2)));
        assert_eq!(is_trail((2, 0), (0, 1), &input), Some((2, 1)));
    }

    #[test]
    fn test_is_trail_invalid() {
        let input = vec![vec![1, 2, 3], vec![4, 5, 6], vec![7, 8, 9]];
        assert_eq!(is_trail((2, 2), (1, 0), &input), None);
        assert_eq!(is_trail((2, 2), (0, 1), &input), None);
        assert_eq!(is_trail((0, 0), (-1, 0), &input), None);
        assert_eq!(is_trail((0, 0), (0, -1), &input), None);
    }
}
