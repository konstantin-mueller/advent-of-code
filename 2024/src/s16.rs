use std::{
    cmp::Ordering,
    collections::{BinaryHeap, HashMap, HashSet},
};

use crate::utils::Point;

const DIRECTIONS: [(i32, i32); 4] = [(0, 1), (0, -1), (1, 0), (-1, 0)];

#[derive(Copy, Clone, Eq, PartialEq)]
struct State {
    cost: usize,
    position: Point<usize>,
    last_direction: usize,
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        other
            .cost
            .cmp(&self.cost)
            .then_with(|| self.position.cmp(&other.position))
            .then_with(|| self.last_direction.cmp(&other.last_direction))
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub fn part_one(input: &Vec<Vec<char>>) -> usize {
    let rows = input.len() as i32;
    let cols = input[0].len() as i32;
    let mut start = Point::new(0, 0);
    let mut end = start;
    for (i, line) in input.iter().enumerate() {
        for (j, &c) in line.iter().enumerate() {
            if c == 'S' {
                start = Point::new(j, i);
            } else if c == 'E' {
                end = Point::new(j, i);
            }
        }
    }

    let mut queue = BinaryHeap::new();
    queue.push(State {
        cost: 0,
        position: start,
        last_direction: 2,
    });

    let mut costs = HashMap::new();
    costs.insert((start, 2), 0);

    while let Some(State {
        cost,
        position,
        last_direction,
    }) = queue.pop()
    {
        if position == end {
            return cost;
        }

        for (i, (dx, dy)) in DIRECTIONS.iter().enumerate() {
            let next = Point::new(position.x as i32 + dx, position.y as i32 + dy);
            if next.x < 0
                || next.x >= cols
                || next.y < 0
                || next.y >= rows
                || input[next.y as usize][next.x as usize] == '#'
            {
                continue;
            }

            let next = Point::new(next.x as usize, next.y as usize);
            let mut next_cost = cost + 1;
            if last_direction != i {
                next_cost += 1000;
            }

            if next_cost < *costs.get(&(next, i)).unwrap_or(&usize::MAX) {
                costs.insert((next, i), next_cost);
                queue.push(State {
                    cost: next_cost,
                    position: next,
                    last_direction: i,
                });
            }
        }
    }

    panic!("No path found")
}

#[derive(Clone, Eq, PartialEq)]
struct State2 {
    cost: usize,
    position: Point<usize>,
    last_direction: usize,
    visited: HashSet<Point<usize>>,
}

impl Ord for State2 {
    fn cmp(&self, other: &Self) -> Ordering {
        other
            .cost
            .cmp(&self.cost)
            .then_with(|| self.position.cmp(&other.position))
            .then_with(|| self.last_direction.cmp(&other.last_direction))
    }
}

impl PartialOrd for State2 {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub fn part_two(input: &Vec<Vec<char>>) -> usize {
    let rows = input.len() as i32;
    let cols = input[0].len() as i32;
    let mut start = Point::new(0, 0);
    let mut end = start;
    for (i, line) in input.iter().enumerate() {
        for (j, &c) in line.iter().enumerate() {
            if c == 'S' {
                start = Point::new(j, i);
            } else if c == 'E' {
                end = Point::new(j, i);
            }
        }
    }

    let mut queue = BinaryHeap::new();
    queue.push(State2 {
        cost: 0,
        position: start,
        last_direction: 2,
        visited: HashSet::new(),
    });

    let mut lowest_cost = usize::MAX;
    let mut best_paths_positions = HashSet::new();

    let mut costs = HashMap::new();
    costs.insert((start, 2), 0);
    best_paths_positions.insert(start);

    while let Some(State2 {
        cost,
        position,
        last_direction,
        mut visited,
    }) = queue.pop()
    {
        visited.insert(position);

        if position == end {
            if cost < lowest_cost {
                lowest_cost = cost;
                best_paths_positions = visited.clone();
            } else if cost == lowest_cost {
                best_paths_positions.extend(&visited);
            }
            continue;
        }

        for (i, (dx, dy)) in DIRECTIONS.iter().enumerate() {
            let next = Point::new(position.x as i32 + dx, position.y as i32 + dy);
            if next.x < 0
                || next.x >= cols
                || next.y < 0
                || next.y >= rows
                || input[next.y as usize][next.x as usize] == '#'
            {
                continue;
            }

            let next = Point::new(next.x as usize, next.y as usize);
            let mut next_cost = cost + 1;
            if last_direction != i {
                next_cost += 1000;
            }

            if next_cost < *costs.get(&(next, i)).unwrap_or(&usize::MAX) {
                costs.insert((next, i), next_cost);
                queue.push(State2 {
                    cost: next_cost,
                    position: next,
                    last_direction: i,
                    visited: visited.clone(),
                });
            } else if next_cost == *costs.get(&(next, i)).unwrap_or(&usize::MAX) {
                queue.push(State2 {
                    cost: next_cost,
                    position: next,
                    last_direction: i,
                    visited: visited.clone(),
                });
            }
        }
    }

    best_paths_positions.len()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = r#"###############
#.......#....E#
#.#.###.#.###.#
#.....#.#...#.#
#.###.#####.#.#
#.#.#.......#.#
#.#.#####.###.#
#...........#.#
###.#.#####.#.#
#...#.....#.#.#
#.#.#.###.#.#.#
#.....#...#.#.#
#.###.#.#.#.#.#
#S..#.....#...#
###############"#;
        assert_eq!(part_one(&to_vec(input)), 7036);
    }

    #[test]
    fn test_part_one_2() {
        let input = r#"#################
#...#...#...#..E#
#.#.#.#.#.#.#.#.#
#.#.#.#...#...#.#
#.#.#.#.###.#.#.#
#...#.#.#.....#.#
#.#.#.#.#.#####.#
#.#...#.#.#.....#
#.#.#####.#.###.#
#.#.#.......#...#
#.#.###.#####.###
#.#.#...#.....#.#
#.#.#.#####.###.#
#.#.#.........#.#
#.#.#.#########.#
#S#.............#
#################"#;
        assert_eq!(part_one(&to_vec(input)), 11048);
    }

    #[test]
    fn test_part_one_3() {
        let input = r#"####E..
####.#.
.......
.###.##
.###.##
S....##"#;
        assert_eq!(part_one(&to_vec(input)), 1009);
    }

    #[test]
    fn test_part_one_4() {
        let input = r#"####...
####.#.
......E
.###.##
.###.##
S....##"#;
        assert_eq!(part_one(&to_vec(input)), 2009);
    }

    #[test]
    fn test_part_one_5() {
        let input = r#"####...
####.#.
......E
.###.#.
.###.#.
S......"#;
        assert_eq!(part_one(&to_vec(input)), 1009);
    }

    #[test]
    fn test_part_one_6() {
        let input = r#"#######E#######
#...#...#######
#.#...#.......#
#.###########.#
#S............#
###############"#;
        assert_eq!(part_one(&to_vec(input)), 3022);
    }

    #[test]
    fn test_part_two() {
        let input = r#"###############
#.......#....E#
#.#.###.#.###.#
#.....#.#...#.#
#.###.#####.#.#
#.#.#.......#.#
#.#.#####.###.#
#...........#.#
###.#.#####.#.#
#...#.....#.#.#
#.#.#.###.#.#.#
#.....#...#.#.#
#.###.#.#.#.#.#
#S..#.....#...#
###############"#;
        assert_eq!(part_two(&to_vec(input)), 45);
    }

    #[test]
    fn test_part_two_2() {
        let input = r#"#################
#...#...#...#..E#
#.#.#.#.#.#.#.#.#
#.#.#.#...#...#.#
#.#.#.#.###.#.#.#
#...#.#.#.....#.#
#.#.#.#.#.#####.#
#.#...#.#.#.....#
#.#.#####.#.###.#
#.#.#.......#...#
#.#.###.#####.###
#.#.#...#.....#.#
#.#.#.#####.###.#
#.#.#.........#.#
#.#.#.#########.#
#S#.............#
#################"#;
        assert_eq!(part_two(&to_vec(input)), 64);
    }

    fn to_vec(input: &str) -> Vec<Vec<char>> {
        input.lines().map(|line| line.chars().collect()).collect()
    }
}
