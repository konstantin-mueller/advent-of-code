use std::collections::{HashMap, HashSet};

pub fn part_one(input: &str) -> usize {
    let mut split = input.split("\n\n");
    let mut rules = HashMap::new();
    for line in split.next().unwrap().split_whitespace() {
        let (a, b) = line.split_once("|").unwrap();
        rules
            .entry(b)
            .and_modify(|v: &mut HashSet<&str>| {
                v.insert(a);
            })
            .or_insert(HashSet::from([a]));
    }

    split
        .next()
        .unwrap()
        .split_whitespace()
        .filter_map(|update| {
            let mut not_allowed: HashSet<&str> = HashSet::new();
            let mut updates = Vec::new();

            for number in update.split(",") {
                if not_allowed.contains(&number) {
                    return None;
                }

                if let Some(rule) = rules.get(number) {
                    not_allowed.extend(rule.iter());
                }

                updates.push(number);
            }

            Some(updates[updates.len() / 2].parse::<usize>().unwrap_or(0))
        })
        .sum()
}

pub fn part_two(input: &str) -> usize {
    let mut split = input.split("\n\n");
    let mut rules = HashMap::new();
    for line in split.next().unwrap().split_whitespace() {
        let (a, b) = line.split_once("|").unwrap();
        rules
            .entry(a)
            .and_modify(|v: &mut HashSet<&str>| {
                v.insert(b);
            })
            .or_insert(HashSet::from([b]));
    }

    split
        .next()
        .unwrap()
        .split_whitespace()
        .filter_map(|update| {
            // first validation check: if validation succeeds, it wasn't necessary to order the updates (was already valid) => ignore
            let (mut valid, mut updates) = is_valid(update, &rules);
            if valid {
                None
            } else {
                // order updates until validation succeeds
                while !valid {
                    let prev_updates = updates.join(",");
                    (valid, updates) = is_valid(&prev_updates, &rules);
                }

                Some(updates[updates.len() / 2].parse::<usize>().unwrap_or(0))
            }
        })
        .sum()
}

fn is_valid(update: &str, rules: &HashMap<&str, HashSet<&str>>) -> (bool, Vec<String>) {
    let mut updates = Vec::new();
    let mut is_valid = true;

    for (i, number) in update.split(",").enumerate() {
        updates.push(number.to_string());

        if let Some(rule) = rules.get(number) {
            let index = updates
                .iter()
                .enumerate()
                .filter_map(|(j, update)| {
                    if rule.contains(update.as_str()) {
                        Some(j)
                    } else {
                        None
                    }
                })
                .next();

            if let Some(j) = index {
                is_valid = false;

                let removed = updates.remove(j);
                updates.insert(i, removed);
            }
        }
    }

    (is_valid, updates)
}
