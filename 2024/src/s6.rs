use std::collections::HashSet;

pub fn part_one(input: &Vec<String>) -> usize {
    let mut guard: (isize, isize) = (0, 0);
    let mut obstacles: HashSet<(isize, isize)> = HashSet::new();
    for (i, line) in input.iter().enumerate() {
        for (j, c) in line.chars().enumerate() {
            if c == '^' {
                guard = (j as isize, i as isize);
            } else if c == '#' {
                obstacles.insert((j as isize, i as isize));
            }
        }
    }

    let mut guard_path = HashSet::new();
    guard_path.insert(guard);

    let mut guard_dir = (0, -1);
    loop {
        let next = (guard.0 + guard_dir.0, guard.1 + guard_dir.1);

        if obstacles.contains(&next) {
            guard_dir = update_direction(guard_dir);
            continue;
        }

        guard = next;
        guard_path.insert(guard);

        if (guard.0 == 0 && guard_dir.0 == -1)
            || (guard.1 == 0 && guard_dir.1 == -1)
            || (guard.0 == input[0].len() as isize - 1 && guard_dir.0 == 1)
            || (guard.1 == input.len() as isize - 1 && guard_dir.1 == 1)
        {
            break;
        }
    }

    guard_path.len()
}

pub fn part_two(input: &Vec<String>) -> usize {
    let mut guard: (isize, isize) = (0, 0);
    let mut obstacles: HashSet<(isize, isize)> = HashSet::new();
    for (i, line) in input.iter().enumerate() {
        for (j, c) in line.chars().enumerate() {
            if c == '^' {
                guard = (j as isize, i as isize);
            } else if c == '#' {
                obstacles.insert((j as isize, i as isize));
            }
        }
    }

    let mut board = input
        .iter()
        .map(|l| l.chars().collect::<Vec<_>>())
        .collect::<Vec<_>>();

    let mut guard_dir = (0, -1);
    let mut result = 0;
    let mut visited = HashSet::new();
    loop {
        let next = (guard.0 + guard_dir.0, guard.1 + guard_dir.1);
        if is_invalid(next.0, next.1, &board) {
            break;
        }

        if obstacles.contains(&next) {
            guard_dir = update_direction(guard_dir);
            continue;
        }

        // Do not count duplicate loops
        if visited.contains(&next) {
            guard = next;
            continue;
        }

        visited.insert(next);

        let i = next.1 as usize;
        let j = next.0 as usize;
        board[i][j] = '#';
        if is_loop(&board, guard, guard_dir) {
            result += 1;
        }
        board[i][j] = '.';

        guard = next;
    }

    result
}

fn is_loop(board: &[Vec<char>], pos: (isize, isize), dir: (isize, isize)) -> bool {
    let mut visited = HashSet::with_capacity(board.len() * board[0].len() / 100);
    let mut direction = dir;
    let mut current = pos;

    loop {
        let new = (current, direction);
        if visited.contains(&new) {
            return true;
        }

        visited.insert(new);

        if is_invalid(current.0 + direction.0, current.1 + direction.1, board) {
            break;
        }

        while board[(current.1 + direction.1) as usize][(current.0 + direction.0) as usize] == '#' {
            direction = update_direction(direction);
        }

        current.0 += direction.0;
        current.1 += direction.1;
        if is_invalid(current.0, current.1, board) {
            break;
        }
    }

    false
}

fn update_direction(direction: (isize, isize)) -> (isize, isize) {
    match direction {
        (0, -1) => (1, 0),
        (1, 0) => (0, 1),
        (0, 1) => (-1, 0),
        (-1, 0) => (0, -1),
        _ => unreachable!(),
    }
}

fn is_invalid(x: isize, y: isize, board: &[Vec<char>]) -> bool {
    (x < 0) || (x >= board[0].len() as isize) || (y < 0) || (y >= board.len() as isize)
}
