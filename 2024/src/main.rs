use std::{env, time::Instant};

use utils::Point;

mod resources;
mod s1;
mod s10;
mod s11;
mod s12;
mod s13;
mod s14;
mod s15;
mod s16;
mod s17;
mod s18;
mod s19;
mod s2;
mod s20;
mod s21;
mod s22;
mod s23;
mod s24;
mod s25;
mod s3;
mod s4;
mod s5;
mod s6;
mod s7;
mod s8;
mod s9;
mod utils;

fn main() {
    let functions = [
        one,
        two,
        three,
        four,
        five,
        six,
        seven,
        eight,
        nine,
        ten,
        eleven,
        twelve,
        thirteen,
        fourteen,
        fifteen,
        sixteen,
        seventeen,
        eighteen,
        nineteen,
        twenty,
        twentyone,
        twentytwo,
        twentythree,
        twentyfour,
        twentyfive,
    ];

    match env::args().skip(1).next() {
        Some(day) => {
            let number = match day.parse::<usize>() {
                Ok(number) if number < 1 || number > 25 => {
                    panic!("argument must be in range 1..=25")
                }
                Ok(number) => number,
                Err(err) => panic!("Could not parse number from argument {}: {}", day, err),
            };

            println!("Day {}:\n", day);
            functions[number - 1]()
        }
        None => functions.into_iter().for_each(|func| func()),
    }
}

fn timer<T>(func: &dyn Fn() -> T)
where
    T: std::fmt::Display,
{
    let mut times = Vec::new();
    for _ in 0..100 {
        let start = Instant::now();
        func();
        times.push(start.elapsed().as_micros());
    }

    println!(
        "100 iterations\tBest: {:.5}\tAverage: {:.5}\t(milliseconds)",
        *(times.iter().min().unwrap()) as f32 / 1000.0,
        times.iter().sum::<u128>() as f32 / times.len() as f32 / 1000.0
    );
    println!("Function result: {}\n\n", func());
}

fn one() {
    let input = resources::as_list(include_bytes!("../resources/1"));
    timer(&|| s1::part_one(&input));
    timer(&|| s1::part_two(&input));
}

fn two() {
    let input = resources::as_usize_list_of_lists(include_bytes!("../resources/2"));
    timer(&|| s2::part_one(&input));
    timer(&|| s2::part_two(&input));
}

fn three() {
    let input = resources::as_string(include_bytes!("../resources/3"));
    timer(&|| s3::part_one(&input));
    timer(&|| s3::part_two(&input));
}

fn four() {
    let input = resources::as_list_of_lists(include_bytes!("../resources/4"));
    timer(&|| s4::part_one(&input));
    timer(&|| s4::part_two(&input));
}

fn five() {
    let input = resources::as_string(include_bytes!("../resources/5"));
    timer(&|| s5::part_one(&input));
    timer(&|| s5::part_two(&input));
}

fn six() {
    let input = resources::as_list(include_bytes!("../resources/6"));
    timer(&|| s6::part_one(&input));
    // timer(&|| s6::part_two(&input));
    println!("{}", s6::part_two(&input));
}

fn seven() {
    let input = resources::as_list(include_bytes!("../resources/7"));
    timer(&|| s7::solve(&input, false));
    timer(&|| s7::solve(&input, true));
}

fn eight() {
    let input = resources::as_list_of_lists(include_bytes!("../resources/8"));
    timer(&|| s8::part_one(&input));
    timer(&|| s8::part_two(&input));
}

fn nine() {
    let input = resources::as_string(include_bytes!("../resources/9"));
    timer(&|| s9::part_one(&input));
    timer(&|| s9::part_two(&input));
}

fn ten() {
    let input = resources::as_list(include_bytes!("../resources/10"));
    timer(&|| s10::part_one(&input));
    timer(&|| s10::part_two(&input));
}

fn eleven() {
    let input = resources::as_string(include_bytes!("../resources/11"));
    timer(&|| s11::day_11(&input, 25));
    timer(&|| s11::day_11(&input, 75));
}

fn twelve() {
    let input = resources::as_list_of_lists(include_bytes!("../resources/12"));
    timer(&|| s12::part_one(&input));
    timer(&|| s12::part_two(&input));
}

fn thirteen() {
    let input = resources::as_list(include_bytes!("../resources/13"));
    timer(&|| s13::day_13(&input, false));
    timer(&|| s13::day_13(&input, true));
}

fn fourteen() {
    let input = resources::as_list(include_bytes!("../resources/14"));
    timer(&|| s14::part_one(&input, utils::Point::new(100, 102)));
    println!("{}", s14::part_two(&input, utils::Point::new(100, 102)));
}

fn fifteen() {
    let input = resources::as_list(include_bytes!("../resources/15"));
    timer(&|| s15::part_one(&input));
    timer(&|| s15::part_two(&input));
}

fn sixteen() {
    let input = resources::as_list_of_lists(include_bytes!("../resources/16"));
    timer(&|| s16::part_one(&input));
    timer(&|| s16::part_two(&input));
}

fn seventeen() {
    let input = resources::as_list(include_bytes!("../resources/17"));
    timer(&|| s17::part_one(&input));
    timer(&|| s17::part_two(&input));
}

fn eighteen() {
    let input = resources::as_list(include_bytes!("../resources/18"));
    timer(&|| s18::part_one(&input, Point::new(71, 71), 1024));
    timer(&|| s18::part_two(&input, Point::new(71, 71), 1024));
}

fn nineteen() {
    let input = resources::as_list(include_bytes!("../resources/19"));
    timer(&|| s19::part_one(&input));
    timer(&|| s19::part_two(&input));
}

fn twenty() {
    let input = resources::as_list_of_lists(include_bytes!("../resources/20"));
    timer(&|| s20::part_one(&input));
    timer(&|| s20::part_two(&input));
}

fn twentyone() {
    let input = resources::as_list(include_bytes!("../resources/21"));
    timer(&|| s21::part_one(&input));
    timer(&|| s21::part_two(&input));
}

fn twentytwo() {
    let input = resources::as_list(include_bytes!("../resources/22"));
    timer(&|| s22::part_one(&input));
    timer(&|| s22::part_two(&input));
}

fn twentythree() {
    let input = resources::as_list(include_bytes!("../resources/23"));
    timer(&|| s23::part_one(&input));
    timer(&|| s23::part_two(&input));
}

fn twentyfour() {
    let input = resources::as_string(include_bytes!("../resources/24"));
    timer(&|| s24::part_one(&input));
    timer(&|| s24::part_two(&input));
}

fn twentyfive() {
    let input = resources::as_string(include_bytes!("../resources/25"));
    timer(&|| s25::part_one(&input));
}
