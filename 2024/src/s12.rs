const DIRECTIONS: [(i32, i32); 4] = [(0, 1), (0, -1), (1, 0), (-1, 0)];
const CORNERS: [(i32, i32); 4] = [(-1, 1), (1, 1), (1, -1), (-1, -1)];

pub fn part_one(input: &Vec<Vec<char>>) -> usize {
    let rows = input.len();
    let cols = input[0].len();
    let mut visited = vec![vec![false; cols]; rows];
    let mut result = 0;

    for i in 0..rows {
        for j in 0..cols {
            if !visited[i][j] {
                let letter = input[i][j];
                let (area, perimeter) = dfs(i, j, letter, &mut visited, input);
                result += area * perimeter;
            }
        }
    }

    result
}

fn dfs(
    x: usize,
    y: usize,
    letter: char,
    visited: &mut Vec<Vec<bool>>,
    input: &Vec<Vec<char>>,
) -> (usize, usize) {
    let rows = input.len() as i32;
    let cols = input[0].len() as i32;

    let mut stack = vec![(x, y)];
    let mut area = 0;
    let mut perimeter = 0;

    while let Some((x, y)) = stack.pop() {
        if visited[x][y] {
            continue;
        }

        visited[x][y] = true;
        area += 1;

        for (dx, dy) in DIRECTIONS {
            let nx = x as i32 + dx;
            let ny = y as i32 + dy;

            if nx >= 0 && ny >= 0 && nx < rows && ny < cols {
                let nx = nx as usize;
                let ny = ny as usize;

                if input[nx][ny] == letter && !visited[nx][ny] {
                    stack.push((nx, ny));
                } else if input[nx][ny] != letter {
                    perimeter += 1;
                }
            } else {
                perimeter += 1;
            }
        }
    }

    (area, perimeter)
}

pub fn part_two(input: &Vec<Vec<char>>) -> usize {
    let rows = input.len();
    let cols = input[0].len();
    let mut visited = vec![vec![false; cols]; rows];
    let mut result = 0;

    for i in 0..rows {
        for j in 0..cols {
            if !visited[i][j] {
                let letter = input[i][j];
                let (area, perimeter) = dfs_part_two(i, j, letter, &mut visited, input);
                result += area * perimeter;
            }
        }
    }

    result
}

fn dfs_part_two(
    x: usize,
    y: usize,
    letter: char,
    visited: &mut Vec<Vec<bool>>,
    input: &Vec<Vec<char>>,
) -> (usize, usize) {
    let rows = input.len() as i32;
    let cols = input[0].len() as i32;

    let mut stack = vec![(x, y)];
    let mut area = 0;
    let mut perimeter = 0;

    while let Some((x, y)) = stack.pop() {
        if visited[x][y] {
            continue;
        }

        visited[x][y] = true;
        area += 1;
        perimeter += count_corners(x, y, input, letter);

        for (dx, dy) in DIRECTIONS {
            let nx = x as i32 + dx;
            let ny = y as i32 + dy;

            if nx >= 0 && ny >= 0 && nx < rows && ny < cols {
                let nx = nx as usize;
                let ny = ny as usize;

                if input[nx][ny] == letter && !visited[nx][ny] {
                    stack.push((nx, ny));
                }
            }
        }
    }

    (area, perimeter)
}

fn count_corners(x: usize, y: usize, input: &Vec<Vec<char>>, letter: char) -> usize {
    let rows = input.len() as i32;
    let cols = input[0].len() as i32;
    let mut corner_count = 0;

    for (dx, dy) in CORNERS {
        let nx = x as i32 + dx;
        let ny = y as i32 + dy;

        // it is a corner if x or y is out of bounds or input[x][y] != letter for the following x, y:
        // - top && right
        // - right && bottom
        // - bottom && left
        // - left && top
        if (nx < 0 || nx >= rows || input[nx as usize][y] != letter)
            && (ny < 0 || ny >= cols || input[x][ny as usize] != letter)
        {
            corner_count += 1;
        }
        // check corners where diagonal is other letter like:
        // CD
        // CC
        else if nx >= 0
            && nx < rows
            && input[nx as usize][y] == letter
            && ny >= 0
            && ny < cols
            && input[x][ny as usize] == letter
        {
            if input[nx as usize][ny as usize] != letter {
                corner_count += 1;
            }
        }
    }

    corner_count
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        assert_eq!(
            part_one(&vec![
                vec!['A', 'A', 'A', 'A'],
                vec!['B', 'B', 'C', 'D'],
                vec!['B', 'B', 'C', 'C'],
                vec!['E', 'E', 'E', 'C']
            ]),
            140
        );
    }

    #[test]
    fn test_part_one_2() {
        assert_eq!(
            part_one(&vec![
                vec!['O', 'O', 'O', 'O', 'O'],
                vec!['O', 'X', 'O', 'X', 'O'],
                vec!['O', 'O', 'O', 'O', 'O'],
                vec!['O', 'X', 'O', 'X', 'O'],
                vec!['O', 'O', 'O', 'O', 'O']
            ]),
            772
        );
    }

    #[test]
    fn test_part_one_3() {
        assert_eq!(
            part_one(&vec![
                vec!['R', 'R', 'R', 'R', 'I', 'I', 'C', 'C', 'F', 'F'],
                vec!['R', 'R', 'R', 'R', 'I', 'I', 'C', 'C', 'C', 'F'],
                vec!['V', 'V', 'R', 'R', 'R', 'C', 'C', 'F', 'F', 'F'],
                vec!['V', 'V', 'R', 'C', 'C', 'C', 'J', 'F', 'F', 'F'],
                vec!['V', 'V', 'V', 'V', 'C', 'J', 'J', 'C', 'F', 'E'],
                vec!['V', 'V', 'I', 'V', 'C', 'C', 'J', 'J', 'E', 'E'],
                vec!['V', 'V', 'I', 'I', 'I', 'C', 'J', 'J', 'E', 'E'],
                vec!['M', 'I', 'I', 'I', 'I', 'I', 'J', 'J', 'E', 'E'],
                vec!['M', 'I', 'I', 'I', 'S', 'I', 'J', 'E', 'E', 'E'],
                vec!['M', 'M', 'M', 'I', 'S', 'S', 'J', 'E', 'E', 'E'],
            ]),
            1930
        );
    }

    #[test]
    fn test_part_two() {
        assert_eq!(
            part_two(&vec![
                vec!['A', 'A', 'A', 'A'],
                vec!['B', 'B', 'C', 'D'],
                vec!['B', 'B', 'C', 'C'],
                vec!['E', 'E', 'E', 'C']
            ]),
            80
        );
    }

    #[test]
    fn test_part_two_2() {
        assert_eq!(
            part_two(&vec![
                vec!['O', 'O', 'O', 'O', 'O'],
                vec!['O', 'X', 'O', 'X', 'O'],
                vec!['O', 'O', 'O', 'O', 'O'],
                vec!['O', 'X', 'O', 'X', 'O'],
                vec!['O', 'O', 'O', 'O', 'O']
            ]),
            436
        );
    }

    #[test]
    fn test_part_two_3() {
        assert_eq!(
            part_two(&vec![
                vec!['R', 'R', 'R', 'R', 'I', 'I', 'C', 'C', 'F', 'F'],
                vec!['R', 'R', 'R', 'R', 'I', 'I', 'C', 'C', 'C', 'F'],
                vec!['V', 'V', 'R', 'R', 'R', 'C', 'C', 'F', 'F', 'F'],
                vec!['V', 'V', 'R', 'C', 'C', 'C', 'J', 'F', 'F', 'F'],
                vec!['V', 'V', 'V', 'V', 'C', 'J', 'J', 'C', 'F', 'E'],
                vec!['V', 'V', 'I', 'V', 'C', 'C', 'J', 'J', 'E', 'E'],
                vec!['V', 'V', 'I', 'I', 'I', 'C', 'J', 'J', 'E', 'E'],
                vec!['M', 'I', 'I', 'I', 'I', 'I', 'J', 'J', 'E', 'E'],
                vec!['M', 'I', 'I', 'I', 'S', 'I', 'J', 'E', 'E', 'E'],
                vec!['M', 'M', 'M', 'I', 'S', 'S', 'J', 'E', 'E', 'E'],
            ]),
            1206
        );
    }

    #[test]
    fn test_part_two_4() {
        assert_eq!(
            part_two(&vec![
                vec!['E', 'E', 'E', 'E', 'E'],
                vec!['E', 'X', 'X', 'X', 'X'],
                vec!['E', 'E', 'E', 'E', 'E'],
                vec!['E', 'X', 'X', 'X', 'X'],
                vec!['E', 'E', 'E', 'E', 'E'],
            ]),
            236
        );
    }

    #[test]
    fn test_part_two_5() {
        assert_eq!(
            part_two(&vec![
                vec!['A', 'A', 'A', 'A', 'A', 'A'],
                vec!['A', 'A', 'A', 'B', 'B', 'A'],
                vec!['A', 'A', 'A', 'B', 'B', 'A'],
                vec!['A', 'B', 'B', 'A', 'A', 'A'],
                vec!['A', 'B', 'B', 'A', 'A', 'A'],
                vec!['A', 'A', 'A', 'A', 'A', 'A'],
            ]),
            368
        );
    }
}
