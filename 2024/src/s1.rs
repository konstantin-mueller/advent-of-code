use std::collections::HashMap;

// extension trait
trait VecExt {
    fn insert_sorted(&mut self, number: usize);
}

impl VecExt for Vec<usize> {
    fn insert_sorted(&mut self, number: usize) {
        let index = self.partition_point(|&x| x <= number);
        self.insert(index, number);
    }
}

pub fn part_one(input: &Vec<String>) -> usize {
    let mut first: Vec<usize> = Vec::new();
    let mut second: Vec<usize> = Vec::new();
    for line in input {
        let mut parts = line.split_whitespace();
        first.insert_sorted(parts.next().unwrap().parse().unwrap());
        second.insert_sorted(parts.next().unwrap().parse().unwrap());
    }

    first
        .into_iter()
        .enumerate()
        .map(|(i, number)| number.abs_diff(second[i]))
        .sum()
}

pub fn part_two(input: &Vec<String>) -> usize {
    let mut map: HashMap<usize, (usize, usize)> = HashMap::new();
    for line in input {
        let mut parts = line.split_whitespace();

        map.entry(parts.next().unwrap().parse().unwrap())
            .and_modify(|(_, times)| *times += 1)
            .or_insert((0, 1));

        map.entry(parts.next().unwrap().parse().unwrap())
            .and_modify(|(appearances, _)| *appearances += 1)
            .or_insert((1, 0));
    }

    map.into_iter()
        .map(|(number, (appearances, times))| number * appearances * times)
        .sum()
}
