use std::{
    cmp::Ordering,
    collections::{BinaryHeap, HashMap, HashSet},
};

use crate::utils::Point;

const DIRECTIONS: [(i32, i32); 4] = [(0, 1), (0, -1), (1, 0), (-1, 0)];

#[derive(Copy, Clone, Eq, PartialEq)]
struct State {
    cost: usize,
    position: Point<usize>,
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        other
            .cost
            .cmp(&self.cost)
            .then_with(|| self.position.cmp(&other.position))
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub fn part_one(input: &Vec<String>, map_size: Point<i32>, fallen_bytes: usize) -> usize {
    let start = Point::new(0, 0);
    let end = Point::new(map_size.x as usize - 1, map_size.y as usize - 1);

    let corrupted: HashSet<Point<usize>> = input
        .iter()
        .take(fallen_bytes)
        .map(|l| {
            let mut numbers = l.split(",");
            Point::new(
                numbers.next().unwrap().parse().unwrap(),
                numbers.next().unwrap().parse().unwrap(),
            )
        })
        .collect();

    let mut queue = BinaryHeap::new();
    queue.push(State {
        cost: 0,
        position: start,
    });

    let mut costs = HashMap::new();
    costs.insert(start, 0);

    while let Some(State { cost, position }) = queue.pop() {
        if position == end {
            return cost;
        }

        for (dx, dy) in DIRECTIONS {
            let next = Point::new(position.x as i32 + dx, position.y as i32 + dy);
            if next.x < 0 || next.x >= map_size.x || next.y < 0 || next.y >= map_size.y {
                continue;
            }

            let next = Point::new(next.x as usize, next.y as usize);
            if corrupted.contains(&next) {
                continue;
            }

            let next_cost = cost + 1;
            if next_cost < *costs.get(&next).unwrap_or(&usize::MAX) {
                costs.insert(next, next_cost);
                queue.push(State {
                    cost: next_cost,
                    position: next,
                });
            }
        }
    }

    panic!("No path found")
}

#[derive(Clone, Eq, PartialEq)]
struct State2 {
    cost: usize,
    position: Point<usize>,
    visited: HashSet<Point<usize>>,
}

impl Ord for State2 {
    fn cmp(&self, other: &Self) -> Ordering {
        other
            .cost
            .cmp(&self.cost)
            .then_with(|| self.position.cmp(&other.position))
    }
}

impl PartialOrd for State2 {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub fn part_two(input: &Vec<String>, map_size: Point<i32>, min_fallen_bytes: usize) -> String {
    let start = Point::new(0, 0);
    let end = Point::new(map_size.x as usize - 1, map_size.y as usize - 1);

    let mut corrupted: HashSet<Point<usize>> = input
        .iter()
        .take(min_fallen_bytes)
        .map(|l| {
            let mut numbers = l.split(",");
            Point::new(
                numbers.next().unwrap().parse().unwrap(),
                numbers.next().unwrap().parse().unwrap(),
            )
        })
        .collect();

    let mut queue = BinaryHeap::new();
    queue.push(State2 {
        cost: 0,
        position: start,
        visited: HashSet::new(),
    });

    let mut costs = HashMap::new();
    costs.insert(start, 0);

    let mut corrupted_index = min_fallen_bytes + 1;
    'outer: loop {
        while let Some(State2 {
            cost,
            position,
            mut visited,
        }) = queue.pop()
        {
            visited.insert(position);

            if position == end {
                // add corrupted until one of the cells of the current best path (visited) is corrupted
                // => new best path has to be found
                loop {
                    corrupted_index += 1;
                    if corrupted_index == input.len() {
                        panic!("No solution found")
                    }

                    let mut numbers = input[corrupted_index].split(",");
                    let new_corrupted = Point::new(
                        numbers.next().unwrap().parse().unwrap(),
                        numbers.next().unwrap().parse().unwrap(),
                    );
                    corrupted.insert(new_corrupted);

                    if visited.contains(&new_corrupted) {
                        break;
                    }
                }

                queue.clear();
                costs.clear();
                queue.push(State2 {
                    cost: 0,
                    position: start,
                    visited: HashSet::new(),
                });
                costs.insert(start, 0);
                continue 'outer;
            }

            for (dx, dy) in DIRECTIONS {
                let next = Point::new(position.x as i32 + dx, position.y as i32 + dy);
                if next.x < 0 || next.x >= map_size.x || next.y < 0 || next.y >= map_size.y {
                    continue;
                }

                let next = Point::new(next.x as usize, next.y as usize);
                if corrupted.contains(&next) {
                    continue;
                }

                let next_cost = cost + 1;
                if next_cost < *costs.get(&next).unwrap_or(&usize::MAX) {
                    costs.insert(next, next_cost);
                    queue.push(State2 {
                        cost: next_cost,
                        position: next,
                        visited: visited.clone(),
                    });
                }
            }
        }

        // Dijkstra found no path
        break;
    }

    input[corrupted_index].clone()
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = r"5,4
4,2
4,5
3,0
2,1
6,3
2,4
1,5
0,6
3,3
2,6
5,1
1,2
5,5
2,5
6,5
1,4
0,4
6,4
1,1
6,1
1,0
0,5
1,6
2,0";

    #[test]
    fn test_part_one() {
        assert_eq!(
            part_one(
                &INPUT.lines().map(|l| l.to_string()).collect(),
                Point::new(7, 7),
                12
            ),
            22
        );
    }

    #[test]
    fn test_part_two() {
        assert_eq!(
            part_two(
                &INPUT.lines().map(|l| l.to_string()).collect(),
                Point::new(7, 7),
                12
            ),
            "6,1"
        );
    }
}
