use std::iter;

pub fn part_one(input: &String) -> usize {
    let disk_map = input
        .chars()
        .skip(1)
        .map(|c| c.to_digit(10).unwrap() as usize)
        .collect::<Vec<_>>();

    let mut blocks = Vec::new();
    let mut is_free = true;
    let mut id_number = 1;
    for block in disk_map {
        if is_free {
            blocks.extend(iter::repeat(0).take(block));
            is_free = false;
        } else {
            blocks.extend(iter::repeat(id_number).take(block));
            is_free = true;
            id_number += 1;
        }
    }

    let mut result = 0;
    let mut free_index = 0;
    let mut file_index = blocks.len() - 1;
    let number_of_zeroes = input.chars().next().unwrap().to_digit(10).unwrap() as usize;
    'outer: loop {
        while blocks[free_index] != 0 {
            result += blocks[free_index] * (free_index + number_of_zeroes);

            free_index += 1;
            if free_index >= blocks.len() || free_index > file_index {
                break 'outer;
            }
        }

        while blocks[file_index] == 0 {
            file_index -= 1;

            if free_index > file_index {
                break 'outer;
            }
        }

        let block = blocks[file_index];
        blocks[file_index] = 0;

        result += block * (free_index + number_of_zeroes);

        free_index += 1;
        file_index -= 1;

        if file_index == 0 || free_index > file_index {
            break;
        }
    }

    result
}

pub fn part_two(input: &String) -> usize {
    let disk_map = input
        .chars()
        .skip(1)
        .map(|c| c.to_digit(10).unwrap() as usize)
        .collect::<Vec<_>>();

    // contains blocks as tuples. each tuple is (id_number, block_size)
    let mut id_blocks = Vec::new();
    // contains all free blocks (only block_sizes)
    let mut free_blocks = Vec::new();
    let mut is_free = true;
    let mut id_number = 1;
    let number_of_zeroes = input.chars().next().unwrap().to_digit(10).unwrap() as usize;
    let mut number_count = number_of_zeroes;
    for block in disk_map {
        if is_free {
            free_blocks.push((block, number_count));
            is_free = false;
        } else {
            id_blocks.push((id_number, block, number_count));
            is_free = true;
            id_number += 1;
        }
        number_count += block;
    }

    id_blocks.reverse();

    let mut result = 0;
    for (id_number, size, file_index) in id_blocks {
        if let Some((index, (free_size, free_index))) = free_blocks
            .iter()
            .enumerate()
            .find(|(_, (block, free_index))| *block >= size && file_index > *free_index)
        {
            let free_size = free_size.clone();
            let free_index = free_index.clone();

            result += id_number * size * (free_index + free_index + size - 1) / 2;

            if free_size == size {
                free_blocks.remove(index);
            } else {
                free_blocks[index] = (
                    free_size - size,
                    free_index + (free_size - (free_size - size)),
                );
            }
        } else {
            // start = file_index
            // end = start + size - 1
            // size = end − start + 1 (= number of indices)
            // sum_of_indices = size * (start + end)​ / 2
            // result = id_number * sum_of_indices
            result += id_number * size * (file_index + file_index + size - 1) / 2;

            // remove last free block because it cannot be used by file blocks that come before it
            free_blocks.pop();
        }
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(&"12345".to_string()), 60);
    }

    #[test]
    fn test_part_one_2() {
        assert_eq!(part_one(&"2333133121414131402".to_string()), 1928);
    }

    #[test]
    fn test_part_two() {
        assert_eq!(part_two(&"12345".to_string()), 132);
    }

    #[test]
    fn test_part_two_1() {
        assert_eq!(part_two(&"133".to_string()), 6);
    }

    #[test]
    fn test_part_two_2() {
        assert_eq!(part_two(&"2333133121414131402".to_string()), 2858);
    }

    #[test]
    fn test_part_two_3() {
        assert_eq!(part_two(&"14312".to_string()), 24);
    }

    #[test]
    fn test_part_two_4() {
        assert_eq!(part_two(&"111".to_string()), 1);
    }

    #[test]
    fn test_part_two_5() {
        assert_eq!(part_two(&"112".to_string()), 5);
    }

    #[test]
    fn test_part_two_6() {
        assert_eq!(part_two(&"13211".to_string()), 7);
    }

    #[test]
    fn test_part_two_7() {
        assert_eq!(part_two(&"7042".to_string()), 34);
    }

    #[test]
    fn test_part_two_8() {
        assert_eq!(part_two(&"2333133121414131499".to_string()), 6204);
    }

    #[test]
    fn test_part_two_9() {
        assert_eq!(part_two(&"1022".to_string()), 3);
    }
}
