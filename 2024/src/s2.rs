pub fn part_one(input: &Vec<Vec<usize>>) -> usize {
    input
        .iter()
        .filter(|line| filter_part_one(line, 1, 0))
        .count()
}

fn filter_part_one(line: &Vec<usize>, start: usize, previous_index: usize) -> bool {
    let mut previous = line[previous_index];
    let increasing = previous < line[start];

    !line.iter().skip(start).any(|&number| {
        let is_unsafe = is_number_unsafe(number, previous, increasing);
        previous = number;
        is_unsafe
    })
}

fn is_number_unsafe(number: usize, previous: usize, increasing: bool) -> bool {
    let diff = number.abs_diff(previous);
    ((!increasing && previous < number) || (increasing && previous > number))
        || diff < 1
        || diff > 3
}

pub fn part_two(input: &Vec<Vec<usize>>) -> usize {
    input
        .iter()
        .filter(|line| {
            let mut before_previous = line[0];
            let mut previous = before_previous;
            let mut increasing = previous < line[1];
            let mut removed = false;

            let mut is_safe = !line.iter().enumerate().skip(1).any(|(i, &number)| {
                let mut is_unsafe = is_number_unsafe(number, previous, increasing);

                if is_unsafe && !removed {
                    removed = true;

                    // remove previous
                    let new_increasing = if i <= 2 {
                        before_previous < number
                    } else {
                        increasing
                    };
                    is_unsafe = is_number_unsafe(number, before_previous, new_increasing);

                    // look ahead to ensure that previous can be removed
                    if !is_unsafe && i < line.len() - 1 {
                        is_unsafe = is_number_unsafe(number, line[i + 1], increasing);
                    }

                    if is_unsafe {
                        // remove current
                        if i <= 2 {
                            increasing = previous < line[i + 1];
                        }
                        return false;
                    }
                    increasing = new_increasing;
                }

                before_previous = previous;
                previous = number;
                is_unsafe
            });

            if !is_safe {
                // remove first number
                is_safe = filter_part_one(line, 2, 1);
            }

            is_safe
        })
        .count()
}
