pub fn part_one(input: &str) -> usize {
    input.split("mul(").skip(1).map(parse_and_calc).sum()
}

pub fn part_two(input: &str) -> usize {
    let mut enabled = true;

    input
        .split("mul(")
        .map(|numbers| {
            let result = if enabled { parse_and_calc(numbers) } else { 0 };

            let do_ = numbers.find("do()");
            if let Some(dont_index) = numbers.find("don't()") {
                if let Some(do_index) = do_ {
                    enabled = dont_index < do_index;
                } else {
                    enabled = false;
                }
            } else if do_.is_some() {
                enabled = true;
            }

            result
        })
        .sum()
}

fn parse_and_calc(numbers: &str) -> usize {
    let mut split = numbers.split(",");
    let first = split.next().unwrap_or("0").parse().unwrap_or(0);
    let second = split
        .next()
        .unwrap_or("0")
        .split(")")
        .next()
        .unwrap_or("0")
        .parse()
        .unwrap_or(0);

    first * second
}
