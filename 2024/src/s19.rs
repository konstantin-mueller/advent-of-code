use std::collections::HashMap;

struct Node {
    children: HashMap<char, Node>,
    is_end: bool,
}

impl Node {
    fn new() -> Node {
        Node {
            children: HashMap::new(),
            is_end: false,
        }
    }
}

// https://en.wikipedia.org/wiki/Trie (efficient search of substrings)
struct Trie {
    root: Node,
}

impl Trie {
    fn new() -> Trie {
        Trie { root: Node::new() }
    }

    fn insert(&mut self, pattern: &str) {
        let mut node = &mut self.root;
        for c in pattern.chars() {
            node = node.children.entry(c).or_insert(Node::new());
        }
        node.is_end = true;
    }
}

pub fn part_one(input: &Vec<String>) -> usize {
    let patterns: Vec<&str> = input[0].split(", ").collect();

    let mut trie = Trie::new();
    for pattern in patterns {
        trie.insert(pattern);
    }

    input[2..]
        .iter()
        .filter(|design| count_arrangements_trie(design, &trie) != 0)
        .count()
}

pub fn part_two(input: &Vec<String>) -> usize {
    let patterns: Vec<&str> = input[0].split(", ").collect();

    let mut trie = Trie::new();
    for pattern in patterns {
        trie.insert(pattern);
    }

    input[2..]
        .iter()
        .map(|design| count_arrangements_trie(design, &trie))
        .sum()
}

fn count_arrangements_trie(design: &str, trie: &Trie) -> usize {
    let n = design.len();
    let mut dp = vec![0; n + 1];
    dp[0] = 1;

    for i in 0..n {
        let mut node = &trie.root;

        for j in i..n {
            let c = design.chars().nth(j).unwrap();

            if node.children.contains_key(&c) {
                node = node.children.get(&c).unwrap();

                if node.is_end {
                    dp[j + 1] += dp[i];
                }
            } else {
                break;
            }
        }
    }
    dp[n]
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = r"r, wr, b, g, bwu, rb, gb, br

brwrr
bggr
gbbr
rrbgbr
ubwu
bwurrg
brgr
bbrgwb";

    #[test]
    fn test_part_one() {
        assert_eq!(part_one(&INPUT.lines().map(|l| l.to_string()).collect()), 6);
    }

    #[test]
    fn test_part_two() {
        assert_eq!(
            part_two(&INPUT.lines().map(|l| l.to_string()).collect()),
            16
        );
    }
}
