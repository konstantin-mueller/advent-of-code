use std::collections::{HashMap, VecDeque};

struct State {
    number1: String,
    number2: String,
    operator: String,
    result: String,
}

impl State {
    fn from(line: &str) -> Self {
        let mut s = line.split_whitespace();
        State {
            number1: s.next().unwrap().to_string(),
            operator: s.next().unwrap().to_string(),
            number2: s.next().unwrap().to_string(),
            result: s.skip(1).next().unwrap().to_string(),
        }
    }
}

pub fn part_one(input: &String) -> usize {
    let mut parts = input.split("\n\n");
    let mut wires: HashMap<String, usize> = parts
        .next()
        .unwrap()
        .split("\n")
        .map(|line| {
            let mut s = line.split(": ");
            (
                s.next().unwrap().to_string(),
                s.next().unwrap().parse().unwrap(),
            )
        })
        .collect();

    let mut queue: VecDeque<State> = parts
        .next()
        .unwrap()
        .split("\n")
        .map(|line| State::from(line))
        .collect();

    while let Some(state) = queue.pop_front() {
        if let Some(n1) = wires.get(&state.number1) {
            if let Some(n2) = wires.get(&state.number2) {
                match state.operator.as_str() {
                    "AND" => wires.insert(state.result, n1 & n2),
                    "OR" => wires.insert(state.result, n1 | n2),
                    "XOR" => wires.insert(state.result, n1 ^ n2),
                    _ => unreachable!(),
                };

                continue;
            }
        }

        queue.push_back(state);
    }

    let mut sorted_wire_names = wires
        .keys()
        .filter(|name| name.starts_with("z"))
        .collect::<Vec<_>>();
    sorted_wire_names.sort_unstable();

    let mut result = 0;
    for (i, &name) in sorted_wire_names.iter().enumerate() {
        result += wires[name] << i;
    }

    result
}

pub fn part_two(input: &String) -> usize {
    0
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = r"x00: 1
x01: 1
x02: 1
y00: 0
y01: 1
y02: 0

x00 AND y00 -> z00
x01 XOR y01 -> z01
x02 OR y02 -> z02";

        assert_eq!(part_one(&input.to_string()), 4);
    }

    #[test]
    fn test_part_one_2() {
        let input = r"x00: 1
x01: 0
x02: 1
x03: 1
x04: 0
y00: 1
y01: 1
y02: 1
y03: 1
y04: 1

ntg XOR fgs -> mjb
y02 OR x01 -> tnw
kwq OR kpj -> z05
x00 OR x03 -> fst
tgd XOR rvg -> z01
vdt OR tnw -> bfw
bfw AND frj -> z10
ffh OR nrd -> bqk
y00 AND y03 -> djm
y03 OR y00 -> psh
bqk OR frj -> z08
tnw OR fst -> frj
gnj AND tgd -> z11
bfw XOR mjb -> z00
x03 OR x00 -> vdt
gnj AND wpb -> z02
x04 AND y00 -> kjc
djm OR pbm -> qhw
nrd AND vdt -> hwm
kjc AND fst -> rvg
y04 OR y02 -> fgs
y01 AND x02 -> pbm
ntg OR kjc -> kwq
psh XOR fgs -> tgd
qhw XOR tgd -> z09
pbm OR djm -> kpj
x03 XOR y03 -> ffh
x00 XOR y04 -> ntg
bfw OR bqk -> z06
nrd XOR fgs -> wpb
frj XOR qhw -> z04
bqk OR frj -> z07
y03 OR x01 -> nrd
hwm AND bqk -> z03
tgd XOR rvg -> z12
tnw OR pbm -> gnj";

        assert_eq!(part_one(&input.to_string()), 2024);
    }
}
