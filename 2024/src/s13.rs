const PART_TWO_PRICE: usize = 10000000000000;

pub fn day_13(input: &Vec<String>, part_two: bool) -> usize {
    let mut a_buttons: Vec<(usize, usize)> = Vec::new();
    let mut b_buttons: Vec<(usize, usize)> = Vec::new();
    let mut prizes: Vec<(usize, usize)> = Vec::new();
    for line in input {
        if line.starts_with("Button A") {
            a_buttons.push(parse_x_y(line));
        } else if line.starts_with("Button B") {
            b_buttons.push(parse_x_y(line));
        } else if line.starts_with("Prize") {
            let mut split = line.split(", Y=");
            let x = split.next().unwrap().split("=").last().unwrap();
            let mut price = (x.parse().unwrap(), split.next().unwrap().parse().unwrap());

            if part_two {
                price.0 += PART_TWO_PRICE;
                price.1 += PART_TWO_PRICE;
            }

            prizes.push(price);
        }
    }

    let mut result = 0;
    for i in 0..prizes.len() {
        let a = a_buttons[i];
        let b = b_buttons[i];
        let prize = prizes[i];

        // solve equations:
        // a.x * x + b.x * x = prize.x
        // a.y * y + b.y * y = prize.y
        // eliminate y: multiply first by b.y and second by b.x
        let mut first_x = a.0 * b.1;
        let first_y = b.0 * b.1;
        let mut first_prize = prize.0 * b.1;
        let second_x = a.1 * b.0;
        let second_y = b.1 * b.0;
        let second_prize = prize.1 * b.0;

        // substract equations to eliminate y
        if first_y != second_y {
            panic!("expected first_y to be equal to second_y");
        }
        first_x = first_x.abs_diff(second_x);
        first_prize = first_prize.abs_diff(second_prize);

        // solve for x:
        let x = first_prize as f64 / first_x as f64;
        // part one is limited by 100 button presses. there is no solution if the result is not an int => skip
        if (!part_two && x > 100.0) || x != x.trunc() {
            continue;
        }

        // substitute x back to find y:
        let first_x = a.0 as f64 * x;
        let first_prize = prize.0 as f64 - first_x;
        let y = first_prize / b.0 as f64;

        if (!part_two && y > 100.0) || y != y.trunc() {
            continue;
        }

        result += x as usize * 3 + y as usize;
    }

    result
}

fn parse_x_y(line: &str) -> (usize, usize) {
    let mut split = line.split(", Y+");
    let x = split.next().unwrap().split("+").last().unwrap();
    (x.parse().unwrap(), split.next().unwrap().parse().unwrap())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        assert_eq!(
            day_13(
                &vec![
                    "Button A: X+94, Y+34".to_string(),
                    "Button B: X+22, Y+67".to_string(),
                    "Prize: X=8400, Y=5400".to_string()
                ],
                false
            ),
            280
        );
    }

    #[test]
    fn test_part_one_2() {
        assert_eq!(
            day_13(
                &vec![
                    "Button A: X+15, Y+97".to_string(),
                    "Button B: X+64, Y+20".to_string(),
                    "Prize: X=2942, Y=9966".to_string()
                ],
                false
            ),
            98 * 3 + 23
        );
    }
}
