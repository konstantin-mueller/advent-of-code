from dataclasses import dataclass


@dataclass
class Part:
    x: int
    m: int
    a: int
    s: int

    @staticmethod
    def from_list(values: list[str]) -> "Part":
        return Part(int(values[0]), int(values[1]), int(values[2]), int(values[3]))


def aplenty(inp: str) -> int:
    workflows, parts = inp.split("\n\n")
    workflows = {
        workflow[: workflow.find("{")]: list(
            map(
                lambda rule: parse_rule(
                    rule[0], rule[1], workflow[: workflow.find("{")]
                ),
                enumerate(workflow[workflow.find("{") + 1 : -1].split(",")),
            )
        )
        for workflow in workflows.splitlines()
    }

    return sum(
        calc(
            "in",
            0,
            Part.from_list(
                part[3:-1]
                .replace("m=", "")
                .replace("a=", "")
                .replace("s=", "")
                .split(",")
            ),
            workflows,
        )
        for part in parts.splitlines()
    )


def parse_rule(i: int, rule: str, current_rule: str):
    if ":" in rule:
        condition, next_rule = rule.split(":")

        if "<" in condition:
            return (
                lambda part: (next_rule, 0)
                if getattr(part, condition[0]) < int(condition[2:])
                else (current_rule, i + 1)
            )

        return (
            lambda part: (next_rule, 0)
            if getattr(part, condition[0]) > int(condition[2:])
            else (current_rule, i + 1)
        )

    return lambda _: (rule, 0)


def calc(rule: str, i: int, part: Part, workflows) -> int:
    match rule:
        case "A":
            return part.x + part.m + part.a + part.s
        case "R":
            return 0
        case _:
            next_rule, next_i = workflows[rule][i](part)
            return calc(next_rule, next_i, part, workflows)


if __name__ == "__main__":
    with open("resources/19", "r", encoding="utf-8") as f:
        print(aplenty(f.read()))
