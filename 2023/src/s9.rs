pub fn mirage_maintenance(report: &Vec<String>, part_two: bool) -> i32 {
    report
        .into_iter()
        .map(|line| {
            let numbers: Vec<i32> = line
                .split_whitespace()
                .map(|number| number.parse().unwrap())
                .collect();

            if part_two {
                numbers[0] - calc(&numbers, part_two)
            } else {
                calc(&numbers, part_two) + numbers.last().unwrap()
            }
        })
        .sum()
}

fn calc(numbers: &Vec<i32>, part_two: bool) -> i32 {
    let new_numbers: Vec<i32> = (0..numbers.len() - 1)
        .map(|i| numbers[i + 1] - numbers[i])
        .collect();

    if new_numbers.iter().all(|number| *number == new_numbers[0]) {
        new_numbers[0]
    } else if part_two {
        new_numbers[0] - calc(&new_numbers, part_two)
    } else {
        calc(&new_numbers, part_two) + new_numbers.last().unwrap()
    }
}
