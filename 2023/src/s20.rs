use std::collections::{HashMap, VecDeque};

pub fn pulse_propagation(configuration: &Vec<String>) -> usize {
    let mut low = 0;
    let mut high = 0;

    let mut broadcaster = vec![];
    let mut start_flip_flop = HashMap::new();
    let mut all_conjunctions = vec![];
    let mut instructions = HashMap::new();
    for line in configuration {
        if line.starts_with("broadcaster") {
            broadcaster = line.split("> ").nth(1).unwrap().split(", ").collect();
            continue;
        }

        let mut split = line.split(" -> ");
        let module = &split.next().unwrap()[1..];
        let destinations: Vec<_> = split.next().unwrap().split(", ").collect();
        instructions.insert(module, destinations.clone());

        if line.starts_with("%") {
            start_flip_flop.insert(module, false);
        } else {
            all_conjunctions.push(module);
        }
    }

    let mut flip_flop = start_flip_flop.clone();

    let mut conjunction_memory: HashMap<&str, HashMap<&str, bool>> = HashMap::new();
    for conj in all_conjunctions {
        conjunction_memory.insert(conj, HashMap::new());

        for (trigger, destinations) in &instructions {
            if destinations.contains(&conj) {
                conjunction_memory.entry(conj).and_modify(|triggers| {
                    triggers.insert(trigger, false);
                });
            }
        }
    }
    let mut conjunction = conjunction_memory.clone();

    let mut cycle = 0;
    for i in 1..=1000 {
        let mut queue = VecDeque::new();
        for module in &broadcaster {
            queue.push_back((*module, false, "broadcaster"));
        }

        low += 1 + broadcaster.len();

        while let Some((module, is_high, triggered_by)) = queue.pop_front() {
            if let Some(state) = flip_flop.get_mut(module) {
                if is_high {
                    continue;
                }

                *state = !*state;
                for next_module in &instructions[module] {
                    queue.push_back((*next_module, *state, module));
                }

                if *state {
                    high += instructions[module].len();
                } else {
                    low += instructions[module].len();
                }
            } else if conjunction.contains_key(module) {
                conjunction.entry(module).and_modify(|triggers| {
                    triggers
                        .entry(triggered_by)
                        .and_modify(|high_or_low| *high_or_low = is_high);
                });

                if conjunction[module].iter().all(|(_, _is_high)| *_is_high) {
                    for next_module in &instructions[module] {
                        queue.push_back((*next_module, false, module));
                    }
                    low += instructions[module].len();
                } else {
                    for next_module in &instructions[module] {
                        queue.push_back((*next_module, true, module));
                    }
                    high += instructions[module].len();
                }
            }
        }

        if flip_flop == start_flip_flop && conjunction == conjunction_memory {
            cycle = i;
            break;
        }
    }

    // no cycle found -> return result
    if cycle == 0 {
        return high * low;
    }

    low *= 1000 / cycle;
    high *= 1000 / cycle;

    for _ in 0..1000 % cycle {
        let mut queue = VecDeque::new();
        for module in &broadcaster {
            queue.push_back((*module, false, "broadcaster"));
        }

        low += 1 + broadcaster.len();

        while let Some((module, is_high, triggered_by)) = queue.pop_front() {
            if let Some(state) = flip_flop.get_mut(module) {
                if is_high {
                    continue;
                }

                *state = !*state;
                for next_module in &instructions[module] {
                    queue.push_back((*next_module, *state, module));
                }

                if *state {
                    high += instructions[module].len();
                } else {
                    low += instructions[module].len();
                }
            } else {
                conjunction.entry(module).and_modify(|triggers| {
                    triggers
                        .entry(triggered_by)
                        .and_modify(|high_or_low| *high_or_low = is_high);
                });

                if conjunction[module].iter().all(|(_, _is_high)| *_is_high) {
                    for next_module in &instructions[module] {
                        queue.push_back((*next_module, false, module));
                    }
                    low += instructions[module].len();
                } else {
                    for next_module in &instructions[module] {
                        queue.push_back((*next_module, true, module));
                    }
                    high += instructions[module].len();
                }
            }
        }
    }

    low * high
}
