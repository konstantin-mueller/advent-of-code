use std::collections::{HashMap, HashSet};

pub fn parabolic_reflector_dish(map: &Vec<Vec<char>>) -> usize {
    let (round_rocks, cube_rocks) = parse_input(map);

    tilt_north(map.len(), &round_rocks, &cube_rocks)
        .into_iter()
        .map(|(_, y)| map.len() - y)
        .sum()
}

pub fn part_two(map: &Vec<Vec<char>>) -> usize {
    let (mut round_rocks, cube_rocks) = parse_input(map);

    let max_x = map[0].len();
    let max_y = map.len();

    let mut cache: HashMap<(usize, usize), usize> = HashMap::new();
    let mut prev = 0;
    let mut j = 0;
    let mut k = 0;
    for i in 1.. {
        round_rocks = cycle(max_x, max_y, &round_rocks, &cube_rocks);
        let result = round_rocks.iter().map(|(_, y)| max_y - y).sum();

        if let Some(existing) = cache.insert((result, prev), i) {
            j = i;
            k = i - existing;
            break;
        }
        prev = result;
    }

    for _ in 0..(1_000_000_000 - j) % k {
        round_rocks = cycle(max_x, max_y, &round_rocks, &cube_rocks);
    }

    round_rocks.into_iter().map(|(_, y)| max_y - y).sum()
}

fn parse_input(map: &Vec<Vec<char>>) -> (HashSet<(usize, usize)>, HashSet<(usize, usize)>) {
    let round_rocks = map
        .iter()
        .enumerate()
        .flat_map(|(y, line)| {
            line.iter()
                .enumerate()
                .filter_map(move |(x, char)| if *char == 'O' { Some((x, y)) } else { None })
        })
        .collect();

    let cube_rocks = map
        .iter()
        .enumerate()
        .flat_map(|(y, line)| {
            line.iter()
                .enumerate()
                .filter_map(move |(x, char)| if *char == '#' { Some((x, y)) } else { None })
        })
        .collect();

    (round_rocks, cube_rocks)
}

fn cycle(
    max_x: usize,
    max_y: usize,
    round_rocks: &HashSet<(usize, usize)>,
    cube_rocks: &HashSet<(usize, usize)>,
) -> HashSet<(usize, usize)> {
    let north = tilt_north(max_y, round_rocks, cube_rocks);
    let west = tilt_west(max_x, &north, cube_rocks);
    let south = tilt_south(max_y, &west, cube_rocks);
    tilt_east(max_x, &south, cube_rocks)
}

fn tilt_north(
    max_y: usize,
    round_rocks: &HashSet<(usize, usize)>,
    cube_rocks: &HashSet<(usize, usize)>,
) -> HashSet<(usize, usize)> {
    let mut moved_rocks = HashSet::new();
    round_rocks.iter().for_each(|(x, y)| {
        let mut new_y = *y;

        if moved_rocks.contains(&(*x, new_y)) {
            new_y = (new_y + 1..max_y)
                .find(|corrected_y| !moved_rocks.contains(&(*x, *corrected_y)))
                .unwrap();
        } else {
            for current_y in (0..*y).rev() {
                if cube_rocks.contains(&(*x, current_y)) || moved_rocks.contains(&(*x, current_y)) {
                    break;
                }
                new_y = current_y;
            }
        }

        moved_rocks.insert((*x, new_y));
    });
    moved_rocks
}

fn tilt_east(
    max_x: usize,
    round_rocks: &HashSet<(usize, usize)>,
    cube_rocks: &HashSet<(usize, usize)>,
) -> HashSet<(usize, usize)> {
    let mut moved_rocks = HashSet::new();
    round_rocks.iter().for_each(|(x, y)| {
        let mut new_x = *x;

        if moved_rocks.contains(&(new_x, *y)) {
            new_x = (0..new_x)
                .rev()
                .find(|corrected_x| !moved_rocks.contains(&(*corrected_x, *y)))
                .unwrap();
        } else {
            for current_x in *x + 1..max_x {
                if cube_rocks.contains(&(current_x, *y)) || moved_rocks.contains(&(current_x, *y)) {
                    break;
                }
                new_x = current_x;
            }
        }

        moved_rocks.insert((new_x, *y));
    });
    moved_rocks
}

fn tilt_south(
    max_y: usize,
    round_rocks: &HashSet<(usize, usize)>,
    cube_rocks: &HashSet<(usize, usize)>,
) -> HashSet<(usize, usize)> {
    let mut moved_rocks = HashSet::new();
    round_rocks.iter().for_each(|(x, y)| {
        let mut new_y = *y;

        if moved_rocks.contains(&(*x, new_y)) {
            new_y = (0..new_y)
                .rev()
                .find(|corrected_y| !moved_rocks.contains(&(*x, *corrected_y)))
                .unwrap();
        } else {
            for current_y in *y + 1..max_y {
                if cube_rocks.contains(&(*x, current_y)) || moved_rocks.contains(&(*x, current_y)) {
                    break;
                }
                new_y = current_y;
            }
        }

        moved_rocks.insert((*x, new_y));
    });
    moved_rocks
}

fn tilt_west(
    max_x: usize,
    round_rocks: &HashSet<(usize, usize)>,
    cube_rocks: &HashSet<(usize, usize)>,
) -> HashSet<(usize, usize)> {
    let mut moved_rocks = HashSet::new();
    round_rocks.iter().for_each(|(x, y)| {
        let mut new_x = *x;

        if moved_rocks.contains(&(new_x, *y)) {
            new_x = (new_x + 1..max_x)
                .find(|corrected_x| !moved_rocks.contains(&(*corrected_x, *y)))
                .unwrap();
        } else {
            for current_x in (0..*x).rev() {
                if cube_rocks.contains(&(current_x, *y)) || moved_rocks.contains(&(current_x, *y)) {
                    break;
                }
                new_x = current_x;
            }
        }

        moved_rocks.insert((new_x, *y));
    });
    moved_rocks
}
