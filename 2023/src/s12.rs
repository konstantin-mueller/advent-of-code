use std::{collections::HashMap, iter::repeat};

pub fn hot_springs(records: &Vec<String>) -> i32 {
    records.iter().map(calculate).sum()
}

fn calculate(line: &String) -> i32 {
    let mut split = line.split_whitespace();
    let record = split.next().unwrap();

    let numbers: Vec<i32> = split
        .next()
        .unwrap()
        .split(",")
        .map(|number| number.parse().unwrap())
        .collect();

    let mut v = vec![0; numbers.len()];

    let mut prev_was_dot = false;
    let mut count = 0;
    for char in record.chars() {
        if char == '#' {
            if count >= v.len() {
                return 0;
            }

            *v.get_mut(count).unwrap() += 1;
            prev_was_dot = false
        } else if char == '?' {
            return calculate(&line.replacen("?", ".", 1)) + calculate(&line.replacen("?", "#", 1));
        } else if !prev_was_dot {
            if v[count] == numbers[count] {
                count += 1;
            } else if v[count] != 0 {
                return 0;
            }

            prev_was_dot = true;
        }
    }

    if count == numbers.len() || v.last().unwrap() == numbers.last().unwrap() {
        1
    } else {
        0
    }
}

pub fn part_two(records: &Vec<String>) -> i64 {
    let mut cache = HashMap::new();

    records
        .into_iter()
        .map(|record| {
            let mut split = record.split_whitespace();
            let unfolded_records = repeat(split.next().unwrap().to_string())
                .take(5)
                .reduce(|acc, element| format!("{acc}?{element}"))
                .unwrap();

            let numbers = repeat(split.next().unwrap().to_string())
                .take(5)
                .reduce(|acc, element| format!("{acc},{element}"))
                .unwrap()
                .split(",")
                .map(|number| number.parse().unwrap())
                .collect();

            calc(unfolded_records, &numbers, &mut cache)
        })
        .sum()
}

fn calc<'a>(
    record: String,
    groups: &Vec<usize>,
    cache: &mut HashMap<(String, Vec<usize>), i64>,
) -> i64 {
    if let Some(cached_result) = cache.get(&(record.clone(), groups.to_vec())) {
        return *cached_result;
    }

    if groups.is_empty() {
        if !record.contains("#") {
            return 1;
        }
        return 0;
    }

    if record.is_empty() {
        return 0;
    }

    let result = match record.chars().next().unwrap() {
        '#' => damaged(record.clone(), groups, groups[0], cache),
        '.' => operational(record.clone(), groups, cache),
        '?' => {
            operational(record.clone(), groups, cache)
                + damaged(record.clone(), groups, groups[0], cache)
        }
        _ => unreachable!(),
    };

    cache.insert((record, groups.to_vec()), result);

    result
}

fn damaged(
    record: String,
    groups: &Vec<usize>,
    next_group: usize,
    cache: &mut HashMap<(String, Vec<usize>), i64>,
) -> i64 {
    let current = if next_group > record.len() {
        record.clone()
    } else {
        record[..next_group].to_string()
    };

    if current.replace("?", "#") != repeat('#').take(next_group).collect::<String>() {
        return 0;
    }

    if record.len() == next_group {
        if groups.len() == 1 {
            return 1;
        }
        return 0;
    }

    let current_char = record.as_bytes()[next_group];
    if current_char == b'?' || current_char == b'.' {
        return calc(
            record[next_group + 1..].to_string(),
            &groups[1..].to_vec(),
            cache,
        );
    }

    0
}

fn operational(
    record: String,
    groups: &Vec<usize>,
    cache: &mut HashMap<(String, Vec<usize>), i64>,
) -> i64 {
    calc(record[1..].to_string(), groups, cache)
}
