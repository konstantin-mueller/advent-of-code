pub fn cosmic_expansion(image: &String, expansion: usize) -> usize {
    let max_x = image.split("\n").next().unwrap().len();
    let mut first = vec![0; max_x];
    let mut second = vec![0; max_x];

    for (pos, _) in image
        .replace("\n", "")
        .chars()
        .enumerate()
        .filter(|(_, b)| *b == '#')
    {
        first[pos % max_x] += 1;
        second[pos / max_x] += 1;
    }

    calc_distance(&first, expansion) + calc_distance(&second, expansion)
}

fn calc_distance(counts: &[usize], expansion: usize) -> usize {
    let mut empty = 0;
    let mut sum = 0;
    let mut items = 0;
    let mut result = 0;

    for (i, count) in counts.iter().enumerate() {
        if *count > 0 {
            let expanded = i + expansion * empty;
            result += count * (items * expanded - sum);
            sum += count * expanded;
            items += count;
        } else {
            empty += 1;
        }
    }

    result
}
