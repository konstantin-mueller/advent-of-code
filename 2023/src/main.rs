use std::time::Instant;

mod resources;
mod s1;
mod s10;
mod s11;
mod s12;
mod s13;
mod s14;
mod s15;
mod s16;
mod s17;
mod s2;
mod s20;
mod s21;
// mod s22;
// mod s23;
// mod s24;
// mod s25;
mod s3;
mod s4;
mod s5;
mod s6;
mod s7;
mod s8;
mod s9;

fn main() {
    println!("Day 1");
    one();
    println!();

    println!("Day 2");
    two();
    println!();

    println!("Day 3");
    three();
    println!();

    println!("Day 4");
    four();
    println!();

    println!("Day 5");
    five();
    println!();

    println!("Day 6");
    six();
    println!();

    println!("Day 7");
    seven();
    println!();

    println!("Day 8");
    eight();
    println!();

    println!("Day 9");
    nine();
    println!();

    println!("Day 10");
    ten();
    println!();

    println!("Day 11");
    eleven();
    println!();

    println!("Day 12");
    twelve();
    println!();

    println!("Day 13");
    thirteen();
    println!();

    println!("Day 14");
    fourteen();
    println!();

    println!("Day 15");
    fifteen();
    println!();

    println!("Day 16");
    sixteen();
    println!();

    println!("Day 17");
    seventeen();
    println!();

    println!("Day 20");
    twenty();
    println!();

    println!("Day 21");
    twentyone();
    println!();

    // println!("Day 22");
    // twentytwo();
    // println!();

    // println!("Day 23");
    // twentythree();
    // println!();

    // println!("Day 24");
    // twentyfour();
    // println!();

    // println!("Day 25");
    // twentyfive();
    // println!();
}

fn timer<T>(func: &dyn Fn() -> T)
where
    T: std::fmt::Display,
{
    let mut times = Vec::new();
    for _ in 0..100 {
        let start = Instant::now();
        func();
        times.push(start.elapsed().as_micros());
    }

    println!(
        "100 iterations\tBest: {:.5}\tAverage: {:.5}\t(milliseconds)",
        *(times.iter().min().unwrap()) as f32 / 1000.0,
        times.iter().sum::<u128>() as f32 / times.len() as f32 / 1000.0
    );
    println!("Function result: {}\n\n", func());
}

fn one() {
    let document = resources::as_list(include_bytes!("../resources/1"));

    timer(&|| s1::trebuchet(&document));
    timer(&|| s1::part_two(&document));
}

fn two() {
    let games = resources::as_list(include_bytes!("../resources/2"));

    timer(&|| s2::cube_conundrum(&games));
    timer(&|| s2::part_two(&games));
}

fn three() {
    let schematic = resources::as_list(include_bytes!("../resources/3"));

    timer(&|| s3::gear_ratios(&schematic));
    timer(&|| s3::part_two(&schematic));
}

fn four() {
    let cards = resources::as_list(include_bytes!("../resources/4"));

    timer(&|| s4::scratchcards(&cards));
    timer(&|| s4::part_two(&cards));
}

fn five() {
    let almanac = resources::as_string(include_bytes!("../resources/5"));

    timer(&|| s5::if_you_give_a_seed_a_fertilizer(&almanac));
    timer(&|| s5::part_two(&almanac));
}

fn six() {
    let paper = resources::as_list(include_bytes!("../resources/6"));

    timer(&|| s6::wait_for_it(&paper));
    timer(&|| s6::part_two(&paper));
}

fn seven() {
    let hands = resources::as_list(include_bytes!("../resources/7"));

    timer(&|| s7::camel_cards(&hands));
    timer(&|| s7::part_two(&hands));
}

fn eight() {
    let documents = resources::as_list(include_bytes!("../resources/8"));

    timer(&|| s8::haunted_wasteland(&documents));
    println!("{}", s8::part_two(&documents));
}

fn nine() {
    let report = resources::as_list(include_bytes!("../resources/9"));

    timer(&|| s9::mirage_maintenance(&report, false));
    timer(&|| s9::mirage_maintenance(&report, true));
}

fn ten() {
    let maze = resources::as_list(include_bytes!("../resources/10"));

    timer(&|| s10::pipe_maze(&maze));
    timer(&|| s10::part_two(&maze));
}

fn eleven() {
    let image = resources::as_string(include_bytes!("../resources/11"));

    timer(&|| s11::cosmic_expansion(&image, 1));
    timer(&|| s11::cosmic_expansion(&image, 999_999));
}

fn twelve() {
    let records = resources::as_list(include_bytes!("../resources/12"));

    println!("{}", s12::hot_springs(&records));
    println!("{}", s12::part_two(&records));
}

fn thirteen() {
    let patterns = resources::as_string(include_bytes!("../resources/13"));

    timer(&|| s13::point_of_incidence(patterns.clone()));
    println!("{}", s13::part_two(patterns));
}

fn fourteen() {
    let map = resources::as_list_of_lists(include_bytes!("../resources/14"));

    timer(&|| s14::parabolic_reflector_dish(&map));
    println!("{}", s14::part_two(&map));
}

fn fifteen() {
    let initialization_sequence = resources::as_string(include_bytes!("../resources/15"));

    timer(&|| s15::lens_library(&initialization_sequence));
    timer(&|| s15::part_two(&initialization_sequence));
}

fn sixteen() {
    let layout = resources::as_list(include_bytes!("../resources/16"));

    timer(&|| s16::the_floor_will_be_lava(&layout));
    println!("{}", s16::part_two(&layout));
}

fn seventeen() {
    let map = resources::as_list(include_bytes!("../resources/17"));

    println!("{}", s17::clumsy_crucible(&map));
    println!("{}", s17::part_two(&map));
}

fn twenty() {
    let configuration = resources::as_list(include_bytes!("../resources/20"));

    timer(&|| s20::pulse_propagation(&configuration));
}

fn twentyone() {
    let map = resources::as_list_of_lists(include_bytes!("../resources/21"));

    println!("{}", s21::step_counter(&map));
}

// fn twentytwo() {
//     let notes = resources::as_list(include_bytes!("../resources/22"));

//     timer(&|| s22::monkey_map(&notes));
// }

// fn twentythree() {
//     let grove = resources::as_list(include_bytes!("../resources/23"));

//     timer(&|| s23::unstable_diffusion(&grove, false));
//     println!("{}", s23::unstable_diffusion(&grove, true));
// }

// fn twentyfour() {
//     let valley = resources::as_list(include_bytes!("../resources/24"));

//     println!("{}", s24::blizzard_basin(&valley, false));
//     println!("{}", s24::blizzard_basin(&valley, true));
// }

// fn twentyfive() {
//     let fuel_requirements = resources::as_list(include_bytes!("../resources/25"));

//     timer(&|| s25::full_of_hot_air(&fuel_requirements));
// }
