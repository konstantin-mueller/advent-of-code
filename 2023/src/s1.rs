use regex::Regex;

pub fn trebuchet(document: &Vec<String>) -> i32 {
    document
        .iter()
        .map(|line| {
            let mut number = String::from(line.chars().find(|char| char.is_numeric()).unwrap());
            number.push(line.chars().rev().find(|char| char.is_numeric()).unwrap());
            number.parse::<i32>().unwrap()
        })
        .sum()
}

pub fn part_two(document: &Vec<String>) -> i32 {
    let regex =
        Regex::new(r"\d|(one)|(two)|(three)|(four)|(five)|(six)|(seven)|(eight)|(nine)").unwrap();

    let regex_rev =
        Regex::new(r"\d|(eno)|(owt)|(eerht)|(ruof)|(evif)|(xis)|(neves)|(thgie)|(enin)").unwrap();

    document
        .iter()
        .map(|line| {
            let first = regex.find(line).unwrap().as_str();

            let reversed = line.chars().rev().collect::<String>();
            let second = regex_rev
                .find(&reversed)
                .unwrap()
                .as_str()
                .chars()
                .rev()
                .collect::<String>();

            let mut number = String::from(get_number(first));
            number.push_str(get_number(&second));
            number.parse::<i32>().unwrap()
        })
        .sum()
}

fn get_number(number: &str) -> &str {
    match number {
        "one" => "1",
        "two" => "2",
        "three" => "3",
        "four" => "4",
        "five" => "5",
        "six" => "6",
        "seven" => "7",
        "eight" => "8",
        "nine" => "9",
        _ => number,
    }
}
