use regex::Regex;

pub fn cube_conundrum(games: &Vec<String>) -> i32 {
    let regex = Regex::new(r"\d+:|\d+\s\w+").unwrap();

    games
        .iter()
        .filter_map(|game| {
            let mut matches = regex.find_iter(game);
            let id = matches.next().unwrap();

            let is_impossible = matches.any(|cubes| {
                let cubes_str = cubes.as_str();
                let number: i32 = cubes_str
                    .split_whitespace()
                    .next()
                    .unwrap()
                    .parse()
                    .unwrap();

                (cubes_str.contains("red") && number > 12)
                    || (cubes_str.contains("green") && number > 13)
                    || (cubes_str.contains("blue") && number > 14)
            });

            if is_impossible {
                None
            } else {
                Some(id.as_str().trim_end_matches(":").parse::<i32>().unwrap())
            }
        })
        .sum()
}

pub fn part_two(games: &Vec<String>) -> i32 {
    let regex = Regex::new(r"\d+\s\w+").unwrap();

    games
        .iter()
        .map(|game| {
            let mut max_red = 0;
            let mut max_green = 0;
            let mut max_blue = 0;

            for cubes in regex.find_iter(game) {
                let mut split_cubes = cubes.as_str().split_whitespace();
                let number: i32 = split_cubes.next().unwrap().parse().unwrap();
                let color = split_cubes.next().unwrap();

                match color {
                    "red" if number > max_red => max_red = number,
                    "green" if number > max_green => max_green = number,
                    "blue" if number > max_blue => max_blue = number,
                    _ => (),
                }
            }

            max_red * max_green * max_blue
        })
        .sum()
}
