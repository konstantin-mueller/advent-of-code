use std::collections::HashMap;

pub fn haunted_wasteland(documents: &Vec<String>) -> usize {
    let instructions = &documents[0];
    let map: HashMap<&str, (String, String)> = documents
        .into_iter()
        .skip(2)
        .map(|line| {
            let mut split = line.split(" = ");
            let node = split.next().unwrap();
            let mut elements = split.next().unwrap().split(", ");
            (
                node,
                (
                    elements.next().unwrap().replace("(", ""),
                    elements.next().unwrap().replace(")", ""),
                ),
            )
        })
        .collect();

    let mut result = 0;
    let mut key = "AAA";
    for instruction in instructions.chars().cycle() {
        key = if instruction == 'L' {
            &map.get(key).unwrap().0
        } else {
            &map.get(key).unwrap().1
        };

        result += 1;
        if key == "ZZZ" {
            break;
        }
    }

    result
}

pub fn part_two(documents: &Vec<String>) -> usize {
    let instructions = &documents[0];

    let mut l_map = HashMap::new();
    let mut r_map = HashMap::new();
    for line in documents.into_iter().skip(2) {
        let mut split = line.split(" = ");
        let node = split.next().unwrap();
        let mut elements = split.next().unwrap().split(", ");

        l_map
            .entry(node)
            .or_insert(elements.next().unwrap().replace("(", ""));

        r_map
            .entry(node)
            .or_insert(elements.next().unwrap().replace(")", ""));
    }

    let mut current_nodes: Vec<&str> = l_map
        .keys()
        .filter_map(|node| {
            if node.ends_with('A') {
                Some(*node)
            } else {
                None
            }
        })
        .collect();

    let mut result = HashMap::new();
    let mut j = 0;
    let mut end_result = 0;

    while end_result == 0 {
        current_nodes = current_nodes
            .iter()
            .enumerate()
            .map(|(i, node)| {
                let new_node = if instructions.chars().nth(j % instructions.len()).unwrap() == 'L' {
                    l_map.get(node).unwrap()
                } else {
                    r_map.get(node).unwrap()
                };

                if end_result == 0 && new_node.ends_with('Z') {
                    result.entry(i).or_insert(j + 1);

                    if result.len() == current_nodes.len() {
                        end_result = result
                            .values()
                            .cloned()
                            .reduce(|acc, e| e * acc / gcd(e, acc))
                            .unwrap();
                    }
                }

                new_node.as_str()
            })
            .collect();

        j += 1;
    }
    end_result
}

fn gcd(a: usize, b: usize) -> usize {
    if b == 0 {
        return a;
    }

    gcd(b, a.rem_euclid(b))
}
