use std::{cmp::Ordering, collections::HashMap};

struct Hand<'a> {
    hand: &'a str,
    bid: usize,
    // 0 - 6
    hand_type: i32,
}

pub fn camel_cards(hands: &Vec<String>) -> usize {
    let strengths = vec![
        'A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2',
    ];

    let mut hands_vec: Vec<Hand<'_>> = hands
        .into_iter()
        .map(|hand| {
            let mut split = hand.split_whitespace();
            let hand_cards = split.next().unwrap();

            let mut cards = HashMap::new();
            for card in hand_cards.chars() {
                cards
                    .entry(card)
                    .and_modify(|count| *count += 1)
                    .or_insert(1);
            }

            let mut hand_type = 0;
            for value in cards.values() {
                match value {
                    5 => hand_type = 6,
                    4 => {
                        hand_type = 5;
                        break;
                    }
                    3 if hand_type == 1 => hand_type = 4,
                    2 if hand_type == 3 => hand_type = 4,
                    3 => hand_type = 3,
                    2 if hand_type == 1 => {
                        hand_type = 2;
                        break;
                    }
                    2 => hand_type = 1,
                    _ => (),
                };
            }

            Hand {
                hand: hand_cards,
                bid: split.next().unwrap().parse().unwrap(),
                hand_type,
            }
        })
        .collect();

    calc_total_winnings(&mut hands_vec, &strengths)
}

pub fn part_two(hands: &Vec<String>) -> usize {
    let strengths = vec![
        'A', 'K', 'Q', 'T', '9', '8', '7', '6', '5', '4', '3', '2', 'J',
    ];

    let mut hands_vec: Vec<Hand<'_>> = hands
        .into_iter()
        .map(|hand| {
            let mut split = hand.split_whitespace();
            let hand_cards = split.next().unwrap();

            let mut cards = HashMap::new();
            for card in hand_cards.chars() {
                cards
                    .entry(card)
                    .and_modify(|count| *count += 1)
                    .or_insert(1);
            }

            let mut hand_type = 0;
            for (card, value) in cards.iter() {
                if *card == 'J' {
                    continue;
                }

                match value {
                    5 => hand_type = 6,
                    4 => {
                        hand_type = 5;
                        break;
                    }
                    3 if hand_type == 1 => hand_type = 4,
                    2 if hand_type == 3 => hand_type = 4,
                    3 => hand_type = 3,
                    2 if hand_type == 1 => {
                        hand_type = 2;
                        break;
                    }
                    2 => hand_type = 1,
                    _ => (),
                };
            }

            let j_count = *cards.get(&'J').unwrap_or(&0);
            hand_type = match j_count {
                1 if hand_type == 5 => 6,
                1 if hand_type == 3 => 5,
                1 if hand_type == 2 => 4,
                1 if hand_type == 1 => 3,
                1 if hand_type == 0 => 1,
                2 if hand_type == 3 => 6,
                2 if hand_type == 1 => 5,
                2 if hand_type == 0 => 3,
                3 if hand_type == 1 => 6,
                3 if hand_type == 0 => 5,
                4 | 5 => 6,
                _ => hand_type,
            };

            Hand {
                hand: hand_cards,
                bid: split.next().unwrap().parse().unwrap(),
                hand_type,
            }
        })
        .collect();

    calc_total_winnings(&mut hands_vec, &strengths)
}

fn calc_total_winnings(hands: &mut Vec<Hand>, strengths: &Vec<char>) -> usize {
    hands.sort_by(|a, b| {
        let result = a.hand_type.cmp(&b.hand_type);

        if result == Ordering::Equal {
            return a
                .hand
                .chars()
                .zip(b.hand.chars())
                .filter(|(current_a, current_b)| current_a != current_b)
                .find_map(|(current_a, current_b)| {
                    strengths.iter().find_map(|strength| {
                        if *strength == current_a {
                            Some(Ordering::Greater)
                        } else if *strength == current_b {
                            Some(Ordering::Less)
                        } else {
                            None
                        }
                    })
                })
                .unwrap();
        }

        result
    });

    hands
        .iter()
        .enumerate()
        .map(|(i, hand)| (i + 1) * hand.bid)
        .sum()
}
