use std::collections::{HashSet, VecDeque};

pub fn step_counter(map: &Vec<Vec<char>>) -> usize {
    let steps = 64;

    let start = map
        .iter()
        .enumerate()
        .find_map(|(i, line)| {
            line.iter().enumerate().find_map(|(j, char)| {
                if *char == 'S' {
                    Some((i as i32, j as i32))
                } else {
                    None
                }
            })
        })
        .unwrap();

    let plots: HashSet<(i32, i32)> = map
        .iter()
        .enumerate()
        .flat_map(|(i, line)| {
            line.iter().enumerate().filter_map(move |(j, char)| {
                if *char == '#' {
                    None
                } else {
                    Some((i as i32, j as i32))
                }
            })
        })
        .collect();

    let mut cache = HashSet::new();
    let mut queue: VecDeque<((i32, i32), usize)> = VecDeque::new();
    queue.push_back((start, 0));

    let max_x = plots.iter().map(|(_, x)| *x).max().unwrap();
    let max_y = plots.iter().map(|(y, _)| *y).max().unwrap();

    let mut result = 0;
    while let Some((pos, current_step)) = queue.pop_front() {
        if cache.contains(&(pos, current_step)) {
            continue;
        }
        cache.insert((pos, current_step));

        if current_step == steps {
            result += 1;
            continue;
        }

        for next_pos in [
            (pos.0, pos.1 - 1),
            (pos.0, pos.1 + 1),
            (pos.0 - 1, pos.1),
            (pos.0 + 1, pos.1),
        ] {
            if plots.contains(&(
                next_pos.1.rem_euclid(max_y + 1),
                next_pos.0.rem_euclid(max_x + 1),
            )) {
                queue.push_back((next_pos, current_step + 1));
            }
        }
    }

    result
}
