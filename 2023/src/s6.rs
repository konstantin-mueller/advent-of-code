use regex::Regex;

pub fn wait_for_it(paper: &Vec<String>) -> i32 {
    let re = Regex::new(r"\d+").unwrap();
    let distances: Vec<i32> = re
        .find_iter(&paper[1])
        .map(|number| number.as_str().parse().unwrap())
        .collect();

    re.find_iter(&paper[0])
        .map(|number| number.as_str().parse().unwrap())
        .enumerate()
        .map(|(i, time)| {
            (2..time)
                .filter(|speed| (time - speed) * speed > distances[i])
                .count() as i32
        })
        .product()
}

pub fn part_two(paper: &Vec<String>) -> i64 {
    let time: i64 = paper[0]
        .replace("Time:", "")
        .replace(" ", "")
        .parse()
        .unwrap();
    let distance = paper[1]
        .replace("Distance:", "")
        .replace(" ", "")
        .parse()
        .unwrap();

    let start = (2..time)
        .find(|speed| (time - speed) * speed > distance)
        .unwrap();

    time - start - start + 1
}
