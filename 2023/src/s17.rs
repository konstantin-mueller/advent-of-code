use std::collections::{BTreeSet, HashMap, HashSet};

pub fn clumsy_crucible(map: &Vec<String>) -> u32 {
    let parsed_map = map
        .into_iter()
        .enumerate()
        .flat_map(|(y, line)| {
            line.char_indices()
                .map(move |(x, c)| ((x as i32, y as i32), c.to_digit(10).unwrap()))
        })
        .collect();

    find_path(
        &parsed_map,
        (map[0].len() as i32 - 1, map.len() as i32 - 1),
        false,
    )
}
pub fn part_two(map: &Vec<String>) -> u32 {
    let parsed_map = map
        .into_iter()
        .enumerate()
        .flat_map(|(y, line)| {
            line.char_indices()
                .map(move |(x, c)| ((x as i32, y as i32), c.to_digit(10).unwrap()))
        })
        .collect();

    find_path(
        &parsed_map,
        (map[0].len() as i32 - 1, map.len() as i32 - 1),
        true,
    )
}

fn find_path(map: &HashMap<(i32, i32), u32>, end: (i32, i32), is_part_two: bool) -> u32 {
    let mut candidates = BTreeSet::from([(0, (0, 0), (0, 0))]);
    let mut visited = HashSet::new();

    while let Some((heat_loss, pos, direction)) = candidates.pop_first() {
        if pos == end {
            return heat_loss;
        }

        if !visited.insert((pos, direction)) {
            continue;
        }

        let next_direction = match direction {
            (1, _) | (-1, _) => [(0, 1), (0, -1)],
            (_, 1) | (_, -1) => [(1, 0), (-1, 0)],
            (0, 0) => [(1, 0), (0, 1)],
            _ => unreachable!(),
        };
        for (x, y) in next_direction {
            let mut heat_loss = heat_loss;
            let mut pos = pos;

            for i in if is_part_two { 1..=10 } else { 0..=2 } {
                pos.0 += x;
                pos.1 += y;

                if let Some(current_heat_loss) = map.get(&pos) {
                    heat_loss += current_heat_loss;

                    if !is_part_two || i >= 4 {
                        candidates.insert((heat_loss, pos, (x, y)));
                    }
                } else {
                    break;
                }
            }
        }
    }

    unreachable!()
}
