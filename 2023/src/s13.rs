pub fn point_of_incidence(patterns: String) -> usize {
    patterns
        .split("\n\n")
        .map(|pattern| calc(pattern, 0, false, false).0)
        .sum()
}

pub fn part_two(patterns: String) -> usize {
    let mut result = 0;

    'outer: for pattern in patterns.split("\n\n") {
        let (_, old_index, was_vertical) = calc(pattern, 0, false, false);

        for line in pattern.split("\n") {
            for i in 0..line.len() {
                let current_pattern = pattern.replace(
                    line,
                    &line
                        .char_indices()
                        .map(|(j, char)| {
                            if j == i {
                                if char == '.' {
                                    '#'
                                } else {
                                    '.'
                                }
                            } else {
                                char
                            }
                        })
                        .collect::<String>(),
                );

                let (part_result, _, _) = calc(&current_pattern, old_index, was_vertical, true);

                if part_result != usize::MAX {
                    result += part_result;
                    continue 'outer;
                }
            }
        }
    }

    result
}

fn calc(
    pattern: &str,
    ignore_index: usize,
    ignore_is_vertical: bool,
    part_two: bool,
) -> (usize, usize, bool) {
    let lines: Vec<_> = pattern.split("\n").collect();

    let first_line = lines.first().unwrap();
    let mut vertical_reflections: Vec<usize> = (1..first_line.len())
        .filter_map(|i| {
            if part_two && ignore_is_vertical && i == ignore_index {
                None
            } else {
                let (first, second) = mirror(first_line, i);
                if first == second {
                    Some(i)
                } else {
                    None
                }
            }
        })
        .collect();

    for line in lines.iter().skip(1) {
        vertical_reflections = vertical_reflections
            .into_iter()
            .filter(|reflection| {
                let (first, second) = mirror(line, *reflection);
                first == second
            })
            .collect();
    }
    if !vertical_reflections.is_empty() {
        return (vertical_reflections[0], vertical_reflections[0], true);
    }

    let first_column: String = lines
        .iter()
        .map(|line| line.chars().next().unwrap())
        .collect();
    let mut horizontal_reflections: Vec<usize> = (1..first_column.len())
        .filter_map(|i| {
            if part_two && !ignore_is_vertical && i == ignore_index {
                None
            } else {
                let (first, second) = mirror(&first_column, i);
                if first == second {
                    Some(i)
                } else {
                    None
                }
            }
        })
        .collect();

    for i in 1..lines[0].len() {
        let column = lines
            .iter()
            .map(|line| line.chars().nth(i).unwrap())
            .collect::<String>();

        horizontal_reflections = horizontal_reflections
            .into_iter()
            .filter(|reflection| {
                let (first, second) = mirror(&column, *reflection);
                first == second
            })
            .collect();
    }

    if !horizontal_reflections.is_empty() {
        (
            horizontal_reflections[0] * 100,
            horizontal_reflections[0],
            false,
        )
    } else {
        (usize::MAX, usize::MAX, false)
    }
}

fn mirror(line: &str, index: usize) -> (String, String) {
    let (f, s) = line.split_at(index);
    let mut first = f.chars().rev().collect::<String>();
    let mut second = String::from(s);

    if first.len() < second.len() {
        second = second.chars().take(first.len()).collect::<String>();
    } else if first.len() > second.len() {
        first = first.chars().take(second.len()).collect();
    }

    (first, second)
}
