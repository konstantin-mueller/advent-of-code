use std::{
    collections::{HashMap, HashSet},
    iter::once,
};

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
enum Direction {
    DOWN,
    UP,
    LEFT,
    RIGHT,
}

pub fn the_floor_will_be_lava(layout: &Vec<String>) -> usize {
    let mut cache = HashMap::new();

    energize(
        (0, 0),
        &Direction::RIGHT,
        &layout
            .into_iter()
            .map(|line| line.chars().collect())
            .collect(),
        &mut cache,
    );

    cache.keys().count()
}

pub fn part_two(layout: &Vec<String>) -> usize {
    let mut result = 0;
    let layout_chars: Vec<Vec<char>> = layout
        .into_iter()
        .map(|line| line.chars().collect())
        .collect();

    let max_y = layout.len();
    let max_x = layout[0].len();

    for y in 0..max_y {
        let mut cache = HashMap::new();
        energize((0, y), &Direction::RIGHT, &layout_chars, &mut cache);
        result = result.max(cache.keys().count());
    }

    for y in 0..max_y {
        let mut cache = HashMap::new();
        energize((max_x - 1, y), &Direction::LEFT, &layout_chars, &mut cache);
        result = result.max(cache.keys().count());
    }

    for x in 0..max_x {
        let mut cache = HashMap::new();
        energize((x, 0), &Direction::DOWN, &layout_chars, &mut cache);
        result = result.max(cache.keys().count());
    }

    for x in 0..max_x {
        let mut cache = HashMap::new();
        energize((x, max_y - 1), &Direction::UP, &layout_chars, &mut cache);
        result = result.max(cache.keys().count());
    }

    result
}

fn energize(
    pos: (usize, usize),
    direction: &Direction,
    layout: &Vec<Vec<char>>,
    cache: &mut HashMap<(usize, usize), HashSet<Direction>>,
) {
    if pos.0 >= layout.len() || pos.1 >= layout[0].len() {
        return;
    }

    if let Some(directions) = cache.get(&pos) {
        if directions.contains(&direction) {
            return;
        }
    }

    cache
        .entry(pos)
        .and_modify(|directions| {
            directions.insert(*direction);
        })
        .or_insert(once(*direction).collect());

    match layout[pos.1][pos.0] {
        '.' => energize(empty(&direction, pos), direction, layout, cache),
        '/' => {
            let (new_pos, new_dir) = mirror_slash(&direction, pos);
            energize(new_pos, &new_dir, layout, cache);
        }
        '\\' => {
            let (new_pos, new_dir) = mirror_backslash(&direction, pos);
            energize(new_pos, &new_dir, layout, cache);
        }
        '-' if *direction == Direction::LEFT || *direction == Direction::RIGHT => {
            energize(empty(&direction, pos), direction, layout, cache)
        }
        '-' => {
            energize((pos.0 - 1, pos.1), &Direction::LEFT, layout, cache);
            energize((pos.0 + 1, pos.1), &Direction::RIGHT, layout, cache);
        }
        '|' if *direction == Direction::UP || *direction == Direction::DOWN => {
            energize(empty(&direction, pos), direction, layout, cache)
        }
        '|' => {
            energize((pos.0, pos.1 - 1), &Direction::UP, layout, cache);
            energize((pos.0, pos.1 + 1), &Direction::DOWN, layout, cache);
        }
        _ => unreachable!(),
    };
}

fn empty(direction: &Direction, pos: (usize, usize)) -> (usize, usize) {
    match direction {
        Direction::DOWN => (pos.0, pos.1 + 1),
        Direction::LEFT => (pos.0 - 1, pos.1),
        Direction::RIGHT => (pos.0 + 1, pos.1),
        Direction::UP => (pos.0, pos.1 - 1),
    }
}

fn mirror_slash(direction: &Direction, pos: (usize, usize)) -> ((usize, usize), Direction) {
    match direction {
        Direction::DOWN => ((pos.0 - 1, pos.1), Direction::LEFT),
        Direction::LEFT => ((pos.0, pos.1 + 1), Direction::DOWN),
        Direction::RIGHT => ((pos.0, pos.1 - 1), Direction::UP),
        Direction::UP => ((pos.0 + 1, pos.1), Direction::RIGHT),
    }
}

fn mirror_backslash(direction: &Direction, pos: (usize, usize)) -> ((usize, usize), Direction) {
    match direction {
        Direction::DOWN => ((pos.0 + 1, pos.1), Direction::RIGHT),
        Direction::LEFT => ((pos.0, pos.1 - 1), Direction::UP),
        Direction::RIGHT => ((pos.0, pos.1 + 1), Direction::DOWN),
        Direction::UP => ((pos.0 - 1, pos.1), Direction::LEFT),
    }
}
