use std::collections::HashMap;
use std::collections::HashSet;
use std::iter::repeat;

#[derive(PartialEq, Debug)]
enum Direction {
    LEFT,
    RIGHT,
    DOWN,
    UP,
}

pub fn pipe_maze(maze: &Vec<String>) -> i32 {
    let pipes: HashMap<(i32, i32), char> = maze
        .iter()
        .enumerate()
        .flat_map(|(i, line)| {
            line.chars().enumerate().filter_map(move |(j, char)| {
                if char == '.' {
                    None
                } else {
                    Some(((i as i32, j as i32), char))
                }
            })
        })
        .collect();

    let start = pipes.iter().find(|(_, char)| **char == 'S').unwrap().0;

    let mut next = ((-1, -1), (-1, -1));
    let mut dirs = (Direction::DOWN, Direction::DOWN);
    let mut current = pipes.get(&(start.0 - 1, start.1));
    if current.is_some() {
        let char = *current.unwrap();
        if char == '|' || char == '7' || char == 'F' {
            next.0 = (start.0 - 1, start.1);
            dirs.0 = Direction::UP;
        }
    }
    current = pipes.get(&(start.0 + 1, start.1));
    if current.is_some() {
        let char = *current.unwrap();
        if char == '|' || char == 'L' || char == 'J' {
            if next.0 .0 == -1 {
                next.0 = (start.0 + 1, start.1);
            } else {
                next.1 = (start.0 + 1, start.1);
            }
        }
    }
    current = pipes.get(&(start.0, start.1 - 1));
    if current.is_some() {
        let char = *current.unwrap();
        if char == '|' || char == 'L' || char == 'F' {
            if next.0 .0 == -1 {
                next.0 = (start.0, start.1 - 1);
                dirs.0 = Direction::LEFT;
            } else {
                next.1 = (start.0, start.1 - 1);
                dirs.1 = Direction::LEFT;
            }
        }
    }
    current = pipes.get(&(start.0, start.1 + 1));
    if current.is_some() {
        let char = *current.unwrap();
        if char == '-' || char == 'J' || char == '7' {
            next.1 = (start.0, start.1 + 1);
            dirs.1 = Direction::RIGHT;
        }
    }

    let mut result = 1;
    while next.0 != next.1 {
        (next.0, dirs.0) = get_next_pipe(pipes.get(&next.0).unwrap(), dirs.0, next.0);
        (next.1, dirs.1) = get_next_pipe(pipes.get(&next.1).unwrap(), dirs.1, next.1);
        result += 1;
    }

    result
}

fn get_next_pipe(char: &char, dir: Direction, current: (i32, i32)) -> ((i32, i32), Direction) {
    match char {
        '-' if dir == Direction::RIGHT => ((current.0, current.1 + 1), dir),
        '-' if dir == Direction::LEFT => ((current.0, current.1 - 1), dir),
        '7' if dir == Direction::RIGHT => ((current.0 + 1, current.1), Direction::DOWN),
        '7' if dir == Direction::UP => ((current.0, current.1 - 1), Direction::LEFT),
        '|' if dir == Direction::DOWN => ((current.0 + 1, current.1), dir),
        '|' if dir == Direction::UP => ((current.0 - 1, current.1), dir),
        'J' if dir == Direction::DOWN => ((current.0, current.1 - 1), Direction::LEFT),
        'J' if dir == Direction::RIGHT => ((current.0 - 1, current.1), Direction::UP),
        'L' if dir == Direction::LEFT => ((current.0 - 1, current.1), Direction::UP),
        'L' if dir == Direction::DOWN => ((current.0, current.1 + 1), Direction::RIGHT),
        'F' if dir == Direction::UP => ((current.0, current.1 + 1), Direction::RIGHT),
        'F' if dir == Direction::LEFT => ((current.0 + 1, current.1), Direction::DOWN),
        _ => unreachable!(),
    }
}

pub fn part_two(report: &Vec<String>) -> usize {
    let mut chars_report: Vec<Vec<char>> = report
        .iter()
        .map(|line| (".".to_owned() + line + ".").chars().collect())
        .collect();
    chars_report.insert(0, repeat('.').take(chars_report[0].len()).collect());
    chars_report.push(chars_report[0].clone());

    let start = (0..chars_report.len())
        .find_map(|i| {
            (0..chars_report[0].len()).find_map(|j| {
                if chars_report[i][j] == 'S' {
                    Some((j, i))
                } else {
                    None
                }
            })
        })
        .unwrap();

    let pipes = get_pipes(&chars_report, start);
    let pipe_set: HashSet<_> = pipes.iter().cloned().collect();

    let mut prev = (start.0 as i64, start.1 as i64);
    let mut points_to_mark = vec![];
    for pipe in pipes {
        let current = (pipe.0 as i64, pipe.1 as i64);
        match (current.0 - prev.0, current.1 - prev.1) {
            (1, 0) => {
                points_to_mark.push((pipe.0, pipe.1 + 1));
                points_to_mark.push((pipe.0.wrapping_sub(1), pipe.1 + 1));
            }
            (0, 1) => {
                points_to_mark.push((pipe.0.wrapping_sub(1), pipe.1.wrapping_sub(1)));
                points_to_mark.push((pipe.0.wrapping_sub(1), pipe.1));
            }
            (-1, 0) => {
                points_to_mark.push((pipe.0, pipe.1.wrapping_sub(1)));
                points_to_mark.push((pipe.0 + 1, pipe.1.wrapping_sub(1)));
            }
            (0, -1) => {
                points_to_mark.push((pipe.0 + 1, pipe.1));
                points_to_mark.push((pipe.0 + 1, pipe.1 + 1));
            }
            _ => {}
        }
        prev = current;
    }

    for point in points_to_mark {
        fill(&mut chars_report, point, &pipe_set);
    }

    let result = (0..chars_report.len())
        .map(|i| chars_report[i].iter().filter(|char| **char == 'X').count())
        .sum();

    if chars_report[0][0] == 'X' {
        chars_report.len() * chars_report[0].len() - result - pipe_set.len()
    } else {
        result
    }
}

fn get_pipes(chars_report: &Vec<Vec<char>>, start: (usize, usize)) -> Vec<(usize, usize)> {
    let mut current = start;
    let neighbors = vec![
        (current.0.wrapping_sub(1), current.1),
        (current.0 + 1, current.1),
        (current.0, current.1.wrapping_sub(1)),
        (current.0, current.1 + 1),
    ];
    for neighbor in neighbors {
        if neighbor == start {
            continue;
        }

        match next_pipe(chars_report, neighbor) {
            Some((c1, c2)) => {
                if c1 == current || c2 == current {
                    current = neighbor;
                    break;
                }
            }
            None => {}
        };
    }

    let mut pipes: Vec<(usize, usize)> = vec![start];
    while chars_report[current.1][current.0] != 'S' {
        let (c1, c2) = next_pipe(chars_report, current).unwrap();
        let next = if c1 == *pipes.last().unwrap() { c2 } else { c1 };

        pipes.push(current);
        current = next;
    }

    pipes
}

fn next_pipe(
    chars_report: &Vec<Vec<char>>,
    (x, y): (usize, usize),
) -> Option<((usize, usize), (usize, usize))> {
    if y >= chars_report.len() || x >= chars_report[0].len() {
        return None;
    }

    match chars_report[y][x] {
        '|' => Some(((x, y.wrapping_sub(1)), (x, y + 1))),
        '-' => Some(((x.wrapping_sub(1), y), (x + 1, y))),
        'L' => Some(((x, y.wrapping_sub(1)), (x + 1, y))),
        'J' => Some(((x.wrapping_sub(1), y), (x, y.wrapping_sub(1)))),
        '7' => Some(((x.wrapping_sub(1), y), (x, y + 1))),
        'F' => Some(((x, y + 1), (x + 1, y))),
        '.' => None,
        'S' => None,
        _ => None,
    }
}

fn fill(
    chars_report: &mut Vec<Vec<char>>,
    current: (usize, usize),
    pipes: &HashSet<(usize, usize)>,
) {
    if current.1 >= chars_report.len()
        || current.0 >= chars_report[0].len()
        || chars_report[current.1][current.0] == 'X'
        || pipes.contains(&current)
    {
        return;
    }

    let neighbors = vec![
        (current.0.wrapping_sub(1), current.1),
        (current.0 + 1, current.1),
        (current.0, current.1.wrapping_sub(1)),
        (current.0, current.1 + 1),
    ];

    chars_report[current.1][current.0] = 'X';
    for neighbor in neighbors {
        fill(chars_report, neighbor, pipes);
    }
}
