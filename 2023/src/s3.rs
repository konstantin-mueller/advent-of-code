use std::collections::{HashMap, HashSet};

pub fn gear_ratios(schematic: &Vec<String>) -> i32 {
    let mut symbols = HashSet::new();
    let mut numbers: HashMap<(i32, i32), String> = HashMap::new();

    for (y, line) in schematic.iter().enumerate() {
        let mut current_number_index: i32 = -1;
        let mut current_number = String::new();

        for (x, char) in line.chars().enumerate() {
            if char.is_numeric() {
                if current_number_index == -1 {
                    current_number_index = x as i32;
                }

                current_number.push(char);
                continue;
            }
            if current_number_index > -1 {
                numbers.insert((current_number_index, y as i32), current_number);

                current_number_index = -1;
                current_number = String::new();
            }

            if char != '.' {
                symbols.insert((x as i32, y as i32));
            }
        }

        if current_number_index > -1 {
            numbers.insert((current_number_index, y as i32), current_number);
        }
    }

    let mut result = 0;

    for ((x, y), number) in numbers {
        'outer: for i in x - 1..=x + number.len() as i32 {
            for j in y - 1..=y + 1 {
                if symbols.contains(&(i, j)) {
                    result += number.parse::<i32>().unwrap();
                    break 'outer;
                }
            }
        }
    }

    result
}

pub fn part_two(schematic: &Vec<String>) -> i32 {
    let mut stars = HashMap::new();
    let mut numbers: HashMap<(i32, i32), String> = HashMap::new();

    for (y, line) in schematic.iter().enumerate() {
        let mut current_number_index: i32 = -1;
        let mut current_number = String::new();

        for (x, char) in line.chars().enumerate() {
            if char.is_numeric() {
                if current_number_index == -1 {
                    current_number_index = x as i32;
                }

                current_number.push(char);
                continue;
            }
            if current_number_index > -1 {
                numbers.insert((current_number_index, y as i32), current_number);

                current_number_index = -1;
                current_number = String::new();
            }

            if char == '*' {
                stars.insert((x as i32, y as i32), (0, 1));
            }
        }

        if current_number_index > -1 {
            numbers.insert((current_number_index, y as i32), current_number);
        }
    }

    for ((x, y), number) in numbers {
        'outer: for i in x - 1..=x + number.len() as i32 {
            for j in y - 1..=y + 1 {
                if stars.contains_key(&(i, j)) {
                    stars.entry((i, j)).and_modify(|(count, num)| {
                        *count += 1;
                        *num *= number.parse::<i32>().unwrap()
                    });
                    break 'outer;
                }
            }
        }
    }

    stars
        .values()
        .filter_map(|(count, number)| if *count == 2 { Some(number) } else { None })
        .sum()
}
