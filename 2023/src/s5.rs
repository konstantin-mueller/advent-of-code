use std::str::Lines;

struct Map {
    source_start: i64,
    source_end: i64,
    destination_start: i64,
}

pub fn if_you_give_a_seed_a_fertilizer(almanac: &String) -> i64 {
    let mut lines = almanac.lines();
    let seeds = lines
        .next()
        .unwrap()
        .split(": ")
        .skip(1)
        .next()
        .unwrap()
        .split_whitespace()
        .map(|number| number.parse::<i64>().unwrap());

    // Skip empty line
    lines.next();

    let converters = vec![
        parse_map(&mut lines),
        parse_map(&mut lines),
        parse_map(&mut lines),
        parse_map(&mut lines),
        parse_map(&mut lines),
        parse_map(&mut lines),
        parse_map(&mut lines),
    ];

    let mut lowest_location = i64::MAX;

    for seed in seeds {
        let mut result = seed;
        for converter in &converters {
            for map in converter {
                if result >= map.source_start && result < map.source_end {
                    result += map.destination_start - map.source_start;
                    break;
                }
            }
        }

        if result < lowest_location {
            lowest_location = result;
        }
    }

    lowest_location
}

pub fn part_two(almanac: &String) -> i64 {
    let mut lines = almanac.lines();
    let mut seeds = lines
        .next()
        .unwrap()
        .split(": ")
        .skip(1)
        .next()
        .unwrap()
        .split_whitespace()
        .map(|number| number.parse::<i64>().unwrap());

    // Skip empty line
    lines.next();

    let converters = vec![
        parse_map(&mut lines),
        parse_map(&mut lines),
        parse_map(&mut lines),
        parse_map(&mut lines),
        parse_map(&mut lines),
        parse_map(&mut lines),
        parse_map(&mut lines),
    ];

    let mut lowest_location = i64::MAX;

    while let Some(start_seed) = seeds.next() {
        let range = seeds.next().unwrap();
        let end_seed = start_seed + range;
        let mut seed = start_seed;
        let mut skip = range;

        while seed < end_seed {
            let mut result = seed;

            for converter in &converters {
                for map in converter {
                    if result >= map.source_start && result < map.source_end {
                        skip = skip.min(map.source_end - result).max(1);
                        result += map.destination_start - map.source_start;
                        break;
                    }
                }
            }

            if result < lowest_location {
                lowest_location = result;
            }

            seed += skip;
            skip = range - seed + start_seed;
        }
    }

    lowest_location
}

fn parse_map(lines: &mut Lines) -> Vec<Map> {
    lines
        .skip(1)
        .take_while(|line| !line.is_empty())
        .map(|line| {
            let mut numbers = line.split_whitespace();
            let destination_start = numbers.next().unwrap().parse().unwrap();
            let source_start = numbers.next().unwrap().parse().unwrap();
            let source_end = source_start + numbers.next().unwrap().parse::<i64>().unwrap();

            Map {
                source_start,
                source_end,
                destination_start,
            }
        })
        .collect()
}
