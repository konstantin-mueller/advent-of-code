use std::collections::{HashMap, HashSet};

pub fn scratchcards(cards: &Vec<String>) -> usize {
    cards
        .iter()
        .map(|card_stack| {
            let (winning_numbers, numbers) = parse_numbers(card_stack);
            let x = winning_numbers.intersection(&numbers).count();

            if x < 2 {
                x
            } else {
                2_usize.pow(x as u32 - 1)
            }
        })
        .sum()
}

pub fn part_two(cards: &Vec<String>) -> i32 {
    let mut result = HashMap::new();

    for (i, card_stack) in cards.iter().enumerate() {
        let (winning_numbers, numbers) = parse_numbers(card_stack);
        let x = winning_numbers.intersection(&numbers).count();

        let number_of_stacks = *result
            .entry(i + 1)
            .and_modify(|count| *count += 1)
            .or_insert(1);

        for j in i + 2..i + 2 + x {
            result
                .entry(j)
                .and_modify(|count| *count += number_of_stacks)
                .or_insert(number_of_stacks);
        }
    }

    result.values().sum()
}

fn parse_numbers(card_stack: &String) -> (HashSet<i32>, HashSet<i32>) {
    let mut split = card_stack.split(": ").skip(1).next().unwrap().split(" | ");

    let winning_numbers: HashSet<i32> = split
        .next()
        .unwrap()
        .trim()
        .split_whitespace()
        .map(|number| number.parse().unwrap())
        .collect();
    let numbers: HashSet<i32> = split
        .next()
        .unwrap()
        .trim()
        .split_whitespace()
        .map(|number| number.parse().unwrap())
        .collect();

    (winning_numbers, numbers)
}
