use std::collections::HashMap;

pub fn lens_library(initialization_sequence: &String) -> usize {
    initialization_sequence.split(",").map(hash).sum()
}

pub fn part_two(initialization_sequence: &String) -> usize {
    let mut boxes: HashMap<usize, Vec<&str>> = HashMap::new();
    let mut labels: HashMap<&str, usize> = HashMap::new();

    for sequence in initialization_sequence.split(",") {
        if sequence.contains("=") {
            let mut split = sequence.split("=");
            let label = split.next().unwrap();
            let focal_length = split.next().unwrap().parse().unwrap();

            boxes
                .entry(hash(label))
                .and_modify(|lenses| {
                    if lenses.contains(&label) {
                        labels.entry(label).and_modify(|length| {
                            *length = focal_length;
                        });
                    } else {
                        lenses.push(label);
                        labels.insert(label, focal_length);
                    }
                })
                .or_insert(vec![label]);

            if !labels.contains_key(label) {
                labels.insert(label, focal_length);
            }
        } else {
            let label = &sequence[0..sequence.len() - 1];
            boxes.entry(hash(label)).and_modify(|lenses| {
                if let Some(lens) = lenses.iter().position(|lens| *lens == label) {
                    lenses.remove(lens);
                }
            });
        }
    }

    boxes
        .into_iter()
        .flat_map(|(i, lenses)| {
            let focal_lengths = &labels;
            lenses
                .into_iter()
                .enumerate()
                .map(move |(j, label)| (i + 1) * (j + 1) * focal_lengths[label])
        })
        .sum()
}

fn hash(sequence: &str) -> usize {
    sequence
        .chars()
        .fold(0, |acc, char| ((acc + char as usize) * 17) % 256)
}
